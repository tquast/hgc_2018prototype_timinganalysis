#Thorben Quast
#17 November 2020
#Script making event display videos from calibrated samples

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
from config.analysis import COMMONTSPARAMETERS
MINHITENERGY = COMMONTSPARAMETERS["MINHITENERGY"]
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import imageio
import os, argparse
import ROOT
ROOT.gROOT.SetBatch(True)
import root_numpy as rn
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--calibFile', type=str, help='Input file with the calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.4/calibrated_samples/calibration_calibrated.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the ntuple for visualisation", default="/afs/cern.ch/user/t/tquast/Desktop/timing_material/event_video", required=False)
parser.add_argument("--referenceNtuplePath", type=str, help="path to to ntuple to create channel map from, only useful if channels are to be visualised", default="/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16/ntuple_912.root", required=False)
args = parser.parse_args()
NEVENTS=3
RUNS = [980, 958]

#channel to position mapping
ch_to_pos_map = {}
ntupleTreeFile = ROOT.TFile(args.referenceNtuplePath, "READ")
ntupleTree = ntupleTreeFile.Get("rechitntupler/hits")
ntupleData = rn.tree2array(ntupleTree, branches=["rechit_%s" % quantity for quantity in ["module", "chip", "channel", "x", "y", "z", "layer"]])
for ev in tqdm(range(len(ntupleData))):
	for nhit in range(len(ntupleData["rechit_module"][ev])):
		key = ntupleData["rechit_module"][ev][nhit]*1000+ntupleData["rechit_chip"][ev][nhit]*100+ntupleData["rechit_channel"][ev][nhit]
		if key in ch_to_pos_map:
			continue
		ch_to_pos_map[key] = (ntupleData["rechit_x"][ev][nhit], ntupleData["rechit_y"][ev][nhit], ntupleData["rechit_z"][ev][nhit], ntupleData["rechit_layer"][ev][nhit])

for RUN in RUNS:
    #read input file
    store = pd.HDFStore(args.calibFile)

    data = store["evaluation_calibrated"] 
    data = data[data.run==RUN]
    if RUN==958:
        data = data[data.N_hits_FH>200]
    else:
        data = data[data.N_hits_FH<80]
    data = data[data.calibrated_hit_time_combined>=0]       #select valid time reconstruction
    data = data[data.calibrated_hit_time_combined<=25]       #select valid time reconstruction
    data = data[data.hit_energy>50]
    events = data.unique_event_index.value_counts().nlargest(NEVENTS)


    for nindex in range(NEVENTS):
        event_data = data[data.unique_event_index==events.index[nindex]]

        channel_key = np.array(event_data.channelkey)
        x = np.array([ch_to_pos_map[ch][0] for ch in channel_key])
        y = np.array([ch_to_pos_map[ch][1] for ch in channel_key])
        z = np.array([ch_to_pos_map[ch][2] for ch in channel_key])
        E = np.array(event_data.hit_energy)
        t = np.array(event_data.calibrated_hit_time_combined)

        print "Number of hits before the cut:",len(t)


        E_max = max(E)
        E_min = min(E)
        t_min = min(t)
        t_max = max(t)
        #window configuration
        zmin = min(z)-5.
        zmax = max(z)+5.
        ymin = min(y)-0.5
        ymax = max(y)+0.5
        xmin = min(x)-0.5
        xmax = max(x)+0.5


        #intensity and colour definition
        intensity = 300*(1-(E_max-E)/(E_max-E_min))
        colours = t-t_min

        #visualisation of frames as png files
        current_t = t_min
        N_ts=0
        while current_t<t_max:
            current_t+=0.05
            print current_t
            N_ts+=1
            x_ts = x[np.where(t<current_t)]
            y_ts = y[np.where(t<current_t)]
            z_ts = z[np.where(t<current_t)]
            intensity_ts = intensity[np.where(t<current_t)]
            colours_ts = colours[np.where(t<current_t)]
            fig = plt.figure(figsize=(15,10))
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(z_ts, y_ts, x_ts, s=intensity_ts, c=colours_ts)
            ax.view_init(elev=27., azim=-121)
            ax.set_xlabel('HGCal Z [cm]')
            ax.set_ylabel('HGCal Y [cm]')
            ax.set_zlabel('HGCal X [cm]')
            ax.set_xlim([zmin,zmax])
            ax.set_ylim([ymin,ymax])
            ax.set_zlim([xmin,xmax])
            ax.set_title("HGCal TB October 2018 %s after %.2f ns"%({958: "250 GeV pion", 980: "250 GeV positron"}[RUN], current_t-t_min))
            plt.savefig(os.path.join(args.outputFilePath, "run%i_event%s_ts%s.png"% (RUN, nindex, N_ts)))
            plt.clf()
            plt.close()

        '''
        with imageio.get_writer(os.path.join(args.outputFilePath, 'event%i.mp4'%(nindex)), fps=10) as writer:

            for ts in range(1, N_ts+1):
                print "Appending",os.path.join(args.outputFilePath, "event%s_ts%s.png"% (nindex, N_ts))
                writer.append_data(imageio.imread(os.path.join(args.outputFilePath, "event%s_ts%s.png"% (nindex, N_ts))))
            writer.close()

        #cleanup of files
        for ts in range(1, N_ts+1):
            print "Removing",os.path.join(args.outputFilePath, "event%s_ts%s.png"% (nindex, N_ts))
            os.remove(os.path.join(args.outputFilePath, "event%s_ts%s.png"% (nindex, N_ts)))
        '''