#Created: 23 April 2020
#Last modified: 29 April 2020
#author: Thorben Quast, thorben.quast@cern.ch

import os
import ROOT
import root_numpy as rn
import pandas as pd
import numpy as np
from tqdm import tqdm
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--calibFile', type=str, help='Input file with the calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.6/calibration/calibration_merged.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the ntuple for visualisation", default="/afs/cern.ch/user/t/tquast/Desktop/figures/EModule_TW_rise_ntuple_for_vis.root", required=False)
parser.add_argument("--referenceNtuplePath", type=str, help="path to to ntuple to create channel map from, only useful if channels are to be visualised", default="/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16/ntuple_918.root", required=False)
args = parser.parse_args()

print "Loading the data"
input_file_calibration = pd.HDFStore(args.calibFile)
calib_data = input_file_calibration["calib"]
input_file_calibration.close()



ch_to_pos_map = {}

ntupleTreeFile = ROOT.TFile(args.referenceNtuplePath, "READ")
ntupleTree = ntupleTreeFile.Get("rechitntupler/hits")
ntupleData = rn.tree2array(ntupleTree, branches=["rechit_%s" % quantity for quantity in ["module", "chip", "channel", "x", "y", "z", "layer"]])

for ev in tqdm(range(len(ntupleData["rechit_module"]))):
	for nhit in range(len(ntupleData["rechit_module"][ev])):
		key = ntupleData["rechit_module"][ev][nhit]*1000+ntupleData["rechit_chip"][ev][nhit]*100+ntupleData["rechit_channel"][ev][nhit]
		if key in ch_to_pos_map:
			continue
		ch_to_pos_map[key] = (ntupleData["rechit_x"][ev][nhit], ntupleData["rechit_y"][ev][nhit], ntupleData["rechit_z"][ev][nhit], ntupleData["rechit_layer"][ev][nhit])


output_dtypes = []	
output_dtypes.append(("calib_layer", np.int32))	
output_dtypes.append(("calib_module", np.int32))	
output_dtypes.append(("calib_chip", np.int32))	
output_dtypes.append(("calib_channel", np.int32))	
output_dtypes.append(("calib_MPV", np.float32))
output_dtypes.append(("calib_x", np.float32))	
output_dtypes.append(("calib_y", np.float32))	
output_dtypes.append(("calib_z", np.float32))	
table = []
values = []

calib_data = calib_data[calib_data.toa_type==1]
for index, row in calib_data.iterrows():
	Module = int(row["Module"])
	ASIC = int(row["Chip"])
	Channel = int(row["Channel"])
	average_calibration = int(row["average_calibration"])
	if average_calibration==1:
		continue
	key = 1000*Module+100*ASIC+Channel
	x1 = 200
	x2 = 1000
	value = 40-average_calibration*10
	values.append(value)
	print Module, ASIC, Channel, value
	if not key in ch_to_pos_map:
		print key,"not found in map for configuration"
		continue
	x = ch_to_pos_map[key][0]
	y = ch_to_pos_map[key][1]
	z = ch_to_pos_map[key][2]
	layer = ch_to_pos_map[key][3]			
	table.append((layer, Module, ASIC, Channel, value, x, y, z))
outtree = rn.array2tree(np.array(table, dtype=output_dtypes))
print "saving to",args.outputFilePath
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
outtree.Write()
outfile.Close()

print np.mean(values), np.std(values)