#Thorben Quast
#19 October 2020
#extended: 26 October 2020
#script to study 
# a) the stability of the TOA_{rise/fall} minimum and maximum values between different run ranges
# b) the properties of the events for which channels exceed the 1-IQR range in the TOA_fall/_rise readings
OUTPUTDIR = "/afs/cern.ch/user/t/tquast/Desktop/hgc2018_timing_pedestals/iqr_v17ntuples"


import os
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import seaborn as sns
from tqdm import tqdm

from tasks.preprocess import TreatMinMax
from config.analysis import MINMAXPARAMETERS

_file_paths = [TreatMinMax(runNumberMin=r_min, runNumberMax=r_max).output().path for r_min, r_max in MINMAXPARAMETERS["MINMAXCONSTRANGES"]]

########### PART b) ###########
merged_data = None    
for _fpath in _file_paths:
    if not os.path.exists(_fpath):
        continue
    print(_fpath)
    run_min = int(_fpath.replace(".h5", "").split("_to_")[0].split("_")[-1])
    store = pd.HDFStore(_fpath)
    _read_data = store["outliers"]
    _read_data = _read_data.assign(start_run=run_min)
    if merged_data is None:
        merged_data = _read_data
    else:
        merged_data = pd.concat([merged_data, _read_data])
    store.close()

fig, axs = plt.subplots(2, 5, figsize=(45,15))
MINDEVIATION = 10
#TOA-fall max with double-digit deviation
extreme_TOA_fall_data = merged_data[merged_data.TOA_fall_diff_to_max>=MINDEVIATION]
axs[0][0].hist(12000*(extreme_TOA_fall_data["run"]-964)+extreme_TOA_fall_data["event"], bins=12000, range=[0, 18*12000])
#axs[0][0].hist2d(extreme_TOA_fall_data["run"], extreme_TOA_fall_data["event"], bins=[18, 120], range=[[963.5, 981.5], [-0.5, 11999.5]], cmap="viridis", vmin=0., vmax=1.)
axs[0][0].set_xlabel("'Time' 12k*(run-964)+event [a.u.]", fontsize=30)
axs[0][0].set_ylabel("Multiplicity", fontsize=40)
axs[0][1].hist(extreme_TOA_fall_data["hit_energy"].unique(), bins=100, range=[5, 1000])
axs[0][1].set_xlabel("$E_{i}$ [MIP]", fontsize=40)
axs[0][1].set_ylabel("N", fontsize=40)
axs[0][2].hist(extreme_TOA_fall_data["E_sum_layer"].unique(), bins=100, range=[0.5, 3000.5])
axs[0][2].set_xlabel("$\Sigma_{i}^{layer} E_{i}$ [MIP]", fontsize=40)
axs[0][2].set_ylabel("N (1 per layer & event)", fontsize=40)
axs[0][3].hist(extreme_TOA_fall_data["TMCP1"].unique(), bins=51, range=[-0.5, 25.5])
axs[0][3].set_xlabel("$T_{MCP}$ [ns]", fontsize=40)
axs[0][3].set_ylabel("N (1 per event)", fontsize=40)
axs[0][4].hist([len(y[1]) for y in extreme_TOA_fall_data.groupby(extreme_TOA_fall_data.unique_event_index)], bins=31, range=[-0.5, 30.5])
axs[0][4].set_xlabel("# abnormal TOA readings in event", fontsize=40)
axs[0][4].set_ylabel("N", fontsize=40)

extreme_TOA_rise_data = merged_data[merged_data.TOA_rise_diff_to_max>=MINDEVIATION]
axs[1][0].hist(12000*(extreme_TOA_rise_data["run"]-964)+extreme_TOA_rise_data["event"], bins=12000, range=[0, 18*12000])
axs[1][0].set_xlabel("'Time' 12k*(run-964)+event [a.u.]", fontsize=30)
axs[1][0].set_ylabel("Multiplicity", fontsize=40)
axs[1][1].hist(extreme_TOA_rise_data["hit_energy"].unique(), bins=100, range=[5, 1000])
axs[1][1].set_xlabel("$E_{i}$ [MIP]", fontsize=40)
axs[1][1].set_ylabel("N", fontsize=40)
axs[1][2].hist(extreme_TOA_rise_data["E_sum_layer"].unique(), bins=100, range=[0.5, 3000.5])
axs[1][2].set_xlabel("$\Sigma_{i}^{layer} E_{i}$ [MIP]", fontsize=40)
axs[1][2].set_ylabel("N (1 per layer & event)", fontsize=40)
axs[1][3].hist(extreme_TOA_rise_data["TMCP1"].unique(), bins=51, range=[-0.5, 25.5])
axs[1][3].set_xlabel("$T_{MCP}$ [ns]", fontsize=40)
axs[1][3].set_ylabel("N (1 per event)", fontsize=40)
axs[1][4].hist([len(y[1]) for y in extreme_TOA_rise_data.groupby(extreme_TOA_rise_data.unique_event_index)], bins=31, range=[-0.5, 30.5])
axs[1][4].set_xlabel("# abnormal TOA readings in event", fontsize=40)
axs[1][4].set_ylabel("N", fontsize=40)
fig.savefig(os.path.join(OUTPUTDIR, "outliers.pdf"))


for _gr in extreme_TOA_rise_data.groupby(extreme_TOA_rise_data.unique_event_index):
    if len(_gr[1]) >= 10:
        x = _gr[1]
        print x.run.unique()[0], x.event.unique()[0], len(x), round(x.TMCP1.unique()[0], 1), round(x.TOA_rise_diff_to_max.mean(), 1)

########### PART a) ###########
merged_data = None    
for _fpath in _file_paths:
    if not os.path.exists(_fpath):
        continue
    print(_fpath)
    run_min = int(_fpath.replace(".h5", "").split("_to_")[0].split("_")[-1])
    store = pd.HDFStore(_fpath)
    _read_data = store["minmax"]
    _read_data = _read_data.assign(start_run=run_min)
    if merged_data is None:
        merged_data = _read_data
    else:
        merged_data = pd.concat([merged_data, _read_data])
    store.close()

if not os.path.exists(OUTPUTDIR):
    os.mkdir(OUTPUTDIR)
outstore = pd.HDFStore(os.path.join(OUTPUTDIR, "TOA_minmax.h5"))
outstore["pd"] = merged_data
outstore.close()

#identifying channels with min/max values in all seven ranges
counts = merged_data.groupby(["channelkey"])["start_run"].nunique()
full_pedestal_information = []
for chkey, count in tqdm(counts.items(), unit="channels"):
    if count < 9:
        continue
    df_channel = merged_data[merged_data.channelkey==chkey].copy()
    df_channel = df_channel.assign(TOA_rise_range=df_channel.TOA_rise_max-df_channel.TOA_rise_min)
    df_channel = df_channel.assign(TOA_fall_range=df_channel.TOA_fall_max-df_channel.TOA_fall_min)
    df_channel = df_channel.assign(TOA_rise_min=df_channel.TOA_rise_min-np.array(df_channel[df_channel.start_run==964].TOA_rise_min)[0])
    df_channel = df_channel.assign(TOA_fall_min=df_channel.TOA_fall_min-np.array(df_channel[df_channel.start_run==964].TOA_fall_min)[0])
    df_channel = df_channel.assign(TOA_rise_max=df_channel.TOA_rise_max-np.array(df_channel[df_channel.start_run==964].TOA_rise_max)[0])
    df_channel = df_channel.assign(TOA_fall_max=df_channel.TOA_fall_max-np.array(df_channel[df_channel.start_run==964].TOA_fall_max)[0])
    for _q in ["TOA_rise_min", "TOA_rise_max", "TOA_fall_min", "TOA_fall_max"]:
        df_channel[_q] = df_channel[_q].abs()
    full_pedestal_information.append(df_channel)
    
full_pedestal_information = pd.concat(full_pedestal_information)

fig, axs = plt.subplots(4, 1, figsize=(32,20))

titles = {
    "TOA_rise_min": "$\Delta TOA_{rise, 0.01\%}$",
    "TOA_rise_max": "$\Delta TOA_{rise, 99.99\%}$",
    "TOA_fall_min": "$\Delta TOA_{fall, 0.01\%}$",
    "TOA_fall_max": "$\Delta TOA_{fall, 99.99\%}$"
}

for _i, _q in enumerate(sorted(titles)):
    _full_pedestal_information = full_pedestal_information.pivot("start_run", "channelkey", _q)
    hm = sns.heatmap(_full_pedestal_information, annot=True, linewidths=.5, cmap="YlOrRd", cbar=False, cbar_kws={'ticks': range(10)}, annot_kws={'ha':'center', 'va':'center', 'fontsize': 12}, ax=axs[_i], vmin=0, vmax=10)
    axs[_i].set_title("|%s|" % titles[_q], fontsize=24)
    axs[_i].set_ylabel("First run #", fontsize=24)
    if _i==3:
        axs[_i].set_xlabel("Module x e3 + Chip x e2 + Channel (at least 10k entries per run range)", fontsize=24)
    else:
        axs[_i].set_xlabel("", fontsize=1)

plt.savefig(os.path.join(OUTPUTDIR, "TOA_pedestal_variations.pdf"))
plt.savefig(os.path.join(OUTPUTDIR, "TOA_pedestal_variations.png"))

