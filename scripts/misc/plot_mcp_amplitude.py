#Created: 27 April 2020
#Last modified: 27 April 2020
#author: Thorben Quast, thorben.quast@cern.ch

import ROOT
ROOT.gROOT.SetBatch(True)

from config.tb_ntuples import *

h1_mcp1_amp = {}
h1_mcp2_amp = {}

colors = {
	20: ROOT.kRed,
	30: ROOT.kRed-2,
	50: ROOT.kOrange,
	80: ROOT.kYellow,
	100: ROOT.kGreen-2,
	120: ROOT.kGreen,
	150: ROOT.kCyan+1,
	200: ROOT.kBlue-2,
	250: ROOT.kBlue,
	300: ROOT.kMagenta
}

BEAMENERGIES = {
	11: [20, 30, 50, 80, 100, 120, 150, 200, 250, 300],
	211: [20, 30, 50, 80, 100, 150, 200, 250, 300]
}

for PDGID in [11, 211]:
	for drawindex, energy in enumerate(BEAMENERGIES[PDGID]):
		trackimpactntuple = ROOT.TChain("trackimpactntupler/impactPoints")
		mcpntuple = ROOT.TChain("MCPntupler/MCP")

		for run in getRuns(pdgID=PDGID, energy=energy):
			trackimpactntuple.Add(get_ntuple_path(run))
			mcpntuple.Add(get_ntuple_path(run))

		mcpntuple.AddFriend(trackimpactntuple)
		
		h1_mcp1_amp[energy] = ROOT.TH1F("h1_mcp1_amp_%iGeV"%energy, "h1_mcp1_amp_%iGeV"%energy, 60, 0., 1200.)
		h1_mcp2_amp[energy] = ROOT.TH1F("h1_mcp2_amp_%iGeV"%energy, "h1_mcp2_amp_%iGeV"%energy, 60, 0., 1200.)

		mcpntuple.Project("h1_mcp1_amp_%iGeV"%energy, "ampFit_MCP1", "(dwcReferenceType==13)&&((b_x+1.5)^2+(b_y-2)^2<0.25^2)", "COLZ")
		h1_mcp1_amp[energy].SetLineWidth(2)
		h1_mcp1_amp[energy].SetLineColor(colors[energy])
		h1_mcp1_amp[energy].SetStats(False)
		h1_mcp1_amp[energy].Scale(1./h1_mcp1_amp[energy].Integral())
		
		mcpntuple.Project("h1_mcp2_amp_%iGeV"%energy, "ampFit_MCP1", "(dwcReferenceType==13)&&((b_x+1.5)^2+(b_y-2)^2>2.^2)", "COLZ")
		h1_mcp2_amp[energy].SetLineWidth(2)
		h1_mcp2_amp[energy].SetLineColor(colors[energy])
		h1_mcp2_amp[energy].SetStats(False)
		h1_mcp2_amp[energy].Scale(1./h1_mcp2_amp[energy].Integral())

	legend_1 = ROOT.TLegend(0.3, 0.7, 0.9, 0.9)
	legend_1.SetNColumns(3)
	legend_2 = ROOT.TLegend(0.3, 0.7, 0.9, 0.9)
	legend_2.SetNColumns(3)

	canvas = ROOT.TCanvas("c", "c", 1600, 900)
	canvas.cd(1)
	for drawindex, energy in enumerate(sorted(h1_mcp1_amp)):
		if drawindex==0:
			h1_mcp1_amp[energy].Draw("HIST")
			h1_mcp1_amp[energy].SetTitle("Selection: %s centrally impinging w.r.t. MCP 1" % {11: "e^{+}", 211: "#pi^{-}"}[PDGID])
			h1_mcp1_amp[energy].GetXaxis().SetTitle("fitted Amp_{MCP 1} [ADC counts]")
			h1_mcp1_amp[energy].GetXaxis().SetTitleOffset(1.2*h1_mcp1_amp[energy].GetXaxis().GetTitleOffset())
			h1_mcp1_amp[energy].GetYaxis().SetTitle("Entries (normalised) [a.u.]")
			h1_mcp1_amp[energy].GetYaxis().SetTitleOffset(1.2*h1_mcp1_amp[energy].GetYaxis().GetTitleOffset())
		else:
			h1_mcp1_amp[energy].Draw("HISTSAME")
		legend_1.AddEntry(h1_mcp1_amp[energy], "%i GeV %s" % (energy, {11: "e^{+}", 211: "#pi^{-}"}[PDGID]))
	legend_1.Draw()
	'''
	canvas.cd(2)
	for drawindex, energy in enumerate(sorted(h1_mcp2_amp)):
		if drawindex==0:
			h1_mcp2_amp[energy].Draw("HIST")
			h1_mcp2_amp[energy].SetTitle("Selection: Non-central e^{+}")
			h1_mcp2_amp[energy].GetXaxis().SetTitle("fitted Amp_{MCP 1} [ADC counts]")
			h1_mcp2_amp[energy].GetXaxis().SetTitleOffset(1.2*h1_mcp2_amp[energy].GetXaxis().GetTitleOffset())
			h1_mcp2_amp[energy].GetYaxis().SetTitle("Entries (normalised) [a.u.]")		
			h1_mcp2_amp[energy].GetYaxis().SetTitleOffset(1.2*h1_mcp2_amp[energy].GetYaxis().GetTitleOffset())
		else:
			h1_mcp2_amp[energy].Draw("HISTSAME")
		legend_2.AddEntry(h1_mcp2_amp[energy], "%i GeV e^{+}"%energy)
	legend_2.Draw()
	'''
	canvas.Print("/afs/cern.ch/user/t/tquast/Desktop/mcp_amplitudes_%i.pdf" % PDGID)
	
