#Created: 10 May 2021
#Last modified: 10 May 2021
#author: Thorben Quast, thorben.quast@cern.ch

import os
CMSSW_CALIB_PATH = os.path.abspath("/afs/cern.ch/user/t/tquast/CMSSW_11_0_0_pre6/src/HGCal/CondObjects/data/calibration_toaFall_config2_perchannel_20November.txt")
CALIBFILE_TQ = os.path.abspath("/home/tquast/hgc_prototype2018_timinganalysis_v1.6/calibration/calib_file.h5")
CHKEY = 69238


from config.analysis import TOA_LINEARITY_MAX
from helpers.functions import func_TOA, func_Esum_TW, func_TW

import pandas as pd
import numpy as np
import ROOT
from tqdm import tqdm

def func_TOA_CMSSW(x, a0, b0, c0, d0):
    return a0 * x + b0 + c0/(x - d0)

print "Loading the TQ's calibration"
input_file_calibration = pd.HDFStore(CALIBFILE_TQ)
calib_data = input_file_calibration["calib"]
input_file_calibration.close()
calib_data = calib_data.assign(channelkey = 1000*calib_data.Module+100*calib_data.Chip+calib_data.Channel)
calib_data = calib_data[calib_data.toa_type == -1]            #select the fall
unique_channels_TQ = calib_data.channelkey.unique()

print "Loading the CMSSW calibration"
calib_data_CMSSW = pd.DataFrame(np.genfromtxt(open(CMSSW_CALIB_PATH, "r"), usecols = (0, 1, 2, 5, 6, 7, 8), skip_header=1), columns=["Module", "Chip", "Channel", "a_TOA", "b_TOA", "c_TOA", "d_TOA"])
calib_data_CMSSW = calib_data_CMSSW[calib_data_CMSSW.a_TOA != -1.]
calib_data_CMSSW = calib_data_CMSSW.assign(channelkey = 1000*calib_data_CMSSW.Module+100*calib_data_CMSSW.Chip+calib_data_CMSSW.Channel)
unique_channels_CMSSW = calib_data_CMSSW.channelkey.unique()

for CHKEY in tqdm(np.intersect1d(unique_channels_TQ, unique_channels_CMSSW)):
    #select channel in question
    _calib_data = calib_data[calib_data.channelkey==CHKEY]
    _calib_data_CMSSW = calib_data_CMSSW[calib_data_CMSSW.channelkey==CHKEY]

    #extract parameters
    a0_TOA_TQ = np.array(_calib_data.a0_TOA)[0]
    a1_TOA_TQ = np.array(_calib_data.a1_TOA)[0]
    b0_TOA_TQ = np.array(_calib_data.b0_TOA)[0]
    c0_TOA_TQ = np.array(_calib_data.c0_TOA)[0]
    c1_TOA_TQ = np.array(_calib_data.c1_TOA)[0]
    d0_TOA_TQ = np.array(_calib_data.d0_TOA)[0]
    d1_TOA_TQ = np.array(_calib_data.d1_TOA)[0]
    opt_TOA_TQ = (a0_TOA_TQ, a1_TOA_TQ, b0_TOA_TQ, c0_TOA_TQ, c1_TOA_TQ, d0_TOA_TQ, d1_TOA_TQ)

    a_TOA_CMSSW = np.array(_calib_data_CMSSW.a_TOA)[0]
    b_TOA_CMSSW = np.array(_calib_data_CMSSW.b_TOA)[0]
    c_TOA_CMSSW = np.array(_calib_data_CMSSW.c_TOA)[0]
    d_TOA_CMSSW = np.array(_calib_data_CMSSW.d_TOA)[0]
    opt_TOA_CMSSW = (a_TOA_CMSSW, b_TOA_CMSSW, c_TOA_CMSSW, d_TOA_CMSSW)



    #dice uniform normalised toa readings
    toa_norm = np.linspace(0., 1., 1001)
    fTOA_TQ = func_TOA(toa_norm, *opt_TOA_TQ)
    fTOA_CMSSW = func_TOA_CMSSW(toa_norm, *opt_TOA_CMSSW)
    #shift by median
    shift = np.median(fTOA_CMSSW-fTOA_TQ)
    fTOA_TQ = fTOA_TQ + shift

    #make graphs
    gr_TQ = ROOT.TGraph()
    gr_CMSSW = ROOT.TGraph()
    legend = ROOT.TLegend(0.5, 0.7, 0.9, 0.9)
    for n in range(len(toa_norm)):
        gr_TQ.SetPoint(n, toa_norm[n], fTOA_TQ[n])
        gr_CMSSW.SetPoint(n, toa_norm[n], fTOA_CMSSW[n])

    gr_TQ.SetLineColor(ROOT.kRed+1)
    gr_CMSSW.SetLineColor(ROOT.kBlack)

    for gr in [gr_TQ, gr_CMSSW]:
        gr.GetXaxis().SetTitle("Normalised TOA_{fall}")
        gr.GetYaxis().SetTitle("f_{TOA} [ns]")
        gr.SetLineWidth(3)
        gr.SetTitle("Channel %i" % CHKEY)

    canvas = ROOT.TCanvas("canvas", "canvas", 1600, 900)
    canvas.SetGrid(True)
    gr_TQ.Draw("AL")
    gr_CMSSW.Draw("LSAME")
    legend.AddEntry(gr_TQ, "MCP calibration", "l")
    legend.AddEntry(gr_CMSSW, "beam uniformity calibration", "l")
    legend.Draw()

    canvas.Print("/afs/cern.ch/user/t/tquast/Desktop/timing_follow_up_May2021/ftoa_channel%i.pdf" % CHKEY)