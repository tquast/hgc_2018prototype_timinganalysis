#Created: 09 March 2021
#Last modified: 09 March 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Samples random data from all (individually) calibrated pads in order to derive an average calibration
#re-using the steps 2-4 from the calibration sequence.

NRANDOM = int(1e4)

from config.analysis import TOA_LINEARITY_MAX
from helpers.functions import func_TOA, func_Esum_TW, func_TW

import os
import pandas as pd
import numpy as np
np.random.seed(0)			#seed for reproducibility
from tqdm import tqdm
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--calibFile', type=str, help='Input file with the calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/calib_file.h5", required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated evaluation sample", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step1.h5", required=True)
args = parser.parse_args()

print "Loading the calibration data"
input_file_calibration = pd.HDFStore(args.calibFile)
calib_data = input_file_calibration["calib"]
input_file_calibration.close()

#extending the calibration data by channel key to facilitate the matching to the samples
calib_data = calib_data.assign(channelkey = 1000*calib_data.Module+100*calib_data.Chip+calib_data.Channel)


#applying the correction
df_random_merged = []
print "Computing the average calibration dataset..."
for channel in tqdm(calib_data.channelkey.unique(), unit="Channels"):
	#create artificial dataframe
	#distribution of TOA_norm
	toa_norm_rand = np.random.uniform(0., 1., NRANDOM)
	hit_energy_rand = np.random.uniform(0., 1000., NRANDOM)
	layer_energy_rand = np.random.uniform(0., 2500., NRANDOM)
	
	df_random = pd.DataFrame({
		"channelkey": np.array([channel]*NRANDOM), 
		"E_sum_layer": layer_energy_rand,
		"hit_energy": hit_energy_rand,
		"toa_rise": toa_norm_rand, 
		"toa_fall": toa_norm_rand
	})
	
	#read the calibration for each channel
	calib_data_channel = calib_data[calib_data.channelkey==channel]
	calib_data_channel_rise = calib_data_channel[calib_data_channel.toa_type==1]
	calib_data_channel_fall = calib_data_channel[calib_data_channel.toa_type==-1]


	##############################
	# read the calibrated values #
	##############################

	#first the TOA-rise
	a0_TOA_rise = np.array(calib_data_channel_rise.a0_TOA)[0]
	a1_TOA_rise = np.array(calib_data_channel_rise.a1_TOA)[0]
	b0_TOA_rise = np.array(calib_data_channel_rise.b0_TOA)[0]
	c0_TOA_rise = np.array(calib_data_channel_rise.c0_TOA)[0]
	c1_TOA_rise = np.array(calib_data_channel_rise.c1_TOA)[0]
	d0_TOA_rise = np.array(calib_data_channel_rise.d0_TOA)[0]
	d1_TOA_rise = np.array(calib_data_channel_rise.d1_TOA)[0]
	opt_TOA_rise = (a0_TOA_rise, a1_TOA_rise, b0_TOA_rise, c0_TOA_rise, c1_TOA_rise, d0_TOA_rise, d1_TOA_rise)

	a0_TW_rise = np.array(calib_data_channel_rise.a0_TW)[0]
	a1_TW_rise = np.array(calib_data_channel_rise.a1_TW)[0]
	b0_TW_rise = np.array(calib_data_channel_rise.b0_TW)[0]
	c0_TW_rise = np.array(calib_data_channel_rise.c0_TW)[0]
	c1_TW_rise = np.array(calib_data_channel_rise.c1_TW)[0]
	d0_TW_rise = np.array(calib_data_channel_rise.d0_TW)[0]
	d1_TW_rise = np.array(calib_data_channel_rise.d1_TW)[0]
	opt_TW_rise = (a0_TW_rise, a1_TW_rise, b0_TW_rise, c0_TW_rise, c1_TW_rise, d0_TW_rise, d1_TW_rise)

	b_TW_residual_rise = np.array(calib_data_channel_rise.b_TW_residual)[0]
	m_TW_residual_rise = np.array(calib_data_channel_rise.m_TW_residual)[0]

	a_TW_Esum_rise = np.array(calib_data_channel_rise.a_TW_Esum)[0]
	b_TW_Esum_rise = np.array(calib_data_channel_rise.b_TW_Esum)[0]
	c_TW_Esum_rise = np.array(calib_data_channel_rise.c_TW_Esum)[0]
	d_TW_Esum_rise = np.array(calib_data_channel_rise.d_TW_Esum)[0]
	e_TW_Esum_rise = np.array(calib_data_channel_rise.e_TW_Esum)[0]
	opt_TW_Esum_rise = (a_TW_Esum_rise, b_TW_Esum_rise, c_TW_Esum_rise, d_TW_Esum_rise, e_TW_Esum_rise)

	l0_rise = np.array(calib_data_channel_rise.l0)[0]


	#then the TOA-fall
	a0_TOA_fall = np.array(calib_data_channel_fall.a0_TOA)[0]
	a1_TOA_fall = np.array(calib_data_channel_fall.a1_TOA)[0]
	b0_TOA_fall = np.array(calib_data_channel_fall.b0_TOA)[0]
	c0_TOA_fall = np.array(calib_data_channel_fall.c0_TOA)[0]
	c1_TOA_fall = np.array(calib_data_channel_fall.c1_TOA)[0]
	d0_TOA_fall = np.array(calib_data_channel_fall.d0_TOA)[0]
	d1_TOA_fall = np.array(calib_data_channel_fall.d1_TOA)[0]
	opt_TOA_fall = (a0_TOA_fall, a1_TOA_fall, b0_TOA_fall, c0_TOA_fall, c1_TOA_fall, d0_TOA_fall, d1_TOA_fall)
	
	a0_TW_fall = np.array(calib_data_channel_fall.a0_TW)[0]
	a1_TW_fall = np.array(calib_data_channel_fall.a1_TW)[0]
	b0_TW_fall = np.array(calib_data_channel_fall.b0_TW)[0]
	c0_TW_fall = np.array(calib_data_channel_fall.c0_TW)[0]
	c1_TW_fall = np.array(calib_data_channel_fall.c1_TW)[0]
	d0_TW_fall = np.array(calib_data_channel_fall.d0_TW)[0]
	d1_TW_fall = np.array(calib_data_channel_fall.d1_TW)[0]
	opt_TW_fall = (a0_TW_fall, a1_TW_fall, b0_TW_fall, c0_TW_fall, c1_TW_fall, d0_TW_fall, d1_TW_fall)

	b_TW_residual_fall = np.array(calib_data_channel_fall.b_TW_residual)[0]
	m_TW_residual_fall = np.array(calib_data_channel_fall.m_TW_residual)[0]

	a_TW_Esum_fall = np.array(calib_data_channel_fall.a_TW_Esum)[0]
	b_TW_Esum_fall = np.array(calib_data_channel_fall.b_TW_Esum)[0]
	c_TW_Esum_fall = np.array(calib_data_channel_fall.c_TW_Esum)[0]
	d_TW_Esum_fall = np.array(calib_data_channel_fall.d_TW_Esum)[0]
	e_TW_Esum_fall = np.array(calib_data_channel_fall.e_TW_Esum)[0]
	opt_TW_Esum_fall = (a_TW_Esum_fall, b_TW_Esum_fall, c_TW_Esum_fall, d_TW_Esum_fall, e_TW_Esum_fall)

	l0_fall = np.array(calib_data_channel_fall.l0)[0]

		
	#compute the calibrated hit timestamps
	calib_hit_times_rise = l0_rise + func_TOA(df_random["toa_rise"], *opt_TOA_rise) + func_TW(df_random["hit_energy"], *opt_TW_rise) + func_Esum_TW(df_random["E_sum_layer"], df_random["hit_energy"], *opt_TW_Esum_rise) + (b_TW_residual_rise + m_TW_residual_rise*df_random["hit_energy"])
	calib_hit_times_fall = l0_fall + func_TOA(df_random["toa_fall"], *opt_TOA_fall) + func_TW(df_random["hit_energy"], *opt_TW_fall) + func_Esum_TW(df_random["E_sum_layer"], df_random["hit_energy"], *opt_TW_Esum_fall) + (b_TW_residual_fall + m_TW_residual_fall*df_random["hit_energy"])

	df_random = df_random.assign(TCorrected_rise=calib_hit_times_rise - calib_hit_times_rise.median() + 17.5)
	df_random = df_random.assign(TCorrected_fall=calib_hit_times_fall - calib_hit_times_fall.median() + 17.5)


	df_random_merged.append(df_random)

#merge all dataframes and write them out to file
df_random_merged = pd.concat(df_random_merged)
outstore = pd.HDFStore(args.outputDataPath)
outstore["step1"] = df_random_merged
outstore.close()