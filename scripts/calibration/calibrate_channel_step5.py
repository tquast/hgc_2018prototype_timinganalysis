#Created: 22 Jan 2021
#Last modified: 22 Jan 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#calibration parameters defined in config/analysis.py
####
#5. Determine global offset and summarise the qualtity of the calibration
####
#run with python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/calibration/calibrate_channel_step5.py --inputFileCalib /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step4.h5 --step2File /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step2.root --step3File /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step3.root --step4File /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step4.root --outputFilePath /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step5.h5 --channelKey 32236

import pandas as pd
import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)

from helpers.functions import func_TOA, NPARAMETERS_FTOA
from helpers.functions import func_TW, NPARAMETERS_FTW
from helpers.functions import func_Esum_TW, NPARAMETERS_FTWESUM

from config.analysis import CALIBRATIONPARAMETERS
NBINS_TOA_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TOA_PRECALIB"]
NBINS_TW_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TW_PRECALIB"]
MINENERGYFORTWCALIB = CALIBRATIONPARAMETERS["MINENERGYFORTWCALIB"]
NBINS_TWESUM_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TWESUM_PRECALIB"]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step4.h5", required=True)
parser.add_argument('--step2File', type=str, help='Input file from calibration step 2', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step2.root", required=True)
parser.add_argument('--step3File', type=str, help='Input file from calibration step 3', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step3.root", required=True)
parser.add_argument('--step4File', type=str, help='Input file from calibration step 4', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step4.root", required=True)
parser.add_argument("--channelKey", type=int, help="Channel key", default=0, required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibration", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/channel_0.root", required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated data", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/channel_0.h5", required=True)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#read the data
instore = pd.HDFStore(args.inputFileCalib)
loaded_samples = instore["step1"]
instore.close()


#read the calibrated parameters
step2_file = ROOT.TFile(args.step2File, "READ")
func_TOA_rise = step2_file.Get("func_TOA_toa_rise")
func_TOA_fall = step2_file.Get("func_TOA_toa_fall")
func_TOA_params = {}
func_TOA_params["rise"] = [func_TOA_rise.GetParameter(i) for i in range(NPARAMETERS_FTOA)]
func_TOA_params["fall"] = [func_TOA_fall.GetParameter(i) for i in range(NPARAMETERS_FTOA)]
step2_file.Close()

step3_file = ROOT.TFile(args.step3File, "READ")
func_TW_rise = step3_file.Get("func_TW_toa_rise")
func_TW_fall = step3_file.Get("func_TW_toa_fall")
func_TW_params = {}
func_TW_params["rise"] = [func_TW_rise.GetParameter(i) for i in range(NPARAMETERS_FTW)]
func_TW_params["fall"] = [func_TW_fall.GetParameter(i) for i in range(NPARAMETERS_FTW)]
step3_file.Close()

step4_file = ROOT.TFile(args.step4File, "READ")
func_TWESUM_rise = step4_file.Get("func_TWESUM_toa_rise")
func_TWESUM_fall = step4_file.Get("func_TWESUM_toa_fall")
func_TWESUM_params = {}
func_TWESUM_params["rise"] = [func_TWESUM_rise.GetParameter(i) for i in range(NPARAMETERS_FTWESUM)]
func_TWESUM_params["fall"] = [func_TWESUM_fall.GetParameter(i) for i in range(NPARAMETERS_FTWESUM)]
step4_file.Close()

l0 = {}


canvas = ROOT.TCanvas("canvas_step5", "canvas_step5", 3200, 900)
canvas.Divide(4,2)

def Finish_Calibration(toaType, canvas_index):
    TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]

    #load relevant information
    toa = loaded_samples["toa_%s"%toaType]
    hit_energy = loaded_samples.hit_energy
    E_sum_layer = loaded_samples.E_sum_layer
    
    E_hit_99percent_quantile = hit_energy.quantile(0.99)
    Esum_99percent_quantile = E_sum_layer.quantile(0.99)

    #compute calibrated timestamp given calibration constants
    f_TOA = func_TOA(toa, *func_TOA_params[toaType])
    f_TW = func_TW(hit_energy, *func_TW_params[toaType])
    f_TWESUM = func_Esum_TW(E_sum_layer, hit_energy, *func_TWESUM_params[toaType])
    
    precalibrated_time = f_TOA + f_TW + f_TWESUM
    ref_time = loaded_samples["TCorrected_%s" % toaType]
    
    l0[toaType] = np.median(ref_time-precalibrated_time)
    
    calibrated_time = precalibrated_time + l0[toaType]

    deltat = calibrated_time - ref_time
    
    #conversion to numpy arrays
    deltat = np.array(1000*deltat)      #conversion to ps
    toa = np.array(toa)
    hit_energy = np.array(hit_energy)
    E_sum_layer = np.array(E_sum_layer)

    h1_deltat = ROOT.TH1F("deltat_%s" % TOA_TYPE, "deltat_%s" % TOA_TYPE, 800, -1000., 1000.)
    
    h2_deltat_vs_toa = ROOT.TH2F("deltat_vs_toa_%s" % TOA_TYPE, "deltat_vs_toa_%s" % TOA_TYPE, NBINS_TOA_PRECALIB, 0., 1., 800, -1000., 1000.)
    
    h2_deltat_vs_tw = ROOT.TH2F("deltat_vs_tw_%s" % TOA_TYPE, "deltat_vs_tw_%s" % TOA_TYPE, NBINS_TW_PRECALIB, MINENERGYFORTWCALIB, E_hit_99percent_quantile, 800, -1000., 1000.)
    pol1_fit_deltat_vs_tw = ROOT.TF1("pol1_fit_deltat_vs_tw_%s" % TOA_TYPE, "pol1", MINENERGYFORTWCALIB, E_hit_99percent_quantile)
    
    h2_deltat_vs_twesum = ROOT.TH2F("deltat_vs_twesum_%s" % TOA_TYPE, "deltat_vs_twesum_%s" % TOA_TYPE, NBINS_TWESUM_PRECALIB, 0, Esum_99percent_quantile, 800, -1000., 1000.)

    for h in [h1_deltat, h2_deltat_vs_toa, h2_deltat_vs_tw, h2_deltat_vs_twesum]:
        h.SetStats(False)

    for entry_index in range(len(deltat)):
        h1_deltat.Fill(deltat[entry_index])
        h2_deltat_vs_toa.Fill(toa[entry_index], deltat[entry_index])
        h2_deltat_vs_tw.Fill(hit_energy[entry_index], deltat[entry_index])
        h2_deltat_vs_twesum.Fill(E_sum_layer[entry_index], deltat[entry_index])
    
    profile_deltat_vs_toa = h2_deltat_vs_toa.ProfileX()
    profile_deltat_vs_tw = h2_deltat_vs_tw.ProfileX()
    profile_deltat_vs_twesum = h2_deltat_vs_twesum.ProfileX()


    for p in [profile_deltat_vs_toa, profile_deltat_vs_tw, profile_deltat_vs_twesum]:
        p.SetMarkerSize(2)
        p.SetMarkerStyle(21)

    canvas.cd(canvas_index*4+1)
    h1_deltat.SetTitle("5.) #Deltat: TOA-%s"%toaType)
    h1_deltat.GetXaxis().SetTitle("#Deltat := Calibrated time - MCP - TOF [ps]")    
    h1_deltat.Draw("")
    canvas.cd(canvas_index*4+2)
    h2_deltat_vs_toa.GetYaxis().SetRangeUser(-250., 250.)
    h2_deltat_vs_toa.Draw("COLZ")
    h2_deltat_vs_toa.SetTitle("#Deltat vs. TOA")
    h2_deltat_vs_toa.GetXaxis().SetTitle("TOA-%s"%toaType)
    h2_deltat_vs_toa.GetYaxis().SetTitle("Calibrated time - MCP - TOF [ps]")
    profile_deltat_vs_toa.Draw("SAME")
    canvas.cd(canvas_index*4+3)
    h2_deltat_vs_tw.SetTitle("#Deltat vs. hit energy")
    h2_deltat_vs_tw.GetXaxis().SetTitle("Hit energy [MIP]")
    h2_deltat_vs_tw.GetYaxis().SetTitle("Calibrated time - MCP - TOF [ps]")
    h2_deltat_vs_tw.GetYaxis().SetRangeUser(-250., 250.)
    h2_deltat_vs_tw.Draw("COLZ")
    profile_deltat_vs_tw.Draw("SAME")
    #additional hit energy TW residual correction
    #profile_deltat_vs_tw.Fit(pol1_fit_deltat_vs_tw, "RQW")
    pol1_fit_deltat_vs_tw.SetParameter(0, 0.)
    pol1_fit_deltat_vs_tw.SetParameter(1, 0.)
    pol1_fit_deltat_vs_tw.Draw("SAME")
    

    canvas.cd(canvas_index*4+4)
    h2_deltat_vs_twesum.SetTitle("#Deltat vs. layer energy")
    h2_deltat_vs_twesum.GetXaxis().SetTitle("layer energy [MIP]")
    h2_deltat_vs_twesum.GetYaxis().SetTitle("Calibrated time - MCP - TOF [ps]")
    h2_deltat_vs_twesum.GetYaxis().SetRangeUser(-250., 250.)
    h2_deltat_vs_twesum.Draw("COLZ")
    profile_deltat_vs_twesum.Draw("SAME")

    return {
        "h1_deltat": h1_deltat,
        "h2_deltat_vs_toa": h2_deltat_vs_toa,
        "profile_deltat_vs_toa": profile_deltat_vs_toa,
        "h2_deltat_vs_tw": h2_deltat_vs_tw,
        "profile_deltat_vs_tw": profile_deltat_vs_tw,
        "pol1_fit_deltat_vs_tw": pol1_fit_deltat_vs_tw,
        "h2_deltat_vs_twesum": h2_deltat_vs_twesum, 
        "profile_deltat_vs_twesum": profile_deltat_vs_twesum
    }


root_objects = {}
for pad_index, toaType in enumerate(["rise", "fall"]):
	root_objects[toaType] = Finish_Calibration(toaType, pad_index)



#write out the data: graphics
canvas.SetTitle("Step 4: Module %i, Chip %i, Channel %i" % (Module, Chip, Channel))
canvas.Print(args.outputFilePath.replace(".root", ".pdf"))


#write out the data: ROOT object
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
for toaType in root_objects:
    for obj in root_objects[toaType]:
        root_objects[toaType][obj].Write()
outfile.Close()



#summarise the calibration parameters and write them out to one file
calib_data = []

for toaType in ["rise", "fall"]:
    entry = [Module, Chip, Channel, {"rise": 1, "fall": -1}[toaType]]
    column_names = ["Module", "Chip", "Channel", "toa_type"]

    entry = np.append(entry, func_TOA_params[toaType])
    column_names += ["a0_TOA", "a1_TOA", "b0_TOA", "c0_TOA", "c1_TOA", "d0_TOA", "d1_TOA"]

    entry = np.append(entry, func_TW_params[toaType])
    column_names += ["a0_TW", "a1_TW", "b0_TW", "c0_TW", "c1_TW", "d0_TW", "d1_TW"]

    #additional hit energy TW residual correction
    entry = np.append(entry, [-root_objects[toaType]["pol1_fit_deltat_vs_tw"].GetParameter(npar)/1000. for npar in range(2)])           #do not forget to scale back to ns
    column_names += ["b_TW_residual", "m_TW_residual"]

    entry = np.append(entry, func_TWESUM_params[toaType])
    column_names += ["a_TW_Esum", "b_TW_Esum", "c_TW_Esum", "d_TW_Esum", "e_TW_Esum"]

    entry = np.append(entry, [l0[toaType]])
    column_names += ["l0"]

    calib_data.append(entry)


calib_data = pd.DataFrame(calib_data, columns=column_names)
outstore = pd.HDFStore(args.outputDataPath)
outstore["calib"] = calib_data
outstore.close()