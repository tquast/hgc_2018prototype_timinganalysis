#Created: 21 Jan 2021
#Last modified: 22 Jan 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#calibration parameters defined in config/analysis.py
####
#2. (Pre-) calibration of the TOA nonlinearity before correcting the for the timewalk
####
#run with python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/calibration/calibrate_channel_step2.py --inputFileCalib /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step1.h5 --outputFilePath /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step2.h5 --channelKey 32236


from helpers.functions import func_TOA_ROOT, func_TOA, NPARAMETERS_FTOA
import pandas as pd
import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)

from config.analysis import CALIBRATIONPARAMETERS
NBINS_TOA_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TOA_PRECALIB"]
MINENERGYFORTOACALIB = CALIBRATIONPARAMETERS["MINENERGYFORTOACALIB"]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step1.h5", required=True)
parser.add_argument("--channelKey", type=int, help="Channel key", default=0, required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibration", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step2.root", required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated data", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step2.h5", required=True)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#read the data
instore = pd.HDFStore(args.inputFileCalib)
loaded_samples = instore["step1"]
instore.close()
selected_samples = loaded_samples[loaded_samples.hit_energy>=MINENERGYFORTOACALIB]

canvas = ROOT.TCanvas("canvas_step2", "canvas_step2", 1600, 900)
canvas.Divide(2,2)

def TOA_Calibration(toaType, canvas_index):
    
    TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]
    f1_toa_func = ROOT.TF1("func_TOA_%s" % TOA_TYPE, func_TOA_ROOT, 0., 1., NPARAMETERS_FTOA)
    h2_t_vs_toa = ROOT.TH2F("t_vs_toa_%s" % TOA_TYPE, "t_vs_toa_%s" % TOA_TYPE, NBINS_TOA_PRECALIB, 0., 1., 350, -2., 33.)
    h2_t_vs_toa.SetStats(False)

    #TOA non-linearity
    canvas.cd(canvas_index*2+1)
    canvas.GetPad(canvas_index*2+1).SetGrid(True)

    #fill scatter
    x = np.array(selected_samples[TOA_TYPE])
    y = np.array(selected_samples["TCorrected_%s" % toaType])
    for entry_index in range(len(x)):
        h2_t_vs_toa.Fill(x[entry_index], y[entry_index])

    #compute profile
    profile_t_vs_toa = h2_t_vs_toa.ProfileX()
    
    #fitting
    f1_toa_func.SetParameters(-15., -15., 30., 4., 4., 1.01, 1.01)
    for npar, parname in enumerate(["a1", "a2", "b1", "c1", "c2", "d1", "d2"]):
        f1_toa_func.SetParName(npar, parname)
    for iteration in range(10):
        profile_t_vs_toa.Fit(f1_toa_func, "NQ")

    #draw everything
    profile_t_vs_toa.SetMarkerStyle(20)
    profile_t_vs_toa.SetMarkerSize(2)
    
    h2_t_vs_toa.SetTitle("TOA-%s" % toaType)
    h2_t_vs_toa.GetXaxis().SetTitle("toa-%s" % toaType)
    h2_t_vs_toa.GetYaxis().SetTitle("#Delta T_{MCP}+TOF [ns]")

    h2_t_vs_toa.Draw("COLZ")
    profile_t_vs_toa.Draw("PSAME")
    f1_toa_func.Draw("SAME")
    legend = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
    legend.AddEntry(profile_t_vs_toa, "Profile (data)", "p")
    legend.AddEntry(f1_toa_func, "Fit-#chi^{2}/ndf=%.1f/%i" % (f1_toa_func.GetChisquare(), f1_toa_func.GetNDF()), "l")
    legend.Draw()

    ### Residuals
    canvas.cd(canvas_index*2+2)
    canvas.GetPad(canvas_index*2+2).SetGrid(True)
    
    gr_residuals = ROOT.TGraphErrors()
    gr_residuals.SetName("residuals_%s" % TOA_TYPE)

    for nbin in range(profile_t_vs_toa.GetNbinsX()):
        x = profile_t_vs_toa.GetBinCenter(nbin)
        y = profile_t_vs_toa.GetBinContent(nbin)
        y_err = profile_t_vs_toa.GetBinError(nbin)
        y_ref = f1_toa_func.Eval(x)
        gr_residuals.SetPoint(nbin, x, 1000*(y-y_ref))
        gr_residuals.SetPointError(nbin, 0, 1000*y_err)
    gr_residuals.SetMarkerStyle(20)
    gr_residuals.SetMarkerSize(2)
    gr_residuals.SetTitle("Residuals, TOA-%s" % toaType)
    gr_residuals.GetXaxis().SetTitle("TOA-%s" % toaType)
    gr_residuals.GetYaxis().SetTitle("Data - Fit [ps]")
    gr_residuals.GetYaxis().SetRangeUser(-200., 200.)
    gr_residuals.Draw("AP")

    return {
        "toa_func": f1_toa_func,
        "2d_distr": h2_t_vs_toa,
        "profile": profile_t_vs_toa,
        "residuals": gr_residuals, 
        "legend": legend
    }


root_objects = {}
for pad_index, toaType in enumerate(["rise", "fall"]):
	root_objects[toaType] = TOA_Calibration(toaType, pad_index)

#write out updated pandas dataframes
func_TOA_rise_params = [root_objects["rise"]["toa_func"].GetParameter(i) for i in range(NPARAMETERS_FTOA)]
func_TOA_fall_params = [root_objects["fall"]["toa_func"].GetParameter(i) for i in range(NPARAMETERS_FTOA)]
loaded_samples = loaded_samples.assign(f_TOA_rise = func_TOA(loaded_samples.toa_rise, *func_TOA_rise_params))
loaded_samples = loaded_samples.assign(f_TOA_fall = func_TOA(loaded_samples.toa_fall, *func_TOA_fall_params))

outstore = pd.HDFStore(args.outputDataPath)
outstore["step2"] = loaded_samples
outstore.close()

#write out the data: graphics
canvas.SetTitle("Step 2: Module %i, Chip %i, Channel %i" % (Module, Chip, Channel))
canvas.Print(args.outputFilePath.replace(".root", ".pdf"))

#write out the data: ROOT object
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
for toaType in root_objects:
    for obj in root_objects[toaType]:
        root_objects[toaType][obj].Write()
outfile.Close()