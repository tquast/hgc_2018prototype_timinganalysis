#Created: 22 Jan 2021
#Last modified: 22 Jan 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#calibration parameters defined in config/analysis.py
####
#3. (Pre-) calibration of the TOF-related timewalk, i.e. dependence on the overall shower energy (~signal sum).
####
#run with python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/calibration/calibrate_channel_step3.py --inputFileCalib /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step2.h5 --outputFilePath /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step3.h5 --channelKey 32236


from helpers.functions import func_TW_ROOT, func_TW, NPARAMETERS_FTW
import pandas as pd
import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)

from config.analysis import CALIBRATIONPARAMETERS
NBINS_TW_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TW_PRECALIB"]
MINENERGYFORTWCALIB = CALIBRATIONPARAMETERS["MINENERGYFORTWCALIB"]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step2.h5", required=True)
parser.add_argument("--channelKey", type=int, help="Channel key", default=0, required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibration", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step3.root", required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated data", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step3.h5", required=True)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#read the data
instore = pd.HDFStore(args.inputFileCalib)
loaded_samples = instore["step2"]
instore.close()
selected_samples = loaded_samples[loaded_samples.hit_energy>=MINENERGYFORTWCALIB]

canvas = ROOT.TCanvas("canvas_step3", "canvas_step3", 1600, 900)
canvas.Divide(2,2)

def TW_Calibration(toaType, canvas_index):
    E_hit_99percent_quantile =selected_samples.hit_energy.quantile(0.99)

    TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]
    f1_tw_func = ROOT.TF1("func_TW_%s" % TOA_TYPE, func_TW_ROOT, MINENERGYFORTWCALIB, E_hit_99percent_quantile, NPARAMETERS_FTW)
    h2_t_vs_tw = ROOT.TH2F("t_vs_tw_%s" % TOA_TYPE, "t_vs_tw_%s" % TOA_TYPE, NBINS_TW_PRECALIB, MINENERGYFORTWCALIB, E_hit_99percent_quantile, 640, -8., 8.)
    h2_t_vs_tw.SetStats(False)

    #TW calibration
    canvas.cd(canvas_index*2+1)
    canvas.GetPad(canvas_index*2+1).SetGrid(True)

    #fill scatter
    x = np.array(selected_samples.hit_energy)
    y = np.array(selected_samples["f_TOA_%s" % toaType])
    y_ref = np.array(selected_samples["TCorrected_%s" % toaType])
    for entry_index in range(len(x)):
        h2_t_vs_tw.Fill(x[entry_index], y_ref[entry_index]-y[entry_index])

    #compute profile
    profile_t_vs_tw = h2_t_vs_tw.ProfileX()
    
    #fitting
    f1_tw_func.SetParameters(0., 0.0, 0.1, -400., -400., -40., -40.)
    for npar, parname in enumerate(["a1", "a2", "b1", "c1", "c2", "d1", "d2"]):
        f1_tw_func.SetParName(npar, parname)
    for iteration in range(10):
        profile_t_vs_tw.Fit(f1_tw_func, "NQ")

    #draw everything
    profile_t_vs_tw.SetMarkerStyle(20)
    profile_t_vs_tw.SetMarkerSize(2)
    
    h2_t_vs_tw.SetTitle("3.) Hit energy TW: TOA-%s" % toaType)
    h2_t_vs_tw.GetXaxis().SetTitle("Hit energy [MIPs]")
    h2_t_vs_tw.GetYaxis().SetTitle("#Delta T_{MCP}+TOF - f_{TOA} [ns]")

    h2_t_vs_tw.Draw("COLZ")
    profile_t_vs_tw.Draw("PSAME")
    f1_tw_func.Draw("SAME")
    legend = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
    legend.AddEntry(profile_t_vs_tw, "Profile (data)", "p")
    legend.AddEntry(f1_tw_func, "Fit-#chi^{2}/ndf=%.1f/%i" % (f1_tw_func.GetChisquare(), f1_tw_func.GetNDF()), "l")
    legend.Draw()

    ### Residuals
    canvas.cd(canvas_index*2+2)
    canvas.GetPad(canvas_index*2+2).SetGrid(True)
    
    gr_residuals = ROOT.TGraphErrors()
    gr_residuals.SetName("residuals_%s" % TOA_TYPE)

    for nbin in range(profile_t_vs_tw.GetNbinsX()):
        x = profile_t_vs_tw.GetBinCenter(nbin)
        y = profile_t_vs_tw.GetBinContent(nbin)
        y_err = profile_t_vs_tw.GetBinError(nbin)
        y_ref = f1_tw_func.Eval(x)
        gr_residuals.SetPoint(nbin, x, 1000*(y-y_ref))
        gr_residuals.SetPointError(nbin, 0, 1000*y_err)
    gr_residuals.SetMarkerStyle(20)
    gr_residuals.SetMarkerSize(2)
    gr_residuals.SetTitle("Residuals, Hit energy TW, TOA-%s" % toaType)
    gr_residuals.GetXaxis().SetTitle("Hit energy [MIP]")
    gr_residuals.GetYaxis().SetTitle("Data - Fit [ps]")
    gr_residuals.GetYaxis().SetRangeUser(-200., 200.)
    gr_residuals.Draw("AP")

    return {
        "tw_func": f1_tw_func,
        "2d_distr": h2_t_vs_tw,
        "profile": profile_t_vs_tw,
        "residuals": gr_residuals, 
        "legend": legend
    }


root_objects = {}
for pad_index, toaType in enumerate(["rise", "fall"]):
	root_objects[toaType] = TW_Calibration(toaType, pad_index)



#write out updated pandas dataframes
func_TW_rise_params = [root_objects["rise"]["tw_func"].GetParameter(i) for i in range(NPARAMETERS_FTW)]
func_TW_fall_params = [root_objects["fall"]["tw_func"].GetParameter(i) for i in range(NPARAMETERS_FTW)]
loaded_samples = loaded_samples.assign(f_TW_rise = func_TW(loaded_samples.hit_energy, *func_TW_rise_params))
loaded_samples = loaded_samples.assign(f_TW_fall = func_TW(loaded_samples.hit_energy, *func_TW_fall_params))

outstore = pd.HDFStore(args.outputDataPath)
outstore["step3"] = loaded_samples
outstore.close()


#write out the data: graphics
canvas.SetTitle("Step 3: Module %i, Chip %i, Channel %i" % (Module, Chip, Channel))
canvas.Print(args.outputFilePath.replace(".root", ".pdf"))


#write out the data: ROOT object
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
for toaType in root_objects:
    for obj in root_objects[toaType]:
        root_objects[toaType][obj].Write()
outfile.Close()
