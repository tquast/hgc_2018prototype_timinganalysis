#Created: 22 Jan 2021
#Last modified: 22 Jan 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#calibration parameters defined in config/analysis.py
####
#4. (Pre-) calibration of the TOF-related timewalk, i.e. dependence on the overall shower energy (~signal sum).
####
#run with python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/calibration/calibrate_channel_step4.py --inputFileCalib /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step3.h5 --outputFilePath /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step4.h5 --channelKey 32236


from helpers.functions import func_Esum_TW, NPARAMETERS_FTWESUM

import pandas as pd
import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)

from config.analysis import CALIBRATIONPARAMETERS, TOT_TURNON_MIPS
NBINS_TWESUM_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TWESUM_PRECALIB"]
MINENERGYFORTWCALIB = CALIBRATIONPARAMETERS["MINENERGYFORTWCALIB"]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step3.h5", required=True)
parser.add_argument("--channelKey", type=int, help="Channel key", default=0, required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibration", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step4.root", required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated data", default="/home/tquast/hgc_prototype2018_timinganalysis_v1.5/calibration/all_average_step4.h5", required=True)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#read the data
instore = pd.HDFStore(args.inputFileCalib)
loaded_samples = instore["step3"]
instore.close()
selected_samples = loaded_samples[loaded_samples.hit_energy>=MINENERGYFORTWCALIB]       #same requirements as for the hit energy TW calibration
selected_samples = selected_samples[selected_samples.hit_energy<TOT_TURNON_MIPS]       #same requirements as for the hit energy TW calibration


canvas = ROOT.TCanvas("canvas_step4", "canvas_step4", 1600, 900)
canvas.Divide(2,2)

def TWESUM_Calibration(toaType, canvas_index):
    Esum_99percent_quantile = selected_samples.E_sum_layer.quantile(0.99)

    TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]
    f1_twesum_func = ROOT.TF1("func_TWESUM_%s" % TOA_TYPE, "pol%i" % (NPARAMETERS_FTWESUM-1), 0, Esum_99percent_quantile)
    h2_t_vs_twesum = ROOT.TH2F("t_vs_twesum_%s" % TOA_TYPE, "t_vs_twesum_%s" % TOA_TYPE, NBINS_TWESUM_PRECALIB, 0, Esum_99percent_quantile, 80, -1., 1.)
    h2_t_vs_twesum.SetStats(False)

    #TW calibration
    canvas.cd(canvas_index*2+1)
    canvas.GetPad(canvas_index*2+1).SetGrid(True)

    #fill scatter
    x = np.array(selected_samples.E_sum_layer)
    y_TOA = np.array(selected_samples["f_TOA_%s" % toaType])
    y_tw = np.array(selected_samples["f_TW_%s" % toaType])
    y_ref = np.array(selected_samples["TCorrected_%s" % toaType])
    for entry_index in range(len(x)):
        h2_t_vs_twesum.Fill(x[entry_index], y_ref[entry_index]- (y_TOA[entry_index] + y_tw[entry_index]))

    #compute profile
    profile_t_vs_twesum = h2_t_vs_twesum.ProfileX()
    
    #fitting
    for iteration in range(10):
        profile_t_vs_twesum.Fit(f1_twesum_func, "NQ")

    #draw everything
    profile_t_vs_twesum.SetMarkerStyle(20)
    profile_t_vs_twesum.SetMarkerSize(2)
    
    h2_t_vs_twesum.SetTitle("4.) Layer energy TW: TOA-%s" % toaType)
    h2_t_vs_twesum.GetXaxis().SetTitle("Layer energy [MIPs]")
    h2_t_vs_twesum.GetYaxis().SetTitle("#Delta T_{MCP}+TOF - f_{TOA} - f_{TW}[ns]")

    h2_t_vs_twesum.Draw("COLZ")
    profile_t_vs_twesum.Draw("PSAME")
    f1_twesum_func.Draw("SAME")
    legend = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
    legend.AddEntry(profile_t_vs_twesum, "Profile (data)", "p")
    legend.AddEntry(f1_twesum_func, "Fit-#chi^{2}/ndf=%.1f/%i" % (f1_twesum_func.GetChisquare(), f1_twesum_func.GetNDF()), "l")
    legend.Draw()

    ### Residuals
    canvas.cd(canvas_index*2+2)
    canvas.GetPad(canvas_index*2+2).SetGrid(True)
    
    gr_residuals = ROOT.TGraphErrors()
    gr_residuals.SetName("residuals_%s" % TOA_TYPE)

    for nbin in range(profile_t_vs_twesum.GetNbinsX()):
        x = profile_t_vs_twesum.GetBinCenter(nbin)
        y = profile_t_vs_twesum.GetBinContent(nbin)
        y_err = profile_t_vs_twesum.GetBinError(nbin)
        y_ref = f1_twesum_func.Eval(x)
        gr_residuals.SetPoint(nbin, x, 1000*(y-y_ref))
        gr_residuals.SetPointError(nbin, 0, 1000*y_err)
    gr_residuals.SetMarkerStyle(20)
    gr_residuals.SetMarkerSize(2)
    gr_residuals.SetTitle("Residuals, Layer energy TW, TOA-%s" % toaType)
    gr_residuals.GetXaxis().SetTitle("Layer energy [MIP]")
    gr_residuals.GetYaxis().SetTitle("Data - Fit [ps]")
    gr_residuals.GetYaxis().SetRangeUser(-200., 200.)
    gr_residuals.Draw("AP")

    return {
        "twesum_func": f1_twesum_func,
        "2d_distr": h2_t_vs_twesum,
        "profile": profile_t_vs_twesum,
        "residuals": gr_residuals, 
        "legend": legend
    }


root_objects = {}
for pad_index, toaType in enumerate(["rise", "fall"]):
	root_objects[toaType] = TWESUM_Calibration(toaType, pad_index)



#write out updated pandas dataframes
func_TWESUM_rise_params = [root_objects["rise"]["twesum_func"].GetParameter(i) for i in range(NPARAMETERS_FTWESUM)]
func_TWESUM_fall_params = [root_objects["fall"]["twesum_func"].GetParameter(i) for i in range(NPARAMETERS_FTWESUM)]
loaded_samples = loaded_samples.assign(f_TWESUM_rise = func_Esum_TW(loaded_samples.E_sum_layer, loaded_samples.hit_energy, *func_TWESUM_rise_params))
loaded_samples = loaded_samples.assign(f_TWESUM_fall = func_Esum_TW(loaded_samples.E_sum_layer, loaded_samples.hit_energy, *func_TWESUM_fall_params))

outstore = pd.HDFStore(args.outputDataPath)
outstore["step4"] = loaded_samples
outstore.close()


#write out the data: graphics
canvas.SetTitle("Step 4: Module %i, Chip %i, Channel %i" % (Module, Chip, Channel))
canvas.Print(args.outputFilePath.replace(".root", ".pdf"))


#write out the data: ROOT object
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
for toaType in root_objects:
    for obj in root_objects[toaType]:
        root_objects[toaType][obj].Write()
outfile.Close()
