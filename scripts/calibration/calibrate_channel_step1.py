#Created: 21 Jan 2021
#Last modified: 10 March 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#calibration parameters defined in config/analysis.py
####
#1. Correcting for the period of 25ns in the MCP-time vs. TOA relation
####
#run e.g. via python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/calibration/calibrate_channel_step1.py --inputFileCalib /home/tquast/hgc_prototype2018_timinganalysis_v1.5/datasets/samples_for_calibration.h5 --outputFilePath /afs/cern.ch/user/t/tquast/Desktop/channel_32136_step1.h5 --channelKey 32236

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
from scipy.stats import norm, gaussian_kde
from scipy.optimize import curve_fit
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from config.analysis import CALIBRATIONPARAMETERS
MINHITENERGY = CALIBRATIONPARAMETERS["MINHITENERGY"]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration_preparation/samples_for_calibration.h5", required=True)
parser.add_argument("--channelKey", type=int, help="Channel key", default=32136, required=True)
parser.add_argument("--tightSelection", type=int, help="Run with MINHITENERGY cut", default=0, required=True)
parser.add_argument("--outputDataPath", type=str, help="Path to write out the calibrated data", default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration/channel_32136.h5", required=True)
args = parser.parse_args()

calib_data = []
Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)


################################
#code snippet adapted from
#https://scikit-learn.org/stable/auto_examples/classification/plot_lda_qda.html#sphx-glr-auto-examples-classification-plot-lda-qda-py
def plot_data_after_discrimination(ax, lda1, lda2, X, y):
	ax.scatter(X[y==0][:,0], X[y==0][:,1],color="tab:blue")
	ax.scatter(X[y==1][:,0], X[y==1][:,1],color="tab:orange")
	ax.scatter(X[y==-1][:,0], X[y==-1][:,1],color="tab:brown")

	try:
		nx, ny = 200, 100
		x_min, x_max = ax.get_xlim()
		y_min, y_max = ax.get_ylim()
		xx, yy = np.meshgrid(np.linspace(x_min, x_max, nx),
		np.linspace(y_min, y_max, ny))
		Z = None
		if lda1 is not None:
			Z = lda1.predict_proba(np.c_[xx.ravel(), yy.ravel()])
			ax.plot(lda1.means_[0][0], lda1.means_[0][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
			ax.plot(lda1.means_[1][0], lda1.means_[1][1], '*', color='yellow', markersize=15, markeredgecolor='grey')

		if lda2 is not None:
			if Z is None:
				Z = -lda2.predict_proba(np.c_[xx.ravel(), yy.ravel()])
			else:
				Z -= lda2.predict_proba(np.c_[xx.ravel(), yy.ravel()])
			ax.plot(lda2.means_[0][0], lda2.means_[0][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
			ax.plot(lda2.means_[1][0], lda2.means_[1][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
		Z = Z[:, 1].reshape(xx.shape)
		from matplotlib import colors
		ax.pcolormesh(xx, yy, Z, cmap='PRGn', norm=colors.Normalize(-1, 1.), zorder=0)
	except:
		pass



################################

#reading channels in files for calibration
store_key = "ch_%i"%args.channelKey
store = pd.HDFStore(args.inputFileCalib)
merged_data_calib = store[store_key]
store.close()

#step 0:
#select good timing data
selected_samples = merged_data_calib[(merged_data_calib.TCorrected>=0)&(merged_data_calib.TCorrected<=25.)&(merged_data_calib.pdgID==11)]
if args.tightSelection==1:
	selected_samples = selected_samples[selected_samples.hit_energy>=MINHITENERGY]
selected_samples = selected_samples.assign(TCorrected_rise = selected_samples.TCorrected)
selected_samples = selected_samples.assign(TCorrected_fall = selected_samples.TCorrected)
Nentries = len(selected_samples.hit_energy)

#prepare the figures
fig, axs = plt.subplots(2, 3, figsize=(24,10))


#step 1) correct for different clock phases, depends on toa_rise or toa_fall
def periodCorrection(toaType, i):
	TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]
	if toaType=="rise":
		#a.) perform corner-based preclustering
		bottom_left_labels=np.array((selected_samples[TOA_TYPE]<0.30)&(selected_samples.TCorrected<7)).astype(int)
		top_right_labels=np.array((selected_samples[TOA_TYPE]>0.8)&(selected_samples.TCorrected>20)).astype(int)
		corner_labels = np.zeros(len(top_right_labels))+bottom_left_labels*(-1)+top_right_labels
		selected_samples[corner_labels==0].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:blue", ax=axs[i][0])
		if np.count_nonzero(corner_labels==1)>0:
			selected_samples[corner_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:orange", ax=axs[i][0])
		if np.count_nonzero(corner_labels==-1)>0:
			selected_samples[corner_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:brown", ax=axs[i][0])
		axs[i][0].set_title('1) Period correction: %s' % toaType, fontsize=20)
		axs[i][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][0].set_ylabel("($\Delta T_{MCP}$+TOF) [ns]")


		bottom_left_fisher_labels =  np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))
		top_right_fisher_labels = np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))

		clf_bl = None
		clf_tr = None

		if np.count_nonzero(bottom_left_labels==1)>0:
			clf_bl = LinearDiscriminantAnalysis(tol=1e-9)
			clf_bl.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(bottom_left_labels))
			bottom_left_fisher_labels = clf_bl.predict(selected_samples[[TOA_TYPE, "TCorrected"]])
			bottom_left_fisher_labels = np.minimum(bottom_left_fisher_labels+bottom_left_labels, len(bottom_left_labels)*[1.])

		
		if np.count_nonzero(top_right_labels==1)>0:
			clf_tr = LinearDiscriminantAnalysis(tol=1e-9)
			clf_tr.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(top_right_labels))
			top_right_fisher_labels = clf_tr.predict(selected_samples[[TOA_TYPE, "TCorrected"]])
			top_right_fisher_labels = np.minimum(top_right_fisher_labels + top_right_labels, len(top_right_labels)*[1.])

		corner_fisher_labels = np.zeros(len(top_right_fisher_labels))+bottom_left_fisher_labels*(-1)+top_right_fisher_labels
		
		plot_data_after_discrimination(axs[i][1], clf_bl, clf_tr, np.array(selected_samples[[TOA_TYPE, "TCorrected"]]), corner_labels)

		axs[i][1].set_title('Fisher Discriminant Analysis')
		axs[i][1].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][1].set_ylabel("($\Delta T_{MCP}$+TOF) [ns]")



		#c.) perform the shift
		selected_samples[corner_fisher_labels==0].plot.scatter(y="TCorrected_rise", x=TOA_TYPE, c="tab:blue", ax=axs[i][2])
		if np.count_nonzero(corner_fisher_labels==1)>0:
			selected_samples.loc[corner_fisher_labels==1, "TCorrected_rise"] -= 25.		#top-right
			selected_samples[corner_fisher_labels==1].plot.scatter(y="TCorrected_rise", x=TOA_TYPE, c="darkmagenta", ax=axs[i][2])
		if np.count_nonzero(corner_fisher_labels==-1)>0:
			selected_samples.loc[corner_fisher_labels==-1, "TCorrected_rise"] += 25.	#bottom-left
			selected_samples[corner_fisher_labels==-1].plot.scatter(y="TCorrected_rise", x=TOA_TYPE, c="darkgreen", ax=axs[i][2])
		axs[i][2].set_title('After period correction')
		axs[i][2].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][2].set_ylabel("(Period-corrected) ($\Delta T_{MCP}$+TOF) [ns]")



	elif toaType=="fall":
		#a.) perform corner-based preclustering
		bottom_left_labels=np.array(25*(1.-selected_samples[TOA_TYPE])>selected_samples.TCorrected).astype(int)
		top_right_labels=np.array(25*(1.-selected_samples[TOA_TYPE])<selected_samples.TCorrected).astype(int)
		corner_labels = bottom_left_labels*(-1)+top_right_labels
		if np.count_nonzero(corner_labels==1)>0:
			selected_samples[corner_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:orange", ax=axs[i][0])
		if np.count_nonzero(corner_labels==-1)>0:
			selected_samples[corner_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:brown", ax=axs[i][0])
		axs[i][0].set_title('1) Period correction: %s' % toaType, fontsize=20)
		axs[i][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][0].set_ylabel("($\Delta T_{MCP}$+TOF) [ns]")


		bottom_left_fisher_labels =  np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))
		top_right_fisher_labels = np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))

		clf_bl = None
		clf_tr = None

		if np.count_nonzero(bottom_left_labels==1)>0:
			clf_bl = LinearDiscriminantAnalysis()
			clf_bl.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(bottom_left_labels))
			bottom_left_fisher_labels = clf_bl.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		
		if np.count_nonzero(top_right_labels==1)>0:
			clf_tr = LinearDiscriminantAnalysis()
			clf_tr.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(top_right_labels))
			top_right_fisher_labels = clf_tr.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		corner_fisher_labels = +bottom_left_fisher_labels*(-1)+top_right_fisher_labels
		
		plot_data_after_discrimination(axs[i][1], clf_bl, clf_tr, np.array(selected_samples[[TOA_TYPE, "TCorrected"]]), corner_labels)

		axs[i][1].set_title('Fisher Discriminant Analysis')
		axs[i][1].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][1].set_ylabel("($\Delta T_{MCP}$+TOF) [ns]")


		#c.) perform the shift
		if np.count_nonzero(corner_fisher_labels==1)>0:
			selected_samples.loc[corner_fisher_labels==1, "TCorrected_fall"] -= 12.5		#top-right
			selected_samples[corner_fisher_labels==1].plot.scatter(y="TCorrected_fall", x=TOA_TYPE, c="darkmagenta", ax=axs[i][2])
		if np.count_nonzero(corner_fisher_labels==-1)>0:
			selected_samples.loc[corner_fisher_labels==-1, "TCorrected_fall"] += 12.5	#bottom-left
			selected_samples[corner_fisher_labels==-1].plot.scatter(y="TCorrected_fall", x=TOA_TYPE, c="darkgreen", ax=axs[i][2])
		axs[i][2].set_title('After period correction')
		axs[i][2].set_xlabel("toa-"+toaType+" (normalised)")
		axs[i][2].set_ylabel("(Period-corrected) ($\Delta T_{MCP}$+TOF) [ns]")


for pad_index, toaType in enumerate(["rise", "fall"]):
	periodCorrection(toaType, pad_index)

fig.suptitle("Step 1: Module %i, Chip %i, Channel %i" % (Module, Chip, Channel), fontsize=30)
fig.savefig(args.outputDataPath.replace(".h5", ".png"))

#skim dataframe to be written to file
selected_samples = selected_samples.drop(columns=["event", "run", "pdgID", "beamEnergy", "N_hits_FH", "TMCP1", "TCorrected", "dwc_bx", "dwc_by", "hit_x", "hit_y", "hit_z", "toa_rise_norm", "toa_fall_norm", "ampMCP1", "N_hits_toa_layer", "unique_event_index"])
#remaining entries: 
#E_sum_layer  channelkey  hit_energy  toa_rise  toa_fall  TCorrected_rise  TCorrected_fall
outstore = pd.HDFStore(args.outputDataPath)
outstore["step1"] = selected_samples
outstore.close()

