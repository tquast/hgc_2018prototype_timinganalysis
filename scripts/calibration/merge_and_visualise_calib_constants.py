#Created: 12 May 2020
#Last modified: 22 January 2021
#author: Thorben Quast, thorben.quast@cern.ch
#1. selects calibration constants with reliable outcome
#2. merges them into one file for later processing

from config.analysis import CALIBRATIONMERGINGPARAMETERS
TOANORMFORREFERENCE = CALIBRATIONMERGINGPARAMETERS["TOANORM_FOR_REFERENCE"]			#0.5
HITENERGYFORREFERENCE = CALIBRATIONMERGINGPARAMETERS["HITENERGY_FOR_REFERENCE"]		#500
ELAYERFORREFERENCE = CALIBRATIONMERGINGPARAMETERS["ELAYER_FOR_REFERENCE"]			#600
MINTIMEREF = CALIBRATIONMERGINGPARAMETERS["MIN_TIMEREF"]					#5
MAXTIMEREF = CALIBRATIONMERGINGPARAMETERS["MAX_TIMEREF"]					#35
MAXDEVIATIONTIMEREF = CALIBRATIONMERGINGPARAMETERS["MAX_DEVIATION_TIMEREF"]					#1.5
BADCHANNELS_HANDSELECTED = CALIBRATIONMERGINGPARAMETERS["BADCHANNELS_HANDSELECTED"]					#[]

from helpers.functions import func_TOA, func_TW, func_Esum_TW

import os, shutil
import pandas as pd
import numpy as np
from tqdm import tqdm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import seaborn as sns

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFiles', nargs='+', help='Input files', required=True)
parser.add_argument("--mergedFile", type=str, help="File path to store the merged calibration file", default="", required=True)
parser.add_argument("--offsetFigure", type=str, help="File path to store the figure illustrating the offsets", default="", required=True)
parser.add_argument("--copyGoodCalibFigures", type=bool, help="Copying of good calibration results into separate folder", default=True, required=False)
args = parser.parse_args()

#create directory for output figures
figure_directory = os.path.dirname(args.offsetFigure)
if not os.path.exists(figure_directory):
	os.mkdir(figure_directory)

#reading channel-wise calibration data and merge them into one dataframe
calib_data = []
print "Merging the calibration constants..."
for infilepath in tqdm(args.inputFiles, unit="files"):
	calibstore = pd.HDFStore(infilepath)
	store_key = "calib"
	calib_data.append(calibstore[store_key].copy())

calib_data = pd.concat(calib_data)   
calib_data = calib_data.assign(unique_index=1000*calib_data.Module+100*calib_data.Chip+calib_data.Channel)


#1. compute and visualise offset

toa_norm_eval = np.array([TOANORMFORREFERENCE])
a0_TOA = calib_data.a0_TOA
a1_TOA = calib_data.a1_TOA
b0_TOA = calib_data.b0_TOA
c0_TOA = calib_data.c0_TOA
c1_TOA = calib_data.c1_TOA
d0_TOA = calib_data.d0_TOA
d1_TOA = calib_data.d1_TOA

hit_energy_eval = np.array([HITENERGYFORREFERENCE])
a0_TW = calib_data.a0_TW
a1_TW = calib_data.a1_TW
b0_TW = calib_data.b0_TW
c0_TW = calib_data.c0_TW
c1_TW = calib_data.c1_TW
d0_TW = calib_data.d0_TW
d1_TW = calib_data.d1_TW

b_TW_residual = calib_data.b_TW_residual
m_TW_residual = calib_data.m_TW_residual

E_layer_eval = np.array([ELAYERFORREFERENCE])
a_TW_Esum = calib_data.a_TW_Esum
b_TW_Esum = calib_data.b_TW_Esum
c_TW_Esum = calib_data.c_TW_Esum
d_TW_Esum = calib_data.d_TW_Esum
e_TW_Esum = calib_data.e_TW_Esum

l0 = calib_data.l0

calib_hit_time = func_TOA(toa_norm_eval, a0_TOA, a1_TOA, b0_TOA, c0_TOA, c1_TOA, d0_TOA, d1_TOA)
calib_hit_time += func_TW(hit_energy_eval, a0_TW, a1_TW, b0_TW, c0_TW, c1_TW, d0_TW, d1_TW) + b_TW_residual + m_TW_residual * hit_energy_eval
calib_hit_time += func_Esum_TW(E_layer_eval, hit_energy_eval, a_TW_Esum, b_TW_Esum, c_TW_Esum, d_TW_Esum, e_TW_Esum) 
calib_hit_time += l0

#1. perform selection based on reference time
calib_data = calib_data.assign(ref_times_calib=calib_hit_time)
reduced_calib_data = calib_data
reduced_calib_data = reduced_calib_data[reduced_calib_data.ref_times_calib>=MINTIMEREF]
reduced_calib_data = reduced_calib_data[reduced_calib_data.ref_times_calib<=MAXTIMEREF]

plt.figure(figsize=(30,10))
#conversion to integer to circumvent floating point representation in plot
reduced_calib_data.Module = reduced_calib_data.Module.astype(np.int32)
reduced_calib_data.Chip = reduced_calib_data.Chip.astype(np.int32)
ax = sns.boxplot(x="Module", y="ref_times_calib", hue="Chip", data=reduced_calib_data)
for item in ([ax.title, ax.yaxis.label] +
             ax.get_yticklabels()):
    item.set_fontsize(30)
for item in ([ax.xaxis.label] + ax.get_xticklabels()):	    
	item.set_fontsize(30)
plt.title("Calibrated hit time $(T_{hit})$ with $TOA_{norm}$:=%f, $E_{hit}$:=%i MIP, $E_{layer}$:=%i MIP" % (TOANORMFORREFERENCE, HITENERGYFORREFERENCE, ELAYERFORREFERENCE), fontsize=40)
plt.ylabel("$T_{hit}$ [ns]")
plt.ylim(14., 24.)
plt.grid(True)
plt.legend(loc=2, prop={'size': 20})
plt.savefig(args.offsetFigure)


#1. perform selection based on spread of reference time
selected_calib_data = []

for index, grouped_data in reduced_calib_data.groupby(reduced_calib_data.Module*100+reduced_calib_data.Chip):
	mean_ref_time = np.mean(grouped_data.ref_times_calib)
	std_ref_time = np.std(grouped_data.ref_times_calib)
	grouped_data = grouped_data[np.abs(grouped_data.ref_times_calib-mean_ref_time)<MAXDEVIATIONTIMEREF*std_ref_time]
	
	for _index, _grouped_data in grouped_data.groupby(grouped_data.Channel):		#require for a channel to have TOA-rise and -fall calibrated --> two entries
		if len(_grouped_data)!=2:
			continue
		chkey = _grouped_data.unique_index.unique()[0]
		if chkey in BADCHANNELS_HANDSELECTED:
			print("Hand-rejected channel", chkey)
			continue
		else:
			selected_calib_data.append(_grouped_data)

selected_calib_data = pd.concat(selected_calib_data)

print("Number of calibrated channels:", len(calib_data.unique_index.unique()))
print("Number of calibrated channels passing absolute reference time requirement:", len(reduced_calib_data.unique_index.unique()))
print("Number of calibrated channels passing all requirements regarding reference time:", len(selected_calib_data.unique_index.unique()))

#2a. write out the merged calibration data
full_calib_store =  pd.HDFStore(args.mergedFile)
full_calib_store["calib"] = selected_calib_data
full_calib_store.close()

#2b. if applicable copy calibration figures of good channels into 
if args.copyGoodCalibFigures:
	copyDir = os.path.join(figure_directory, "calibFiguresCopy")
	if not os.path.exists(copyDir):
		os.mkdir(copyDir)
	from tasks.calibration import CalibrateTimingForChannel
	for chkey in selected_calib_data.unique_index.unique():	
		fpath = CalibrateTimingForChannel(channelKey=chkey).output()["validation"].path.replace(".root", ".pdf")
		shutil.copy(fpath, copyDir)