#Created: 21 Feb 2020
#Last modified: 10 March 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Applies the derived calibration constants to derive hit timestamps for TOA fall and rise independently.
#The TOA fall is corrected by T/2 = 12.5ns to match the TOA rise based timestamp.
#The period of the MCP time is similarly corrected.

from config.analysis import TOA_LINEARITY_MAX
from helpers.functions import func_TOA, func_Esum_TW, func_TW

import os
import pandas as pd
import numpy as np
from tqdm import tqdm
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration_preparation/samples_for_evaluation.h5", required=True)
parser.add_argument('--calibFile', type=str, help='Input file with the calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calib_file.h5", required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibrated evaluation sample", default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=True)
parser.add_argument("--dataset", type=str, help="evaluation or calibration dataset", default="evaluation", required=True)
args = parser.parse_args()

print "Loading the data"
input_file_calibration = pd.HDFStore(args.calibFile)
calib_data = input_file_calibration["calib"]
input_file_calibration.close()
calib_data = calib_data.assign(channelkey = 1000*calib_data.Module+100*calib_data.Chip+calib_data.Channel)

# create module-layer mapping
print "Create module to layer mapping"
layer, module = np.genfromtxt(open(os.path.join(
    os.environ["WORKFLOWDIR"], "config/CondObjects_data_layer_geom_full_October2018_setup2_v1_promptReco.txt"), "r"), usecols=(0, 5), unpack=True)
module_layer_map = {}
for i in range(len(layer)):
    module_layer_map[int(module[i])] = int(layer[i])

def performCalibrationOnDataframe(sample, channel):
	#extending the calibration data by channel key to facilitate the matching to the samples
	sample = sample.assign(calibrated_hit_time_fall = -999)
	sample = sample.assign(calibrated_hit_time_rise = -999)
	sample = sample.assign(calibrated_hit_time_combined = -999)
	sample = sample.assign(average_calibration = -999)	
	
	#read the calibration for each channel
	calib_data_channel = calib_data[calib_data.channelkey==channel]
	calib_data_channel_rise = calib_data_channel[calib_data_channel.toa_type==1]
	calib_data_channel_fall = calib_data_channel[calib_data_channel.toa_type==-1]
	average_calibration = np.array(calib_data_channel.average_calibration)[1]			#index 0 would also do, but both fall and rise should be available ---> hence a good cross-check

	##############################
	# read the calibrated values #
	##############################

	#first the TOA-rise
	a0_TOA_rise = np.array(calib_data_channel_rise.a0_TOA)[0]
	a1_TOA_rise = np.array(calib_data_channel_rise.a1_TOA)[0]
	b0_TOA_rise = np.array(calib_data_channel_rise.b0_TOA)[0]
	c0_TOA_rise = np.array(calib_data_channel_rise.c0_TOA)[0]
	c1_TOA_rise = np.array(calib_data_channel_rise.c1_TOA)[0]
	d0_TOA_rise = np.array(calib_data_channel_rise.d0_TOA)[0]
	d1_TOA_rise = np.array(calib_data_channel_rise.d1_TOA)[0]
	opt_TOA_rise = (a0_TOA_rise, a1_TOA_rise, b0_TOA_rise, c0_TOA_rise, c1_TOA_rise, d0_TOA_rise, d1_TOA_rise)

	a0_TW_rise = np.array(calib_data_channel_rise.a0_TW)[0]
	a1_TW_rise = np.array(calib_data_channel_rise.a1_TW)[0]
	b0_TW_rise = np.array(calib_data_channel_rise.b0_TW)[0]
	c0_TW_rise = np.array(calib_data_channel_rise.c0_TW)[0]
	c1_TW_rise = np.array(calib_data_channel_rise.c1_TW)[0]
	d0_TW_rise = np.array(calib_data_channel_rise.d0_TW)[0]
	d1_TW_rise = np.array(calib_data_channel_rise.d1_TW)[0]
	opt_TW_rise = (a0_TW_rise, a1_TW_rise, b0_TW_rise, c0_TW_rise, c1_TW_rise, d0_TW_rise, d1_TW_rise)

	b_TW_residual_rise = np.array(calib_data_channel_rise.b_TW_residual)[0]
	m_TW_residual_rise = np.array(calib_data_channel_rise.m_TW_residual)[0]

	a_TW_Esum_rise = np.array(calib_data_channel_rise.a_TW_Esum)[0]
	b_TW_Esum_rise = np.array(calib_data_channel_rise.b_TW_Esum)[0]
	c_TW_Esum_rise = np.array(calib_data_channel_rise.c_TW_Esum)[0]
	d_TW_Esum_rise = np.array(calib_data_channel_rise.d_TW_Esum)[0]
	e_TW_Esum_rise = np.array(calib_data_channel_rise.e_TW_Esum)[0]
	opt_TW_Esum_rise = (a_TW_Esum_rise, b_TW_Esum_rise, c_TW_Esum_rise, d_TW_Esum_rise, e_TW_Esum_rise)

	l0_rise = np.array(calib_data_channel_rise.l0)[0]

	#then the TOA-fall
	a0_TOA_fall = np.array(calib_data_channel_fall.a0_TOA)[0]
	a1_TOA_fall = np.array(calib_data_channel_fall.a1_TOA)[0]
	b0_TOA_fall = np.array(calib_data_channel_fall.b0_TOA)[0]
	c0_TOA_fall = np.array(calib_data_channel_fall.c0_TOA)[0]
	c1_TOA_fall = np.array(calib_data_channel_fall.c1_TOA)[0]
	d0_TOA_fall = np.array(calib_data_channel_fall.d0_TOA)[0]
	d1_TOA_fall = np.array(calib_data_channel_fall.d1_TOA)[0]
	opt_TOA_fall = (a0_TOA_fall, a1_TOA_fall, b0_TOA_fall, c0_TOA_fall, c1_TOA_fall, d0_TOA_fall, d1_TOA_fall)
	
	a0_TW_fall = np.array(calib_data_channel_fall.a0_TW)[0]
	a1_TW_fall = np.array(calib_data_channel_fall.a1_TW)[0]
	b0_TW_fall = np.array(calib_data_channel_fall.b0_TW)[0]
	c0_TW_fall = np.array(calib_data_channel_fall.c0_TW)[0]
	c1_TW_fall = np.array(calib_data_channel_fall.c1_TW)[0]
	d0_TW_fall = np.array(calib_data_channel_fall.d0_TW)[0]
	d1_TW_fall = np.array(calib_data_channel_fall.d1_TW)[0]
	opt_TW_fall = (a0_TW_fall, a1_TW_fall, b0_TW_fall, c0_TW_fall, c1_TW_fall, d0_TW_fall, d1_TW_fall)

	b_TW_residual_fall = np.array(calib_data_channel_fall.b_TW_residual)[0]
	m_TW_residual_fall = np.array(calib_data_channel_fall.m_TW_residual)[0]

	a_TW_Esum_fall = np.array(calib_data_channel_fall.a_TW_Esum)[0]
	b_TW_Esum_fall = np.array(calib_data_channel_fall.b_TW_Esum)[0]
	c_TW_Esum_fall = np.array(calib_data_channel_fall.c_TW_Esum)[0]
	d_TW_Esum_fall = np.array(calib_data_channel_fall.d_TW_Esum)[0]
	e_TW_Esum_fall = np.array(calib_data_channel_fall.e_TW_Esum)[0]
	opt_TW_Esum_fall = (a_TW_Esum_fall, b_TW_Esum_fall, c_TW_Esum_fall, d_TW_Esum_fall, e_TW_Esum_fall)

	l0_fall = np.array(calib_data_channel_fall.l0)[0]


	#compute the calibrated hit timestamps
	channel_samples = sample[sample.channelkey==channel]
	calib_hit_times_rise = l0_rise + func_TOA(channel_samples["toa_rise"], *opt_TOA_rise) + func_TW(channel_samples["hit_energy"], *opt_TW_rise) + func_Esum_TW(channel_samples["E_sum_layer"], channel_samples["hit_energy"], *opt_TW_Esum_rise) + (b_TW_residual_rise + m_TW_residual_rise*channel_samples["hit_energy"])
	calib_hit_times_fall = l0_fall + func_TOA(channel_samples["toa_fall"], *opt_TOA_fall) + func_TW(channel_samples["hit_energy"], *opt_TW_fall) + func_Esum_TW(channel_samples["E_sum_layer"], channel_samples["hit_energy"], *opt_TW_Esum_fall) + (b_TW_residual_fall + m_TW_residual_fall*channel_samples["hit_energy"])

	#store them to the main dataframe
	sample.loc[sample.channelkey==channel, "average_calibration"] = average_calibration
	sample.loc[sample.channelkey==channel, "calibrated_hit_time_rise"] = calib_hit_times_rise
	sample.loc[sample.channelkey==channel, "calibrated_hit_time_fall"] = calib_hit_times_fall - 12.5 *  np.sign(calib_hit_times_fall-calib_hit_times_rise)		#correct for the periodicity difference by 12.5 seconds

	return sample

#applying the correction
print "Applying the calibration..."
input_file_evaluation = pd.HDFStore(args.inputFileEvaluation)
evaluation_samples = []
for channel_str in tqdm(input_file_evaluation, unit="channels"):
	sample = input_file_evaluation[channel_str]
	channel = int(channel_str.replace("/ch_", ""))
	if calib_data.channelkey[calib_data.channelkey.isin([channel])].empty:
		continue
	sample = performCalibrationOnDataframe(sample, channel)
	sample = sample.assign(layer=sample.apply(lambda x: module_layer_map[int(x["channelkey"]/1000)], axis=1))	
	evaluation_samples.append(sample)
print "Concatenating..."
evaluation_samples = pd.concat(evaluation_samples)
input_file_evaluation.close()


#remove all non-calibrated channels:
print "Remove all non-calibrated channels"
evaluation_samples = evaluation_samples[evaluation_samples.calibrated_hit_time_fall!=-999.]

#sophisticated combination of hit timestamps, compute average if TOA within linear region
print "Compute combined ts"
numerator_combined_ts = (np.array(evaluation_samples.toa_fall<TOA_LINEARITY_MAX).astype(int)*evaluation_samples.calibrated_hit_time_fall+np.array(evaluation_samples.toa_rise<TOA_LINEARITY_MAX).astype(int)*evaluation_samples.calibrated_hit_time_rise)
denominator_combined_ts = (np.array(evaluation_samples.toa_fall<TOA_LINEARITY_MAX).astype(int)+np.array(evaluation_samples.toa_rise<TOA_LINEARITY_MAX).astype(int))
evaluation_samples = evaluation_samples.assign(calibrated_hit_time_combined = numerator_combined_ts/denominator_combined_ts)
evaluation_samples = evaluation_samples.assign(calibrated_hit_time_combined_ID = 2*np.array(evaluation_samples.toa_fall<TOA_LINEARITY_MAX).astype(int)+np.array(evaluation_samples.toa_rise<TOA_LINEARITY_MAX).astype(int))		#3: both TOA's, 2: TOA-fall, 1: TOA-rise, 0: none


#applying the period-correction for MCP-corrected timestamp
print "Apply period-corrected MCP timestamp"
period_correction_up_indexes = evaluation_samples.calibrated_hit_time_combined-evaluation_samples.TCorrected < -12 
period_correction_down_indexes = evaluation_samples.calibrated_hit_time_combined-evaluation_samples.TCorrected > 12 

evaluation_samples.loc[period_correction_up_indexes, "calibrated_hit_time_rise"] += 25.
evaluation_samples.loc[period_correction_up_indexes, "calibrated_hit_time_fall"] += 25.
evaluation_samples.loc[period_correction_up_indexes, "calibrated_hit_time_combined"] += 25.

evaluation_samples.loc[period_correction_down_indexes, "calibrated_hit_time_rise"] -= 25.
evaluation_samples.loc[period_correction_down_indexes, "calibrated_hit_time_fall"] -= 25.
evaluation_samples.loc[period_correction_down_indexes, "calibrated_hit_time_combined"] -= 25.


print "Write file to: ", args.outputFilePath
input_file_evaluation_calibrated = pd.HDFStore(args.outputFilePath)
input_file_evaluation_calibrated["calibrated"] = evaluation_samples
input_file_evaluation_calibrated.close()
