# Created: 21 Feb 2020
# Last modified: 24 November 2021
# author: Thorben Quast, thorben.quast@cern.ch
# Computes the common timestamp (or rather the combined deviation) of the calibrated hit timestamps
# with respect to the MCP time.
# Applied both for the TOA-rise/fall and both combined.

import pdb
import argparse
import os
from config.tb_ntuples import getBeamEnergyFromRun, getPDGIDFromRun
from tqdm import tqdm
import numpy as np
import pandas as pd
from config.analysis import COMMONTSPARAMETERS, TOA_LINEARITY_MAX
MINHITENERGY = COMMONTSPARAMETERS["MINHITENERGY"]


parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation',
                    default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=True)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums",
                    default="/home/tquast/tbOctober2018_H2/timing_analysis/performance/energy_weighted_sums.h5", required=True)
args = parser.parse_args()

# reading channels in files for calibration
merged_data_calib = pd.read_hdf(args.inputFileEvaluation, key="calibrated", mode="r")

# select only entries which which have enough energy
print "Selecting hits with more than %i MIP of reconstructed energy..." % MINHITENERGY
merged_data_calib = merged_data_calib[(
    merged_data_calib.hit_energy > MINHITENERGY)]

# ...and are calibrated
print "Selecting hits with valid hit time ID ..."
selected_data_calib = merged_data_calib[(
    merged_data_calib.calibrated_hit_time_combined_ID > 0)]

del merged_data_calib

print "Reducing the data content"
selected_data_calib = selected_data_calib.drop(
    columns=["N_hits_FH", "E_sum_layer", "N_hits_toa_layer"])
selected_data_calib = selected_data_calib.drop(
    columns=["toa_rise_norm", "toa_fall_norm"])
selected_data_calib = selected_data_calib.drop(
    columns=["dwc_bx", "dwc_by"])

# select only events with valid MCP timestamps
print "Selecting only events with valid MCP timestamps"
selected_data_calib = selected_data_calib[~selected_data_calib.TMCP1.isnull()]

out_data = []
run_indexes = selected_data_calib.run.unique()
channelkeys = selected_data_calib.channelkey.unique()
modules = np.unique(channelkeys/1000)

for run_index in tqdm(run_indexes, unit="runs"):
    run_data = selected_data_calib[selected_data_calib.run == run_index]

    pdgID = getPDGIDFromRun(run_index)

    if pdgID not in [11, 211]:
        import sys
        sys.exit("Neither an electron nor a charged pion run!")
    beam_energy = getBeamEnergyFromRun(run_index)

    event_indexes = sorted(run_data.event.unique())
    for event in tqdm(event_indexes, unit="events"):
        event_data = run_data[run_data.event == event]
        TMCP1 = event_data.TMCP1.iloc[0]
        ampMCP1 = event_data.ampMCP1.iloc[0]
        for AverageCalibIncluded in [0, 1]:
            if AverageCalibIncluded == 1:
                selected_event_data = event_data
            elif AverageCalibIncluded == 0:
                selected_event_data = event_data[event_data.average_calibration == 0]

            if len(selected_event_data) == 0:
                continue

            # compute an energy-weighted sum
            selected_event_data_toa_rise = selected_event_data[
                selected_event_data.toa_rise <= TOA_LINEARITY_MAX]
            selected_event_data_toa_fall = selected_event_data[
                selected_event_data.toa_fall <= TOA_LINEARITY_MAX]

            all_layers_event = sorted(
                np.append([0], selected_event_data.layer.unique()))
            for layer in all_layers_event:
                if layer == 0:
                    _selected_event_data = selected_event_data
                    _selected_event_data_toa_rise = selected_event_data_toa_rise
                    _selected_event_data_toa_fall = selected_event_data_toa_fall
                else:
                    _selected_event_data = selected_event_data[selected_event_data.layer == layer]
                    _selected_event_data_toa_rise = selected_event_data_toa_rise[
                        selected_event_data_toa_rise.layer == layer]
                    _selected_event_data_toa_fall = selected_event_data_toa_fall[
                        selected_event_data_toa_fall.layer == layer]

                common_time_stamp_toa_rise = 1000.*np.sum(_selected_event_data_toa_rise.hit_energy*(_selected_event_data_toa_rise.calibrated_hit_time_rise -
                                                                                                    _selected_event_data_toa_rise.TCorrected))/np.sum(_selected_event_data_toa_rise.hit_energy)  # conversion into ps
                common_time_stamp_toa_fall = 1000.*np.sum(_selected_event_data_toa_fall.hit_energy*(_selected_event_data_toa_fall.calibrated_hit_time_fall -
                                                                                                    _selected_event_data_toa_fall.TCorrected))/np.sum(_selected_event_data_toa_fall.hit_energy)  # conversion into ps
                common_time_stamp_toa_combined = 1000.*np.sum(_selected_event_data.hit_energy*(
                    _selected_event_data.calibrated_hit_time_combined-_selected_event_data.TCorrected))/np.sum(_selected_event_data.hit_energy)  # conversion into ps

                energy_sum = np.sum(_selected_event_data.hit_energy)
                nhits = len(_selected_event_data.hit_energy)
                out_data.append((beam_energy, pdgID, run_index, event, TMCP1, ampMCP1, layer, common_time_stamp_toa_rise,
                                 common_time_stamp_toa_fall, common_time_stamp_toa_combined, energy_sum, nhits, AverageCalibIncluded))

pd_out = pd.DataFrame(out_data, columns=["beamenergy", "pdgID", "run", "event", "TMCP1", "ampMCP1", "layer",
                                         "deltaT_toa_rise", "deltaT_toa_fall", "deltaT_toa_combined", "energysum", "Nhits", "AverageCalibIncluded"])
outstore = pd.HDFStore(args.outputFilePath)
outstore["processed"] = pd_out
outstore.close()
