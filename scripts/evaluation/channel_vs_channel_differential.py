# Created: 26 July 2021
# Last modified: 24 November 2021
# author: Thorben Quast, thorben.quast@cern.ch
# Channel vs. channel timing resolution with one channel ("1") as reference 
# for the other ("2") with variable hit energy.
# Produces resolution estimates vs. E2 hit energies.

from datetime import time
from config.analysis import EVALUATIONPARAMETERS
from config.analysis import EVALUATIONPARAMETERS
MANUALCHANNELSELECTION = EVALUATIONPARAMETERS["MANUALCHANNELSELECTION"]
MINENERGY = 100
MINENERGYREFERENCE = 800

import itertools
import pandas as pd
import numpy as np
from tqdm import tqdm
import os
import argparse
from math import sqrt
from helpers.functions import repeatedGausFit
import ROOT
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation',
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/calibrated_samples/calibration_calibrated.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums",
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/channel_evaluation/channel_vs_channel_differential/output.root", required=False)
args = parser.parse_args()


def compute_difference(df1, df2, array):
	df = pd.concat([df1, df2])
	df = df[df.index.duplicated(keep=False) == True]
	for x in df.groupby(df.index):
		_df = x[1]
		if len(_df) != 2:
			import pdb
			pdb.set_trace()  # debug --> should never arrive here
		_df = _df.reset_index(drop=True)
		E1 = _df.hit_energy[0]
		if E1 < MINENERGYREFERENCE:
			continue
		E2 = _df.hit_energy[1]
		delta_t = _df.calibrated_hit_time_combined[1]-_df.calibrated_hit_time_combined[0]
		array.append((E1, E2, delta_t))


# reading channels in files for calibration
merged_data_calib = pd.read_hdf(args.inputFileEvaluation, key="calibrated", mode="r")
selected_data = merged_data_calib[merged_data_calib.channelkey.isin(MANUALCHANNELSELECTION)]
del merged_data_calib

print "Reducing the data content"
selected_data = selected_data.drop(
    columns=["event", "run", "pdgID", "N_hits_FH", "E_sum_layer", "N_hits_toa_layer"])
selected_data = selected_data.drop(
    columns=["toa_rise", "toa_fall", "toa_rise_norm", "toa_fall_norm", "ampMCP1", "TMCP1", "TCorrected"])
selected_data = selected_data.drop(
    columns=["dwc_bx", "dwc_by", "calibrated_hit_time_rise", "calibrated_hit_time_fall", "calibrated_hit_time_combined_ID"])

# rejection of nans
selected_data = selected_data[~pd.isnull(
    selected_data.calibrated_hit_time_combined)]

# minimum energy requirement
selected_data = selected_data[selected_data.hit_energy > MINENERGY]

# determine unique channel indexes in the dataset
unique_channels = sorted(selected_data.channelkey.unique())
if MANUALCHANNELSELECTION != None:
	unique_channels = MANUALCHANNELSELECTION
Nchannels = len(unique_channels)

print "Redistributing by channels"
selected_channel_data = {}
for _ch in tqdm(unique_channels, unit="channels"):
    selected_channel_data[_ch] = selected_data[selected_data.channelkey == _ch]
    selected_channel_data[_ch] = selected_channel_data[_ch].set_index(
        "unique_event_index")

outdir = os.path.dirname(args.outputFilePath)
if not os.path.exists(outdir):
	os.mkdir(outdir)
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
outfile.Close()

h1_tmp = None
gaus_tmp = None

for ch1_index, ch2_index in itertools.product(range(Nchannels), range(Nchannels)):
	ch1 = unique_channels[ch1_index]
	ch2 = unique_channels[ch2_index]
	print ch1, ch2
	if ch1 == ch2:
		continue
	
	ch1_data = selected_channel_data[ch1]
	ch2_data = selected_channel_data[ch2]

	arr = []
	compute_difference(ch1_data, ch2_data, arr)
	if len(arr) == 0:
		continue
	df = pd.DataFrame(arr, columns=["E1", "E2", "DeltaT"])

	Ncomparisons = len(df)
	print "Performing comparison with", Ncomparisons, "entries"

	#apply timestamp difference correction
	df.DeltaT[df.DeltaT>12.5] = df.DeltaT - 25.
	df.DeltaT[df.DeltaT<-12.5] = df.DeltaT + 25.
	df.DeltaT = df.DeltaT * 1E3


	#IQR based sigma estimate vs E2
	gr_tres_estimate_vs_E2 = ROOT.TGraphErrors()
	title = "ch%i_vs_ch%i_iqr" % (ch1, ch2)
	gr_tres_estimate_vs_E2.SetName(title)
	gr_tres_estimate_vs_E2.SetTitle(title)

	#sigma vs E2
	gr_tres_vs_E2 = ROOT.TGraphErrors()
	title = "ch%i_vs_ch%i" % (ch1, ch2)
	gr_tres_vs_E2.SetName(title)
	gr_tres_vs_E2.SetTitle(title)


	E2_quantiles = [50.*i for i in range(31)]
	for quantile_index in range(0, len(E2_quantiles)-1):
		E2_limit_min = E2_quantiles[quantile_index]
		E2_limit_max = E2_quantiles[quantile_index+1]
		selected_samples = df[df.E2.between(E2_limit_min, E2_limit_max)]
		if len(selected_samples) < 100:
			continue

		mean_E2 = selected_samples.E2.mean()
		mean_E2_err = selected_samples.E2.std()/sqrt(1.*len(selected_samples))
		
		time_difference_ps = selected_samples.DeltaT

		#IQR-based estimate
		mu_estimate = np.median(time_difference_ps)
		iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
		sigma_estimate = 0.5*(iqr_84-iqr_16)

		#gaussian fits
		if h1_tmp != None:
			del h1_tmp
			del gaus_tmp
		h1_tmp = ROOT.TH1F("h1_tmp", "h1_tmp", 100, -250., 250.)
		h1_tmp.FillN(len(time_difference_ps), np.array(time_difference_ps), np.array([1.]*len(time_difference_ps)))
		gaus_tmp = ROOT.TF1("gaus_tmp", "gaus", -250., 250.)
		gaus_tmp.SetParameter(1, mu_estimate)
		gaus_tmp.SetParameter(2, sigma_estimate)
		repeatedGausFit(h1_tmp, gaus_tmp, rangeInSigmaLeft=2.0, rangeInSigmaRight=2.0, range_adjustment=True)
		sigma = gaus_tmp.GetParameter(2)
		sigma_error = gaus_tmp.GetParError(2)

		npoint = gr_tres_vs_E2.GetN()
		gr_tres_vs_E2.SetPoint(npoint, mean_E2, sigma)
		gr_tres_vs_E2.SetPointError(npoint, mean_E2_err, sigma_error)

		gr_tres_estimate_vs_E2.SetPoint(npoint, mean_E2, sigma_estimate)
		gr_tres_estimate_vs_E2.SetPointError(npoint, mean_E2_err, 0.)

	canvas = ROOT.TCanvas(title, title, 1600, 900)
	gr_tres_vs_E2.GetXaxis().SetTitle("Hit energy of channel %i [MIP]" %ch2)
	gr_tres_vs_E2.GetYaxis().SetTitle("Time resolution: #sigma [ps]")
	gr_tres_vs_E2.SetTitle("Reference: Channel %i (E_{hit}>%iMIP)" % (ch1, MINENERGYREFERENCE))
	gr_tres_vs_E2.SetMarkerSize(2)
	gr_tres_vs_E2.SetMarkerStyle(20)
	gr_tres_vs_E2.SetLineWidth(2)
	gr_tres_vs_E2.GetXaxis().SetLimits(0., 1600.)
	gr_tres_vs_E2.GetYaxis().SetRangeUser(1., 200.)
	gr_tres_vs_E2.Draw("APL")
	canvas.SaveAs(os.path.join(outdir, title+".pdf"))

	gr_tres_estimate_vs_E2.GetXaxis().SetTitle("Hit energy of channel %i [MIP]" %ch2)
	gr_tres_estimate_vs_E2.GetYaxis().SetTitle("Time resolution: IQR [ps]")
	gr_tres_estimate_vs_E2.SetTitle("Reference: Channel %i (E_{hit}>%iMIP)" % (ch1, MINENERGYREFERENCE))
	gr_tres_estimate_vs_E2.SetMarkerSize(2)
	gr_tres_estimate_vs_E2.SetMarkerStyle(20)
	gr_tres_estimate_vs_E2.SetLineWidth(2)
	gr_tres_estimate_vs_E2.GetXaxis().SetLimits(0., 1600.)
	gr_tres_estimate_vs_E2.GetYaxis().SetRangeUser(1., 200.)

	outfile = ROOT.TFile(args.outputFilePath, "UPDATE")
	gr_tres_vs_E2.Write()
	gr_tres_estimate_vs_E2.Write()
	outfile.Close()
