#Created: 21 Feb 2020
#Last modified: 19 April 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Evaluation strategy as for the channels but applied to the event timestamps from many channels combined.
#Applied both for the TOA-rise/fall and both combined.


import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import pandas as pd
import numpy as np
from scipy.stats import norm
import ROOT
ROOT.gROOT.SetBatch(True)
from math import sqrt

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', type=str, help='Input file for evaluation', default="/afs/cern.ch/work/t/tquast/hgc_prototype2018_timinganalysis_v1.6/combined_performance/energy_weighted_sums.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums", default="/afs/cern.ch/work/t/tquast/hgc_prototype2018_timinganalysis_v1.6/performance_evaluated/performance.pdf", required=False)
parser.add_argument("--rootFilePath", type=str, help="File path to store the sums", default="/afs/cern.ch/work/t/tquast/hgc_prototype2018_timinganalysis_v1.6/performance_evaluated/hists.root", required=False)
parser.add_argument("--pdgID", type=int, help="PDG ID to evaluate", default=11, required=False)
args = parser.parse_args()

DTRANGEMAX = {11: 189., 211: 389.}[args.pdgID]
FITRANGEMAX = {11: 150., 211: 350.}[args.pdgID]

from config.analysis import EVALUATIONPARAMETERS
MINAMPMCP1 = EVALUATIONPARAMETERS["MINAMPMCP1"][args.pdgID]

trueBeamEnergy_Electrons = {}
trueBeamEnergy_Electrons[300] = 287.18
trueBeamEnergy_Electrons[250] = 243.61
trueBeamEnergy_Electrons[200] = 197.32
trueBeamEnergy_Electrons[150] = 149.14  
trueBeamEnergy_Electrons[120] = 119.65
trueBeamEnergy_Electrons[100] = 99.83
trueBeamEnergy_Electrons[80] = 79.93
trueBeamEnergy_Electrons[50] = 49.99
trueBeamEnergy_Electrons[30] = 30
trueBeamEnergy_Electrons[20] = 20

from helpers.functions import repeatedGausFit, mcp_resolution

#reading channels in files for calibration
store = pd.HDFStore(args.inputFile)
processed_data = store["processed"]
store.close()

#select runs according to particle ID for now:
processed_data = processed_data[processed_data.pdgID==args.pdgID]
processed_data = processed_data[processed_data.AverageCalibIncluded==0]				#do not use average calibration file to ensure that results are reproduced
processed_data = processed_data[processed_data.layer==0]							#use full shower timestamp

beamenergies = sorted(processed_data.beamenergy.unique())
fig1, axs1 = plt.subplots(2, 5, figsize=(25, 10))
fig2, axs2 = plt.subplots(1, 3, figsize=(18,5))


colors = {
	"deltaT_toa_combined": ("black", ROOT.kBlack),	
	"deltaT_toa_rise": ("blue", ROOT.kBlue),	
	"deltaT_toa_fall": ("red", ROOT.kRed),	
}


deltaT_types = ["deltaT_toa_rise", "deltaT_toa_fall", "deltaT_toa_combined"]

x_energies = []
y_mus = {}
y_mus_error = {}
y_sigmas = {}
y_sigmas_error = {}
for toa_type in deltaT_types:
	y_mus[toa_type] = []
	y_mus_error[toa_type] = []
	y_sigmas[toa_type] = []
	y_sigmas_error[toa_type] = []

h1_delta_t = {}
f1_gaus_delta_t = {}
h2_nhits_vs_delta_comb = {}
h2_esum_vs_delta_comb = {}

h1_ampMCP1 = {}
h1_MCP1_resolution = {}
h1_nhits = {}
h1_esum = {}
h2_esum_nhits = {}

gr_tres_vs_nhits = {}
gr_tres_vs_esum = {}
gr_tres_vs_edensity = {}

for drawindex, beamenergy in enumerate(beamenergies):
	beamenergy_selected_data = processed_data[(processed_data.beamenergy==beamenergy)]
	beamenergy_selected_data = beamenergy_selected_data[beamenergy_selected_data.ampMCP1>MINAMPMCP1]

	#compute energy density
	beamenergy_selected_data = beamenergy_selected_data.assign(energydensity = beamenergy_selected_data.energysum/beamenergy_selected_data.Nhits)

	if args.pdgID == 211:
		x_energies.append(beamenergy)
	else:
		x_energies.append(trueBeamEnergy_Electrons[beamenergy])

	ampMCP1_array = np.array(beamenergy_selected_data.ampMCP1)
	nhits_array = np.array(beamenergy_selected_data.Nhits)
	esum_array = np.array(beamenergy_selected_data.energysum)

	name = "h1_ampMCP1_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_ampMCP1[beamenergy] = ROOT.TH1F(name, name, 60, 0., 1200.)
	for x in ampMCP1_array:
		h1_ampMCP1[beamenergy].Fill(x)

	name = "h1_MCP1_resolution_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_MCP1_resolution[beamenergy] = ROOT.TH1F(name, name, 60, 0., 180.)
	for x in ampMCP1_array:
		h1_MCP1_resolution[beamenergy].Fill(mcp_resolution(x))

	name = "h1_nhits_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_nhits[beamenergy] = ROOT.TH1F(name, name, 60, 0., 120.)
	for x in nhits_array:
		h1_nhits[beamenergy].Fill(x)

	name = "h1_esum_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_esum[beamenergy] = ROOT.TH1F(name, name, 300, 0., 23000.)
	for x in esum_array:
		h1_esum[beamenergy].Fill(x)

	name ="h2_esum_nhits_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h2_esum_nhits[beamenergy] = ROOT.TH2F(name, name, 60, 0., 120., 230, 0., 23000.)
	for i in range(len(esum_array)):
		h2_esum_nhits[beamenergy].Fill(nhits_array[i], esum_array[i])


	h1_delta_t[beamenergy] = {}
	f1_gaus_delta_t[beamenergy] = {}
	h2_nhits_vs_delta_comb[beamenergy] = {}
	h2_esum_vs_delta_comb[beamenergy] = {}
	gr_tres_vs_nhits[beamenergy] = {}
	gr_tres_vs_esum[beamenergy] = {}
	gr_tres_vs_edensity[beamenergy] = {}

	this_ax = axs1[drawindex/5][drawindex%5]
	for toa_type in deltaT_types:
		time_difference_ps = beamenergy_selected_data[toa_type]
		time_difference_ps = time_difference_ps.dropna()		#drop the nans
		time_difference_ps = np.array(time_difference_ps)

		name = "h1_delta_t_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h1_delta_t[beamenergy][toa_type] = ROOT.TH1F(name, name, 40, -DTRANGEMAX, DTRANGEMAX)
		for x in time_difference_ps:
			h1_delta_t[beamenergy][toa_type].Fill(x)

		name = "f1_gaus_delta_t_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		f1_gaus_delta_t[beamenergy][toa_type] = ROOT.TF1(name, "gaus", -FITRANGEMAX, FITRANGEMAX)
		repeatedGausFit(h1_delta_t[beamenergy][toa_type], f1_gaus_delta_t[beamenergy][toa_type], range_adjustment=False)

		name = "h2_nhits_vs_delta_comb_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h2_nhits_vs_delta_comb[beamenergy][toa_type] = ROOT.TH2F(name, name, 40, -DTRANGEMAX, DTRANGEMAX, 60, 0., 120.)

		name = "h2_esum_vs_delta_comb_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h2_esum_vs_delta_comb[beamenergy][toa_type] = ROOT.TH2F(name, name, 40, -DTRANGEMAX, DTRANGEMAX, 230, 0., 23000.)

		for i in range(len(time_difference_ps)):
			h2_nhits_vs_delta_comb[beamenergy][toa_type].Fill(time_difference_ps[i], nhits_array[i])
			h2_esum_vs_delta_comb[beamenergy][toa_type].Fill(time_difference_ps[i], esum_array[i])


		frequencies, bins, patches = this_ax.hist(time_difference_ps, bins=40, range=[-DTRANGEMAX, DTRANGEMAX], density=True, color=colors[toa_type][0], label="")
		mu = f1_gaus_delta_t[beamenergy][toa_type].GetParameter(1)
		mu_error = f1_gaus_delta_t[beamenergy][toa_type].GetParError(1)
		sigma = f1_gaus_delta_t[beamenergy][toa_type].GetParameter(2)
		sigma_error = f1_gaus_delta_t[beamenergy][toa_type].GetParError(2)

		y_gaus = norm.pdf( bins, mu, sigma)
		this_ax.plot(bins, y_gaus, colors[toa_type][0], linewidth=2, label=r"%s: $\mu$=%5.3fps, $\sigma$=%5.3fps" % (toa_type.replace("deltaT_", ""), mu, sigma))
		
		y_mus[toa_type].append(mu)
		y_mus_error[toa_type].append(mu_error)
		y_sigmas[toa_type].append(sigma)
		y_sigmas_error[toa_type].append(sigma_error)

		#time resolution vs number of hits and energy sum
		name = "gr_tres_vs_nhits_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_nhits[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_nhits[beamenergy][toa_type].SetName(name)
		
		name = "gr_tres_vs_esum_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_esum[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_esum[beamenergy][toa_type].SetName(name)

		name = "gr_tres_vs_edensity_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_edensity[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_edensity[beamenergy][toa_type].SetName(name)

		#vs nhits
		nhits_quantiles = sorted(np.array(beamenergy_selected_data.Nhits.quantile([0.05*i for i in range(21)])))
		for quantile_index in range(0, len(nhits_quantiles)-1):
			nhits_limit_min = nhits_quantiles[quantile_index]
			nhits_limit_max = nhits_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.Nhits.between(nhits_limit_min, nhits_limit_max)]
			
			mean_nhits = (nhits_limit_max+nhits_limit_min)/2.
			mean_nhits_err = (nhits_limit_max-nhits_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))

			gr_tres_vs_nhits[beamenergy][toa_type].SetPoint(quantile_index, mean_nhits, time_res)
			gr_tres_vs_nhits[beamenergy][toa_type].SetPointError(quantile_index, mean_nhits_err, 0.)
			
		#vs energy sum
		esum_quantiles = sorted(np.array(beamenergy_selected_data.energysum.quantile([0.05*i for i in range(21)])))
		for quantile_index in range(0, len(esum_quantiles)-1):
			esum_limit_min = esum_quantiles[quantile_index]
			esum_limit_max = esum_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.energysum.between(esum_limit_min, esum_limit_max)]
			
			mean_esum = (esum_limit_max+esum_limit_min)/2.
			mean_esum_err = (esum_limit_max-esum_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))
			gr_tres_vs_esum[beamenergy][toa_type].SetPoint(quantile_index, mean_esum, time_res)
			gr_tres_vs_esum[beamenergy][toa_type].SetPointError(quantile_index, mean_esum_err, 0.)

		#vs energy density
		edensity_quantiles = sorted(np.array(beamenergy_selected_data.energydensity.quantile([0.05*i for i in range(21)])))
		for quantile_index in range(0, len(edensity_quantiles)-1):
			edensity_limit_min = edensity_quantiles[quantile_index]
			edensity_limit_max = edensity_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.energydensity.between(edensity_limit_min, edensity_limit_max)]
			
			mean_esum = (edensity_limit_max+edensity_limit_min)/2.
			mean_esum_err = (edensity_limit_max-edensity_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))
			gr_tres_vs_edensity[beamenergy][toa_type].SetPoint(quantile_index, mean_esum, time_res)
			gr_tres_vs_edensity[beamenergy][toa_type].SetPointError(quantile_index, mean_esum_err, 0.)		
		

	this_ax.set_xlabel("Time difference [ps]")
	this_ax.set_ylabel("Frequency (normalised)")
	this_ax.legend()
	this_ax.set_title("%i GeV $%s$" % (beamenergy, {11: "e^{+}", 211: "\pi^{+}"}[args.pdgID]), fontsize=14)


axs2[0].hist2d(processed_data.deltaT_toa_rise, processed_data.deltaT_toa_fall, bins=50, range=[[-500., 500.],[-500., 500.]], label="Data")
axs2[0].set_xlabel("toa-rise time (energy-weighted channels) - MCP time [ps]")
axs2[0].set_ylabel("toa-fall time (energy-weighted channels) - MCP time [ps]")
for toa_type in deltaT_types:
	axs2[1].errorbar(np.array(x_energies)*1., np.array(y_mus[toa_type]), yerr=np.array(y_mus_error[toa_type]), label=toa_type.replace("deltaT_", ""), color= colors[toa_type][0])
axs2[1].axhline(linewidth=1, color='r')
axs2[1].set_xlabel("%s energy [GeV]" % {11: "Positron", 211: "Charged pion"}[args.pdgID])
axs2[1].set_ylabel("<Combined time difference to MCP> [ps]")
axs2[1].legend()
for toa_type in deltaT_types:
	axs2[2].errorbar(np.array(x_energies)*1., np.array(y_sigmas[toa_type]), yerr=np.array(y_sigmas_error[toa_type]), label=toa_type.replace("deltaT_", ""), color= colors[toa_type][0])
axs2[2].set_xlabel("%s energy [GeV]" % {11: "Positron", 211: "Charged pion"}[args.pdgID])
axs2[2].set_ylabel("IQR68(Combined time difference to MCP) [ps]")
axs2[2].legend()
fig2.suptitle("$%s$-timing performance" % {11: "e^{+}", 211: "\pi^{+}"}[args.pdgID], fontsize=30)


fig1.savefig(args.outputFilePath.replace(".png", "_distributions.png"))
fig2.savefig(args.outputFilePath)


#create resolution and accuracy graphs
gr_accuracies = {}
gr_resolutions = {}
gr_resolutions_mcp_subtr = {}
for toa_type in deltaT_types:
	gr_accuracies[toa_type] = ROOT.TGraphErrors()
	gr_resolutions[toa_type] = ROOT.TGraphErrors()
	gr_resolutions_mcp_subtr[toa_type] = ROOT.TGraphErrors()
	for i, beamenergy in enumerate(beamenergies):
		gr_accuracies[toa_type].SetPoint(i, x_energies[i], y_mus[toa_type][i])
		gr_accuracies[toa_type].SetPointError(i, 0, y_mus_error[toa_type][i])

		gr_resolutions[toa_type].SetPoint(i, x_energies[i], y_sigmas[toa_type][i])
		gr_resolutions[toa_type].SetPointError(i, 0, y_sigmas_error[toa_type][i])

		gr_resolutions_mcp_subtr[toa_type].SetPoint(i, x_energies[i], sqrt(y_sigmas[toa_type][i]**2-h1_MCP1_resolution[beamenergy].GetMean()**2))
		gr_resolutions_mcp_subtr[toa_type].SetPointError(i, 0, sqrt(y_sigmas_error[toa_type][i]**2+h1_MCP1_resolution[beamenergy].GetMeanError()**2))
	
	gr_accuracies[toa_type].SetName("gr_accuracy_%s"%toa_type)
	gr_accuracies[toa_type].SetTitle("%s shower timing accuracy" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
	gr_accuracies[toa_type].GetXaxis().SetTitle("%s energy [GeV]" % {11: "Positron", 211: "Charged pion"}[args.pdgID])
	gr_accuracies[toa_type].GetYaxis().SetTitle("<#Delta t> [ps]")
	
	gr_resolutions[toa_type].SetName("gr_resolution_%s"%toa_type)
	gr_resolutions[toa_type].SetTitle("%s shower timing resolution" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
	gr_resolutions[toa_type].GetXaxis().SetTitle("%s energy [GeV]" % {11: "Positron", 211: "Charged pion"}[args.pdgID])	
	gr_resolutions[toa_type].GetYaxis().SetTitle("#sigma_{t} [ps]")	

	gr_resolutions_mcp_subtr[toa_type].SetName("gr_resolution_minus_mcp_%s"%toa_type)
	gr_resolutions_mcp_subtr[toa_type].SetTitle("%s shower timing resolution minus MCP" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
	gr_resolutions_mcp_subtr[toa_type].GetXaxis().SetTitle("%s energy [GeV]" % {11: "Positron", 211: "Charged pion"}[args.pdgID])	
	gr_resolutions_mcp_subtr[toa_type].GetYaxis().SetTitle("#sigma_{t} - MCP [ps]")	

	gr_accuracies[toa_type].SetMarkerStyle(21)
	gr_resolutions[toa_type].SetMarkerStyle(22)
	gr_resolutions_mcp_subtr[toa_type].SetMarkerStyle(23)

	gr_accuracies[toa_type].SetMarkerSize(2)
	gr_resolutions[toa_type].SetMarkerSize(2)
	gr_resolutions_mcp_subtr[toa_type].SetMarkerSize(2)	

	gr_accuracies[toa_type].SetMarkerColor(colors[toa_type][1])
	gr_resolutions[toa_type].SetMarkerColor(colors[toa_type][1])
	gr_resolutions_mcp_subtr[toa_type].SetMarkerColor(colors[toa_type][1])
	
	gr_accuracies[toa_type].SetLineColor(colors[toa_type][1])
	gr_resolutions[toa_type].SetLineColor(colors[toa_type][1])
	gr_resolutions_mcp_subtr[toa_type].SetLineColor(colors[toa_type][1])

	gr_accuracies[toa_type].SetLineStyle(1)
	gr_resolutions[toa_type].SetLineStyle(1)
	gr_resolutions_mcp_subtr[toa_type].SetLineStyle(2)

	gr_accuracies[toa_type].SetLineWidth(3)
	gr_resolutions[toa_type].SetLineWidth(3)
	gr_resolutions_mcp_subtr[toa_type].SetLineWidth(3)	


#print accuracy figure
canvas = ROOT.TCanvas("canvas_accuracy", "canvas_accuracy", 1600, 900)
legend = ROOT.TLegend(0.3, 0.1, 0.9, 0.2)
legend.SetNColumns(3)

gr_accuracies["deltaT_toa_rise"].Draw("APL")
legend.AddEntry(gr_accuracies["deltaT_toa_rise"], "rise", "pl")
gr_accuracies["deltaT_toa_rise"].GetYaxis().SetRangeUser(-100., 100.)

zero_line = ROOT.TF1("zero_line", "0", 0., 300.)
zero_line.SetLineColor(ROOT.kBlack)
zero_line.SetLineWidth(2)
zero_line.Draw("SAME")

gr_accuracies["deltaT_toa_fall"].Draw("PLSAME")
legend.AddEntry(gr_accuracies["deltaT_toa_fall"], "fall", "pl")

gr_accuracies["deltaT_toa_combined"].Draw("PLSAME")
legend.AddEntry(gr_accuracies["deltaT_toa_combined"], "rise+fall", "pl")

legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_accuracy.pdf"))

#print resolution figure
canvas = ROOT.TCanvas("canvas_resolutions", "canvas_resolutions", 1600, 900)
legend = ROOT.TLegend(0.5, 0.6, 0.9, 0.9)
legend.SetNColumns(2)
gr_resolutions["deltaT_toa_rise"].Draw("APL")
if args.pdgID==11:
	gr_resolutions["deltaT_toa_rise"].GetYaxis().SetRangeUser(50, 140.)
else:
	gr_resolutions["deltaT_toa_rise"].GetYaxis().SetRangeUser(50, 300.)
legend.AddEntry(gr_resolutions["deltaT_toa_rise"], "rise", "pl")

gr_resolutions_mcp_subtr["deltaT_toa_rise"].Draw("PLSAME")
legend.AddEntry(gr_resolutions_mcp_subtr["deltaT_toa_rise"], "rise (MCP subtr.)", "pl")

gr_resolutions["deltaT_toa_fall"].Draw("PLSAME")
legend.AddEntry(gr_resolutions["deltaT_toa_fall"], "fall", "pl")
gr_resolutions_mcp_subtr["deltaT_toa_fall"].Draw("PLSAME")
legend.AddEntry(gr_resolutions_mcp_subtr["deltaT_toa_fall"], "fall (MCP subtr.)", "pl")

gr_resolutions["deltaT_toa_combined"].Draw("PLSAME")
legend.AddEntry(gr_resolutions["deltaT_toa_combined"], "rise+fall", "pl")
gr_resolutions_mcp_subtr["deltaT_toa_combined"].Draw("PLSAME")
legend.AddEntry(gr_resolutions_mcp_subtr["deltaT_toa_combined"], "rise+fall (MCP subtr.)", "pl")

legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions.pdf"))


#print resolution vs. nhits 
canvas = ROOT.TCanvas("canvas_resolutions_vs_nhits", "canvas_resolutions_vs_nhits", 1600, 900)
legend = ROOT.TLegend(0.4, 0.5, 0.9, 0.9)
legend.SetNColumns(2)
energy_colors = [ROOT.kRed, ROOT.kRed-2, ROOT.kOrange, ROOT.kYellow, ROOT.kGreen-2, ROOT.kGreen, ROOT.kCyan+1, ROOT.kBlue-2, ROOT.kBlue, ROOT.kViolet]
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	if i==0:
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetTitle("Resolution vs. N_{hits}, %s" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
		
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("Number of contributing hits")
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")
	
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(0., 120.)
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].Draw("AP")
	else:
		gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].Draw("PSAME")
	legend.AddEntry(gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"], "E_{beam} = %i GeV" % beamenergy, "pl")
legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_nhits.pdf"))


#print resolution vs. esum 
canvas = ROOT.TCanvas("canvas_resolutions_vs_esum", "canvas_resolutions_vs_esum", 1600, 900)
legend = ROOT.TLegend(0.4, 0.5, 0.9, 0.9)
legend.SetNColumns(2)
energy_colors = [ROOT.kRed, ROOT.kRed-2, ROOT.kOrange, ROOT.kYellow, ROOT.kGreen-2, ROOT.kGreen, ROOT.kCyan+1, ROOT.kBlue-2, ROOT.kBlue, ROOT.kViolet]
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	if i==0:
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetTitle("Resolution vs. E_{sum}, %s" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
		
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("Energy sum of contributing hits [MIP]")
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")
		
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(0., 23000.)
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Draw("AP")
	else:
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Draw("PSAME")
	legend.AddEntry(gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"], "E_{beam} = %i GeV" % beamenergy, "pl")
legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_esum.pdf"))


#print resolution vs. energy density 
canvas = ROOT.TCanvas("canvas_resolutions_vs_esum", "canvas_resolutions_vs_esum", 1600, 900)
legend = ROOT.TLegend(0.4, 0.5, 0.9, 0.9)
legend.SetNColumns(2)
energy_colors = [ROOT.kRed, ROOT.kRed-2, ROOT.kOrange, ROOT.kYellow, ROOT.kGreen-2, ROOT.kGreen, ROOT.kCyan+1, ROOT.kBlue-2, ROOT.kBlue, ROOT.kViolet]
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	if i==0:
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetTitle("Resolution vs. E_{density}, %s" % {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID])
		
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("Energy density of contributing hits [MIP/hit]")
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")
		
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(49., 330.)
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].Draw("AP")
	else:
		gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].Draw("PSAME")
	legend.AddEntry(gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"], "E_{beam} = %i GeV" % beamenergy, "pl")
legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_density.pdf"))


#print the distributions
for drawindex, beamenergy in enumerate(beamenergies):
	canvas = ROOT.TCanvas("canvas_aux_%iGeV" % beamenergy, "canvas_aux_%iGeV" % beamenergy, 4800, 1800)
	canvas.Divide(3, 2)
	canvas.cd(1)
	h1_ampMCP1[beamenergy].GetXaxis().SetTitle("MCP 1 amplitude [ADC]")
	h1_ampMCP1[beamenergy].GetYaxis().SetTitle("Counts")
	h1_ampMCP1[beamenergy].Draw("HIST")
	canvas.cd(2)
	h1_MCP1_resolution[beamenergy].GetXaxis().SetTitle("MCP 1 resolution estimate [ps]")
	h1_MCP1_resolution[beamenergy].GetYaxis().SetTitle("Counts")
	h1_MCP1_resolution[beamenergy].Draw("HIST")
	canvas.cd(3)
	h1_nhits[beamenergy].GetXaxis().SetTitle("Number of contributing hits")
	h1_nhits[beamenergy].GetYaxis().SetTitle("Counts")
	h1_nhits[beamenergy].Draw("HIST")
	canvas.cd(4)
	h1_esum[beamenergy].GetXaxis().SetTitle("Energy sum of contributing hits [MIP]")
	h1_esum[beamenergy].GetYaxis().SetTitle("Counts")
	h1_esum[beamenergy].Draw("HIST")
	canvas.cd(5)
	h2_esum_nhits[beamenergy].GetXaxis().SetTitle("Number of contributing hits")
	h2_esum_nhits[beamenergy].GetYaxis().SetTitle("Energy sum of contributing hits [MIPs]")
	h2_esum_nhits[beamenergy].Draw("COLZ")
	#canvas.Print(args.rootFilePath.replace(".root", "_aux_%iGeV.pdf" % beamenergy))

	canvas = ROOT.TCanvas("canvas_dt_%iGeV" % beamenergy, "canvas_dt_%iGeV" % beamenergy, 4800, 2700)
	canvas.Divide(3, 3)
	for c_index, toa_type in enumerate(deltaT_types):
		canvas.cd(c_index*3+1)
		h1_delta_t[beamenergy][toa_type].SetLineColor(colors[toa_type][1])
		f1_gaus_delta_t[beamenergy][toa_type].SetLineColor(colors[toa_type][1])
		h1_delta_t[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h1_delta_t[beamenergy][toa_type].GetYaxis().SetTitle("Counts")
		h1_delta_t[beamenergy][toa_type].Draw("HIST")
		f1_gaus_delta_t[beamenergy][toa_type].Draw("SAME")
		canvas.cd(c_index*3+2)
		h2_nhits_vs_delta_comb[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h2_nhits_vs_delta_comb[beamenergy][toa_type].GetYaxis().SetTitle("Number of contributing hits")
		h2_nhits_vs_delta_comb[beamenergy][toa_type].Draw("COLZ")
		canvas.cd(c_index*3+3)
		h2_esum_vs_delta_comb[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h2_esum_vs_delta_comb[beamenergy][toa_type].GetYaxis().SetTitle("Energy sum of contributing hits [MIP]")		
		h2_esum_vs_delta_comb[beamenergy][toa_type].Draw("COLZ")	
	canvas.Print(args.rootFilePath.replace(".root", "_%iGeV.pdf" % beamenergy))

outfile_root = ROOT.TFile(args.rootFilePath, "RECREATE")
for toa_type in deltaT_types:
	gr_accuracies[toa_type].Write()
	gr_resolutions[toa_type].Write()
	gr_resolutions_mcp_subtr[toa_type].Write()
for beamenergy in beamenergies:
	h1_ampMCP1[beamenergy].Write()
	h1_MCP1_resolution[beamenergy].Write()
	h1_nhits[beamenergy].Write()
	h1_esum[beamenergy].Write()
	h2_esum_nhits[beamenergy].Write()
	for toa_type in deltaT_types:
		h1_delta_t[beamenergy][toa_type].Write()
		f1_gaus_delta_t[beamenergy][toa_type].Write()
		h2_nhits_vs_delta_comb[beamenergy][toa_type].Write()
		h2_esum_vs_delta_comb[beamenergy][toa_type].Write()
		gr_tres_vs_nhits[beamenergy][toa_type].Write()
		gr_tres_vs_esum[beamenergy][toa_type].Write()
		gr_tres_vs_edensity[beamenergy][toa_type].Write()
outfile_root.Close()