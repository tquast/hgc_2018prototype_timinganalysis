# Created: 04 October 2021
# Last modified: 24 November 2021
# author: Thorben Quast, thorben.quast@cern.ch
# Computation of timestamp differences of
# a) full HGC prototype w.r.t. MCP
# b) odd vs. even layer combined timestamps
# using 300 GeV electron dataset

from config.analysis import CHANNELEVALUATIONPARAMETERS
MINAMPMCP1 = CHANNELEVALUATIONPARAMETERS["MINAMPMCP1"]


import argparse
import pandas as pd
import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)

from helpers.functions import repeatedGausFit

parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation',
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/calibrated_samples/calibration_calibrated.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums",
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/combined_performance/ts_comparison.root", required=False)
parser.add_argument("--energy", type=int, help="",
                    default=100, required=False)
args = parser.parse_args()

BEAMENERGY = args.energy


data = pd.read_hdf(args.inputFileEvaluation, key="calibrated", mode="r")


selected_data = data[(data.pdgID==11)&(data.beamEnergy==BEAMENERGY)]
del data
selected_data.drop(columns=["run", "pdgID", "beamEnergy"])
selected_data = selected_data[selected_data.average_calibration==0]
selected_data = selected_data.drop(columns=["average_calibration"])
selected_data = selected_data[~selected_data.TMCP1.isnull()]
selected_data = selected_data[selected_data.calibrated_hit_time_combined_ID>0]
columns_to_delete = ["N_hits_FH", "N_hits_toa_layer", "channelkey", "calibrated_hit_time_fall", "calibrated_hit_time_rise"]
columns_to_delete += ["toa_rise", "toa_fall", "calibrated_hit_time_combined_ID"]
columns_to_delete += ["event", "dwc_bx", "dwc_by", "toa_rise_norm", "toa_fall_norm", "hit_x", "hit_y", "hit_z"]
selected_data = selected_data.drop(columns=columns_to_delete) 
selected_data = selected_data[selected_data.ampMCP1>MINAMPMCP1]


#prepare outputfile
outfile = ROOT.TFile(args.outputFilePath.replace(".root", "_%iGeV.root" % BEAMENERGY), "RECREATE")
outfile.Close()

for MINHITENERGY in [50, 100, 200, 300, 400, 500, 600, 700, 800]:
    selected_data_iteration = selected_data[selected_data.hit_energy>MINHITENERGY]

    #compute timestamp differences
    selected_data_iteration = selected_data_iteration.assign(dt=selected_data_iteration.calibrated_hit_time_combined-selected_data_iteration.TCorrected)
    selected_data_iteration.dt = selected_data_iteration.dt * 1E3

    #compute combined timestamps
    TS_wrt_MCP = []
    TS_odd_vs_even = []
    for _ , ev_data in selected_data_iteration.groupby(selected_data_iteration.unique_event_index):
        TS_all_layers = np.average(ev_data.dt, weights=ev_data.hit_energy)
        TS_wrt_MCP.append(TS_all_layers)
        
        odd_layer_data = ev_data[ev_data.layer%2==1]
        if len(odd_layer_data) == 0:
            continue
        TS_odd_layers = np.average(odd_layer_data.dt, weights=odd_layer_data.hit_energy)
        even_layer_data = ev_data[ev_data.layer%2==0]
        if len(even_layer_data) == 0:
            continue
        TS_even_layers = np.average(even_layer_data.dt, weights=even_layer_data.hit_energy)
        TS_odd_vs_even.append(TS_odd_layers-TS_even_layers)


    h1_TS_wrt_MCP = ROOT.TH1F("h1_TS_wrt_MCP_min%iMIP" % MINHITENERGY, "h1_TS_wrt_MCP_min%iMIP" % MINHITENERGY, 40, -189., 189.)
    for _e in TS_wrt_MCP:
        h1_TS_wrt_MCP.Fill(_e)
    h1_TS_odd_vs_even = ROOT.TH1F("h1_TS_odd_vs_even_min%iMIP" % MINHITENERGY, "h1_TS_odd_vs_even_min%iMIP" % MINHITENERGY, 40, -189., 189.)
    for _e in TS_odd_vs_even:
        h1_TS_odd_vs_even.Fill(_e)

    f1_TS_odd_vs_even = ROOT.TF1("f1_TS_odd_vs_even_min%iMIP" % MINHITENERGY, "gaus", -150., 150.)
    h1_TS_odd_vs_even.Fit(f1_TS_odd_vs_even, "RQ")
    repeatedGausFit(h1_TS_odd_vs_even, f1_TS_odd_vs_even, range_adjustment=False)

    f1_TS_wrt_MCP = ROOT.TF1("f1_TS_wrt_MCP_min%iMIP" % MINHITENERGY, "gaus", -150., 150.)
    repeatedGausFit(h1_TS_wrt_MCP, f1_TS_wrt_MCP, range_adjustment=False)


    outfile = ROOT.TFile(args.outputFilePath.replace(".root", "_%iGeV.root" % BEAMENERGY), "UPDATE")
    h1_TS_wrt_MCP.Write()
    f1_TS_wrt_MCP.Write()
    h1_TS_odd_vs_even.Write()
    f1_TS_odd_vs_even.Write()
    outfile.Close()
