# Created: 19 April 2021
# Last modified: 19 April 2021
# author: Thorben Quast, thorben.quast@cern.ch
# Compares timestamp of 2d clusters per event between the layers.

from config.analysis import EVALUATIONPARAMETERS
MANUALLAYERSELECTION = range(5, 15) 
MINMODULEENERGY = 1000

import itertools
import pandas as pd
import numpy as np
from tqdm import tqdm
import pdb
import argparse
from math import sqrt
import ROOT
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', type=str, help='Input file for evaluation', default="/Users/thorbenquast/Desktop/per_layer_timing/energy_weighted_sums.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums",
                    default="/Users/thorbenquast/Desktop/per_layer_timing/layer_vs_layer.root", required=False)
args = parser.parse_args()


def compute_difference(df1, df2, array):
	df = pd.concat([df1, df2])
	df = df[df.index.duplicated(keep=False) == True]
	for x in df.groupby(df.index):
		_df = x[1]
		if len(_df) != 2:
			import pdb
			pdb.set_trace()  # debug --> should never arrive here
		_df = _df.reset_index(drop=True)
		E1 = _df.energysum[0]
		E2 = _df.energysum[1]
		delta_t = _df.deltaT_toa_combined[1]-_df.deltaT_toa_combined[0]
		array.append((E1, E2, delta_t))


# reading channels in files for calibration
store = pd.HDFStore(args.inputFile)
processed_data = store["processed"]
store.close()


#select runs according to particle ID for now:
processed_data = processed_data[processed_data.pdgID==11]							#select 300 GeV positrons
processed_data = processed_data[processed_data.beamenergy==300]
processed_data = processed_data[processed_data.AverageCalibIncluded==0]				#do not use average calibration file to ensure that results are reproduced
processed_data = processed_data[processed_data.layer!=0]				#do not use average calibration file to ensure that results are reproduced

processed_data = processed_data[processed_data.energysum > MINMODULEENERGY]

processed_data = processed_data.assign(unique_event_index = 1E5*processed_data.run+processed_data.event)

print("Reducing the data content")
processed_data = processed_data.drop(
    columns=["beamenergy", "pdgID", "run", "event", "ampMCP1"])
processed_data = processed_data.drop(
    columns=["deltaT_toa_rise", "deltaT_toa_fall", "AverageCalibIncluded"])

# rejection of nans
processed_data = processed_data[~pd.isnull(
    processed_data.deltaT_toa_combined)]

# determine unique channel indexes in the dataset
unique_layers = sorted(processed_data.layer.unique())
if MANUALLAYERSELECTION != None:
	unique_layers = MANUALLAYERSELECTION
Nlayers = len(unique_layers)

print("Redistributing by channels")
selected_layer_data = {}
for _layer in tqdm(unique_layers, unit="channels"):
    selected_layer_data[_layer] = processed_data[processed_data.layer == _layer]
    selected_layer_data[_layer] = selected_layer_data[_layer].set_index(
        "unique_event_index")

#prepare ROOT objects
h1fs = []
gaus_fits = []
h2_mu_delta_t = ROOT.TH2F("h2_mu_delta_t", "h2_mu_delta_t", Nlayers, -0.5, Nlayers-0.5, Nlayers, -0.5, Nlayers-0.5)
h2_sigma_delta_t = ROOT.TH2F("h2_sigma_delta_t", "h2_sigma_delta_t", Nlayers, -0.5, Nlayers-0.5, Nlayers, -0.5, Nlayers-0.5)

for h2 in [h2_mu_delta_t, h2_sigma_delta_t]:
	h2.GetXaxis().SetTitle("Layer 1")
	h2.GetYaxis().SetTitle("Layer 2")
	for index1, key in enumerate(unique_layers):
		h2.SetStats(False)
		h2.GetXaxis().SetBinLabel(index1+1, str("%i" % key))
		h2.GetYaxis().SetBinLabel(index1+1, str("%i" % key))
		h2.GetYaxis().SetTitleOffset(1.03*h2.GetYaxis().GetTitleOffset())


for layer1_index, layer2_index in itertools.product(range(Nlayers), range(Nlayers)):
	if layer1_index >= layer2_index:
		continue
	layer1 = unique_layers[layer1_index]
	layer2 = unique_layers[layer2_index]
	print(layer1, layer2)
	
	layer1_data = selected_layer_data[layer1]
	layer2_data = selected_layer_data[layer2]

	arr = []
	compute_difference(layer1_data, layer2_data, arr)
	if len(arr) == 0:
		continue
	df = pd.DataFrame(arr, columns=["E1", "E2", "DeltaT"])
	
	h1_name = "layer1_%i_vs_layer2_%s" % (layer1, layer2)
	h1 = ROOT.TH1F(h1_name, h1_name, 280, -700., 700.)
	g = ROOT.TF1("gaus_%s"%h1_name, "gaus", -700., 700.)
	for entry in np.array(df.DeltaT):
		h1.Fill(entry)

	h1.Fit(g, "Q")
	mu = g.GetParameter(1)
	sigma_red = g.GetParameter(2)/sqrt(2.)

	h2_mu_delta_t.SetBinContent(layer1_index+1, layer2_index+1, mu)
	h2_mu_delta_t.SetBinContent(layer2_index+1, layer1_index+1, -mu)
	
	h2_sigma_delta_t.SetBinContent(layer1_index+1, layer2_index+1, sigma_red)
	h2_sigma_delta_t.SetBinContent(layer2_index+1, layer1_index+1, sigma_red)

	h1fs.append(h1)
	gaus_fits.append(g)
	print(mu, sigma_red)
	print

#prepare canvas printing
from copy import deepcopy
h2_mu_delta_t_copy = deepcopy(h2_mu_delta_t)
h2_sigma_delta_t_copy = deepcopy(h2_sigma_delta_t)

h2_mu_delta_t.GetZaxis().SetRangeUser(-15., 15.)
h2_sigma_delta_t.GetZaxis().SetRangeUser(50., 80.)

canvas = ROOT.TCanvas("canvas_mu_delta_t", "canvas_mu_delta_t", 1600, 1200)
h2_mu_delta_t.Draw("COLZ")
h2_mu_delta_t_copy.Draw("TEXTSAME")
selecion_text = ROOT.TPaveText(0.15, 0.70, 0.55, 0.90, "NDC")
selecion_text.AddText("%i #leq E_{module} [MIP]" % MINMODULEENERGY)
selecion_text.AddText("Shown: #mu(T_{1}-T_{2}) [ps]")
selecion_text.Draw()
h2_mu_delta_t.SetTitle("2D cluster vs. 2D cluster (300 GeV e^{+})")
canvas.Update()
palette = h2_mu_delta_t.GetListOfFunctions().FindObject("palette")
palette.SetY1NDC(1.5)
palette.SetY2NDC(1.6)
palette.SetX1NDC(1.5)
palette.SetX2NDC(1.6)
canvas.Modified()
canvas.Update()
canvas.Print(args.outputFilePath.replace(".root", "_mu.pdf"))

canvas = ROOT.TCanvas("canvas_sigma_delta_t", "canvas_sigma_delta_t", 1600, 1200)
h2_sigma_delta_t.Draw("COLZ")
h2_sigma_delta_t_copy.Draw("TEXTSAME")
selecion_text = ROOT.TPaveText(0.15, 0.70, 0.55, 0.90, "NDC")
selecion_text.AddText("%i #leq E_{module} [MIP]" % MINMODULEENERGY)
selecion_text.AddText("Shown: #sigma(T_{1}-T_{2})/#sqrt{2} [ps]")
selecion_text.Draw()
h2_sigma_delta_t.SetTitle("2D cluster vs. 2D cluster (300 GeV e^{+})")
canvas.Update()
palette = h2_sigma_delta_t.GetListOfFunctions().FindObject("palette")
palette.SetY1NDC(1.5)
palette.SetY2NDC(1.6)
palette.SetX1NDC(1.5)
palette.SetX2NDC(1.6)
canvas.Modified()
canvas.Update()
canvas.Print(args.outputFilePath.replace(".root", "_sigma.pdf"))

#write out in ROOT TFile
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
h2_mu_delta_t.Write()
h2_sigma_delta_t.Write()
for h1 in h1fs:
	h1.Write()
for g in gaus_fits:
	g.Write()
outfile.Close()
