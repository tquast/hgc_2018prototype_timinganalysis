#Created: 19 April 2021
#Last modified: 19 April 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Evaluation strategy as for the channels but applied to the event timestamps from many channels combined in a given layer.


import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import pandas as pd
import numpy as np
from scipy.stats import norm
import ROOT
ROOT.gROOT.SetBatch(True)
from math import sqrt, exp

def fit_func(_x, _par):
	c = _par[0]
	s = _par[1]
	n = _par[2]
	#c1 = _par[3]
	#tau = _par[4]
	x = _x[0]
	#return sqrt(c**2+s**2/x+n**2/x**2+c1*exp(-x/abs(tau)))
	return sqrt(c**2+s**2/x+n**2/x**2)


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', type=str, help='Input file for evaluation', default="/Users/thorbenquast/Desktop/per_layer_timing/energy_weighted_sums.h5", required=False)
parser.add_argument("--rootFilePath", type=str, help="File path to store the sums", default="/Users/thorbenquast/Desktop/per_layer_timing/plots/hists_layer8.root", required=False)
parser.add_argument("--layer", type=int, help="Layer index to evaluate", default=8, required=False)
parser.add_argument("--pdgID", type=int, help="PDG ID to evaluate", default=11, required=False)
args = parser.parse_args()

from config.analysis import EVALUATIONPARAMETERS
MINAMPMCP1 = EVALUATIONPARAMETERS["MINAMPMCP1"][args.pdgID]


from helpers.functions import repeatedGausFit, mcp_resolution

store = pd.HDFStore(args.inputFile)
processed_data = store["processed"]
store.close()

#select runs according to particle ID for now:
processed_data = processed_data[processed_data.pdgID==args.pdgID]
processed_data = processed_data[processed_data.AverageCalibIncluded==0]				#do not use average calibration file to ensure that results are reproduced
processed_data = processed_data[processed_data.layer==args.layer]							#use full shower timestamp
#beamenergies = sorted(processed_data.beamenergy.unique())
beamenergies = [0]

colors = {
	"deltaT_toa_combined": ("black", ROOT.kBlack),	
	"deltaT_toa_rise": ("blue", ROOT.kBlue),	
	"deltaT_toa_fall": ("red", ROOT.kRed),	
}
deltaT_types = ["deltaT_toa_rise", "deltaT_toa_fall", "deltaT_toa_combined"]

h1_delta_t = {}
f1_gaus_delta_t = {}
h2_nhits_vs_delta_comb = {}
h2_esum_vs_delta_comb = {}

h1_ampMCP1 = {}
h1_MCP1_resolution = {}
h1_nhits = {}
h1_esum = {}
h2_esum_nhits = {}

gr_tres_vs_nhits = {}
gr_tres_vs_esum = {}
fits_tres_vs_esum = {}
gr_tres_vs_edensity = {}

for drawindex, beamenergy in enumerate(beamenergies):
	beamenergy_selected_data = processed_data[(processed_data.beamenergy>=beamenergy)]
	beamenergy_selected_data = beamenergy_selected_data[beamenergy_selected_data.ampMCP1>MINAMPMCP1]

	#compute energy density
	beamenergy_selected_data = beamenergy_selected_data.assign(energydensity = beamenergy_selected_data.energysum/beamenergy_selected_data.Nhits)

	ampMCP1_array = np.array(beamenergy_selected_data.ampMCP1)
	nhits_array = np.array(beamenergy_selected_data.Nhits)
	esum_array = np.array(beamenergy_selected_data.energysum)

	name = "h1_ampMCP1_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_ampMCP1[beamenergy] = ROOT.TH1F(name, name, 60, 0., 1200.)
	for x in ampMCP1_array:
		h1_ampMCP1[beamenergy].Fill(x)

	name = "h1_MCP1_resolution_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_MCP1_resolution[beamenergy] = ROOT.TH1F(name, name, 60, 0., 180.)
	for x in ampMCP1_array:
		h1_MCP1_resolution[beamenergy].Fill(mcp_resolution(x))

	name = "h1_nhits_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_nhits[beamenergy] = ROOT.TH1F(name, name, 60, 0., 10.)
	for x in nhits_array:
		h1_nhits[beamenergy].Fill(x)

	name = "h1_esum_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h1_esum[beamenergy] = ROOT.TH1F(name, name, 300, 0., 2300.)
	for x in esum_array:
		h1_esum[beamenergy].Fill(x)

	name ="h2_esum_nhits_pdgID%i_%iGeV" % (args.pdgID, beamenergy)
	h2_esum_nhits[beamenergy] = ROOT.TH2F(name, name, 60, 0., 10., 230, 0., 2300.)
	for i in range(len(esum_array)):
		h2_esum_nhits[beamenergy].Fill(nhits_array[i], esum_array[i])


	h1_delta_t[beamenergy] = {}
	f1_gaus_delta_t[beamenergy] = {}
	h2_nhits_vs_delta_comb[beamenergy] = {}
	h2_esum_vs_delta_comb[beamenergy] = {}
	gr_tres_vs_nhits[beamenergy] = {}
	gr_tres_vs_esum[beamenergy] = {}
	fits_tres_vs_esum[beamenergy] = {}
	gr_tres_vs_edensity[beamenergy] = {}

	for toa_type in deltaT_types:
		time_difference_ps = beamenergy_selected_data[toa_type]
		time_difference_ps = time_difference_ps.dropna()		#drop the nans
		time_difference_ps = np.array(time_difference_ps)

		name = "h1_delta_t_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h1_delta_t[beamenergy][toa_type] = ROOT.TH1F(name, name, 100, -1000., 1000.)
		for x in time_difference_ps:
			h1_delta_t[beamenergy][toa_type].Fill(x)

		name = "f1_gaus_delta_t_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		f1_gaus_delta_t[beamenergy][toa_type] = ROOT.TF1(name, "gaus", -1000., 1000.)
		repeatedGausFit(h1_delta_t[beamenergy][toa_type], f1_gaus_delta_t[beamenergy][toa_type])

		name = "h2_nhits_vs_delta_comb_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h2_nhits_vs_delta_comb[beamenergy][toa_type] = ROOT.TH2F(name, name, 50, -1000., 1000., 10, 0.5, 10.5)

		name = "h2_esum_vs_delta_comb_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		h2_esum_vs_delta_comb[beamenergy][toa_type] = ROOT.TH2F(name, name, 50, -1000., 1000., 230, 0., 2300.)

		for i in range(len(time_difference_ps)):
			h2_nhits_vs_delta_comb[beamenergy][toa_type].Fill(time_difference_ps[i], nhits_array[i])
			h2_esum_vs_delta_comb[beamenergy][toa_type].Fill(time_difference_ps[i], esum_array[i])


		#time resolution vs number of hits and energy sum
		name = "gr_tres_vs_nhits_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_nhits[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_nhits[beamenergy][toa_type].SetName(name)
		
		name = "gr_tres_vs_esum_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_esum[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_esum[beamenergy][toa_type].SetName(name)

		name = "gr_tres_vs_edensity_pdgID%i_%iGeV_%s" % (args.pdgID, beamenergy, toa_type)
		gr_tres_vs_edensity[beamenergy][toa_type] = ROOT.TGraphErrors()
		gr_tres_vs_edensity[beamenergy][toa_type].SetName(name)

		#vs nhits
		nhits_quantiles = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5]
		for quantile_index in range(0, len(nhits_quantiles)-1):
			nhits_limit_min = nhits_quantiles[quantile_index]
			nhits_limit_max = nhits_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.Nhits.between(nhits_limit_min, nhits_limit_max)]
			
			mean_nhits = (nhits_limit_max+nhits_limit_min)/2.
			mean_nhits_err = (nhits_limit_max-nhits_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))

			gr_tres_vs_nhits[beamenergy][toa_type].SetPoint(quantile_index, mean_nhits, time_res)
			gr_tres_vs_nhits[beamenergy][toa_type].SetPointError(quantile_index, mean_nhits_err, 0.)
			
		#vs energy sum
		esum_quantiles = sorted(np.array(beamenergy_selected_data.energysum.quantile([0.05*i for i in range(21)])))
		for quantile_index in range(0, len(esum_quantiles)-1):
			esum_limit_min = esum_quantiles[quantile_index]
			esum_limit_max = esum_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.energysum.between(esum_limit_min, esum_limit_max)]
			
			mean_esum = (esum_limit_max+esum_limit_min)/2.
			mean_esum_err = (esum_limit_max-esum_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))
			gr_tres_vs_esum[beamenergy][toa_type].SetPoint(quantile_index, mean_esum, time_res)
			gr_tres_vs_esum[beamenergy][toa_type].SetPointError(quantile_index, mean_esum_err, 0.)

		#vs energy density
		edensity_quantiles = sorted(np.array(beamenergy_selected_data.energydensity.quantile([0.05*i for i in range(21)])))
		for quantile_index in range(0, len(edensity_quantiles)-1):
			edensity_limit_min = edensity_quantiles[quantile_index]
			edensity_limit_max = edensity_quantiles[quantile_index+1]
			selected_samples = beamenergy_selected_data[beamenergy_selected_data.energydensity.between(edensity_limit_min, edensity_limit_max)]
			
			mean_esum = (edensity_limit_max+edensity_limit_min)/2.
			mean_esum_err = (edensity_limit_max-edensity_limit_min)/2.
			
			time_difference_ps = selected_samples[toa_type]
			time_difference_ps = time_difference_ps.dropna()		
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			time_res = sqrt(max([sigma**2-h1_MCP1_resolution[beamenergy].GetMean()**2, 0]))
			gr_tres_vs_edensity[beamenergy][toa_type].SetPoint(quantile_index, mean_esum, time_res)
			gr_tres_vs_edensity[beamenergy][toa_type].SetPointError(quantile_index, mean_esum_err, 0.)		
		

#print resolution vs. nhits 
canvas = ROOT.TCanvas("canvas_resolutions_vs_nhits", "canvas_resolutions_vs_nhits", 1600, 900)
legend = ROOT.TLegend(0.4, 0.8, 0.9, 0.9)
legend.SetNColumns(2)
#energy_colors = [ROOT.kRed, ROOT.kRed-2, ROOT.kOrange, ROOT.kYellow, ROOT.kGreen-2, ROOT.kGreen, ROOT.kCyan+1, ROOT.kBlue-2, ROOT.kBlue, ROOT.kViolet]
energy_colors = [ROOT.kBlack]
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].SetTitle("Layer %i: Resolution vs. N_{hits}, %s" % (args.layer, {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID]))
	
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("Number of contributing hits")
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")

	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(0., 10.)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
	gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"].Draw("AP")

	legend.AddEntry(gr_tres_vs_nhits[beamenergy]["deltaT_toa_combined"], "e^{+} data, layer %i" % args.layer, "pl")
legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_nhits.pdf"))


#print resolution vs. esum 
canvas = ROOT.TCanvas("canvas_resolutions_vs_esum", "canvas_resolutions_vs_esum", 1600, 900)
legend = ROOT.TLegend(0.2, 0.7, 0.9, 0.9)
legend.SetNColumns(2)
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetTitle("Layer %i: Resolution vs. E_{sum}, %s" % (args.layer, {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID]))
	
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("E = Energy sum of contributing hits [MIP]")
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")
	
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(0., 2300.)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Draw("AP")

	#fit
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"] = ROOT.TF1("fits_tres_vs_esum_%iGeV" % beamenergy, fit_func, 50., 2300., 3)
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParName(0, "c")
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParName(1, "s")
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParName(2, "n")
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParName(3, "c1")
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParName(4, "tau")
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParameter(0, 100.)
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParameter(1, 10.)
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParameter(2, 100.)
	#fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParameter(3, 50.)
	#fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].SetParameter(4, 50.)
	for i in range(10):
		gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Fit(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"], "RQ")
	gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Fit(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"], "R")

	legend.AddEntry(gr_tres_vs_esum[beamenergy]["deltaT_toa_combined"], "e^{+} data, layer %i" % args.layer, "pl")
	legend.AddEntry(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"], "#sqrt{c_{0}^{2} + #frac{s^{2}}{E} + #frac{n^{2}}{E^{2}}}", "l")

legend.Draw()
canvas.SetGrid(True)
canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_esum.pdf"))


#print resolution vs. energy density 
canvas = ROOT.TCanvas("canvas_resolutions_vs_esum", "canvas_resolutions_vs_esum", 1600, 900)
legend = ROOT.TLegend(0.4, 0.8, 0.9, 0.9)
legend.SetNColumns(2)
for i, beamenergy in enumerate(beamenergies):
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerColor(energy_colors[i])
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerStyle(18+i)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetMarkerSize(2)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineColor(energy_colors[i])
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineStyle(1)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetLineWidth(1)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].SetTitle("Layer %i: Resolution vs. E_{density}, %s" % (args.layer, {11: "e^{+}", 211: "#pi^{-}"}[args.pdgID]))
	
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetXaxis().SetTitle("Energy density of contributing hits [MIP/hit]")
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetYaxis().SetTitle("Mean shower timing resolution (estimate) [ps]")
	
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetXaxis().SetLimits(49., 330.)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].GetYaxis().SetRangeUser(30., 350.)
	gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"].Draw("AP")

	legend.AddEntry(gr_tres_vs_edensity[beamenergy]["deltaT_toa_combined"], "e^{+} data, layer %i" % args.layer, "pl")
legend.Draw()
canvas.SetGrid(True)
#canvas.Print(args.rootFilePath.replace(".root", "_shower_timing_resolutions_vs_density.pdf"))


#print the distributions
for drawindex, beamenergy in enumerate(beamenergies):
	canvas = ROOT.TCanvas("canvas_aux_%iGeV" % beamenergy, "canvas_aux_%iGeV" % beamenergy, 4800, 1800)
	canvas.Divide(3, 2)
	canvas.cd(1)
	h1_ampMCP1[beamenergy].GetXaxis().SetTitle("MCP 1 amplitude [ADC]")
	h1_ampMCP1[beamenergy].GetYaxis().SetTitle("Counts")
	h1_ampMCP1[beamenergy].Draw("HIST")
	canvas.cd(2)
	h1_MCP1_resolution[beamenergy].GetXaxis().SetTitle("MCP 1 resolution estimate [ps]")
	h1_MCP1_resolution[beamenergy].GetYaxis().SetTitle("Counts")
	h1_MCP1_resolution[beamenergy].Draw("HIST")
	canvas.cd(3)
	h1_nhits[beamenergy].GetXaxis().SetTitle("Number of contributing hits")
	h1_nhits[beamenergy].GetYaxis().SetTitle("Counts")
	h1_nhits[beamenergy].Draw("HIST")
	canvas.cd(4)
	h1_esum[beamenergy].GetXaxis().SetTitle("Energy sum of contributing hits [MIP]")
	h1_esum[beamenergy].GetYaxis().SetTitle("Counts")
	h1_esum[beamenergy].Draw("HIST")
	canvas.cd(5)
	h2_esum_nhits[beamenergy].GetXaxis().SetTitle("Number of contributing hits")
	h2_esum_nhits[beamenergy].GetYaxis().SetTitle("Energy sum of contributing hits [MIPs]")
	h2_esum_nhits[beamenergy].Draw("COLZ")
	#canvas.Print(args.rootFilePath.replace(".root", "_aux_%iGeV.pdf" % beamenergy))

	canvas = ROOT.TCanvas("canvas_dt_%iGeV" % beamenergy, "canvas_dt_%iGeV" % beamenergy, 4800, 2700)
	canvas.Divide(3, 3)
	for c_index, toa_type in enumerate(deltaT_types):
		canvas.cd(c_index*3+1)
		h1_delta_t[beamenergy][toa_type].SetLineColor(colors[toa_type][1])
		f1_gaus_delta_t[beamenergy][toa_type].SetLineColor(colors[toa_type][1])
		h1_delta_t[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h1_delta_t[beamenergy][toa_type].GetYaxis().SetTitle("Counts")
		h1_delta_t[beamenergy][toa_type].Draw("HIST")
		f1_gaus_delta_t[beamenergy][toa_type].Draw("SAME")
		canvas.cd(c_index*3+2)
		h2_nhits_vs_delta_comb[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h2_nhits_vs_delta_comb[beamenergy][toa_type].GetYaxis().SetTitle("Number of contributing hits")
		h2_nhits_vs_delta_comb[beamenergy][toa_type].Draw("COLZ")
		canvas.cd(c_index*3+3)
		h2_esum_vs_delta_comb[beamenergy][toa_type].GetXaxis().SetTitle("#Deltat [ps]")
		h2_esum_vs_delta_comb[beamenergy][toa_type].GetYaxis().SetTitle("Energy sum of contributing hits [MIP]")		
		h2_esum_vs_delta_comb[beamenergy][toa_type].Draw("COLZ")	
	#canvas.Print(args.rootFilePath.replace(".root", "_%iGeV.pdf" % beamenergy))

outfile_root = ROOT.TFile(args.rootFilePath, "RECREATE")
for beamenergy in beamenergies:
	h1_ampMCP1[beamenergy].Write()
	h1_MCP1_resolution[beamenergy].Write()
	h1_nhits[beamenergy].Write()
	h1_esum[beamenergy].Write()
	h2_esum_nhits[beamenergy].Write()
	for toa_type in deltaT_types:
		h2_nhits_vs_delta_comb[beamenergy][toa_type].Write()
		h2_esum_vs_delta_comb[beamenergy][toa_type].Write()
		gr_tres_vs_nhits[beamenergy][toa_type].Write()
		gr_tres_vs_esum[beamenergy][toa_type].Write()
		gr_tres_vs_edensity[beamenergy][toa_type].Write()
	fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].Write()
outfile_root.Close()

with open(args.rootFilePath.replace(".root", ".txt"), "w") as parameter_file:
	parameter_file.write("layer, c, s, n, c_err, s_err, n_err\n")
	line = str(args.layer)
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParameter(0)))
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParameter(1)))
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParameter(2)))
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParError(0)))
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParError(1)))
	line += "," + str(abs(fits_tres_vs_esum[beamenergy]["deltaT_toa_combined"].GetParError(2)))
	parameter_file.write("%s\n" % line)
	