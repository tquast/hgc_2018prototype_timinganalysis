# Created: 03 Feb 2021
# Last modified: 24 November 2021
# author: Thorben Quast, thorben.quast@cern.ch
# Computes the channel-channel timing resolution for a few selected channels.
# Only applied for the TOA-combined.
# Reproducing Fig. 9.22b of my PhD thesis.
# Not part of the analysis workflow for now. Has to be executed standalone with ROOT6 sourced.
# e.g. via: python /afs/cern.ch/user/t/tquast/hgc_2018prototype_timinganalysis/scripts/evaluation/channel_vs_channel_resolution.py

MINHITENERGY = 300
MAXHITENERGY = 700

from config.analysis import EVALUATIONPARAMETERS
MANUALCHANNELSELECTION = EVALUATIONPARAMETERS["MANUALCHANNELSELECTION"]

import itertools
import pandas as pd
import numpy as np
from tqdm import tqdm
import pdb
import argparse
from math import sqrt
import ROOT
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation',
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/calibrated_samples/calibration_calibrated.h5", required=True)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums",
                    default="/home/tquast/hgc_prototype2018_timinganalysis_v1.8/channel_evaluation/channel_vs_channel.root", required=True)
args = parser.parse_args()


def compute_difference(df1, df2, array):
	df = pd.concat([df1, df2])
	df = df[df.index.duplicated(keep=False) == True]
	for x in df.groupby(df.index):
		_df = x[1]
		if len(_df) != 2:
			import pdb
			pdb.set_trace()  # debug --> should never arrive here
		_df = _df.reset_index(drop=True)
		E1 = _df.hit_energy[0]
		E2 = _df.hit_energy[1]
		delta_t = _df.calibrated_hit_time_combined[1]-_df.calibrated_hit_time_combined[0]
		array.append((E1, E2, delta_t))


# reading channels in files for calibration
merged_data_calib = pd.read_hdf(args.inputFileEvaluation, key="calibrated", mode="r")
selected_data = merged_data_calib[merged_data_calib.channelkey.isin(MANUALCHANNELSELECTION)]
del merged_data_calib

print "Reducing the data content"
selected_data = selected_data.drop(
    columns=["event", "run", "pdgID", "N_hits_FH", "E_sum_layer", "N_hits_toa_layer"])
selected_data = selected_data.drop(
    columns=["toa_rise", "toa_fall", "toa_rise_norm", "toa_fall_norm", "ampMCP1", "TMCP1", "TCorrected"])
selected_data = selected_data.drop(
    columns=["dwc_bx", "dwc_by", "calibrated_hit_time_rise", "calibrated_hit_time_fall", "calibrated_hit_time_combined_ID"])

# rejection of nans
selected_data = selected_data[~pd.isnull(
    selected_data.calibrated_hit_time_combined)]

# minimum energy requirement
selected_data = selected_data[selected_data.hit_energy > MINHITENERGY]
selected_data = selected_data[selected_data.hit_energy < MAXHITENERGY]

# determine unique channel indexes in the dataset
unique_channels = sorted(selected_data.channelkey.unique())
if MANUALCHANNELSELECTION != None:
	unique_channels = MANUALCHANNELSELECTION
Nchannels = len(unique_channels)

print "Redistributing by channels"
selected_channel_data = {}
for _ch in tqdm(unique_channels, unit="channels"):
    selected_channel_data[_ch] = selected_data[selected_data.channelkey == _ch]
    selected_channel_data[_ch] = selected_channel_data[_ch].set_index(
        "unique_event_index")

#prepare ROOT objects
h1fs = []
gaus_fits = []
h2_mu_delta_t = ROOT.TH2F("h2_mu_delta_t", "h2_mu_delta_t", Nchannels, -0.5, Nchannels-0.5, Nchannels, -0.5, Nchannels-0.5)
h2_sigma_delta_t = ROOT.TH2F("h2_sigma_delta_t", "h2_sigma_delta_t", Nchannels, -0.5, Nchannels-0.5, Nchannels, -0.5, Nchannels-0.5)

for h2 in [h2_mu_delta_t, h2_sigma_delta_t]:
	h2.GetXaxis().SetTitle("Channel 1 (module, chip, channel)")
	h2.GetYaxis().SetTitle("Channel 2 (module, chip, channel)")
	for index1, key in enumerate(unique_channels):
		h2.SetStats(False)
		h2.GetXaxis().SetBinLabel(index1+1, str(key/1000)+","+str((key%1000)/100)+","+str(key%100))
		h2.GetYaxis().SetBinLabel(index1+1, str(key/1000)+","+str((key%1000)/100)+","+str(key%100))
		h2.GetYaxis().SetTitleOffset(1.03*h2.GetYaxis().GetTitleOffset())


for ch1_index, ch2_index in itertools.product(range(Nchannels), range(Nchannels)):
	if ch1_index >= ch2_index:
		continue
	ch1 = unique_channels[ch1_index]
	ch2 = unique_channels[ch2_index]
	print ch1, ch2
	
	ch1_data = selected_channel_data[ch1]
	ch2_data = selected_channel_data[ch2]

	arr = []
	compute_difference(ch1_data, ch2_data, arr)
	if len(arr) == 0:
		continue
	df = pd.DataFrame(arr, columns=["E1", "E2", "DeltaT"])
	#apply timestamp difference correction

	df.DeltaT[df.DeltaT>12.5] = df.DeltaT - 25.
	df.DeltaT[df.DeltaT<-12.5] = df.DeltaT + 25.
	df.DeltaT = df.DeltaT * 1E3
	
	h1_name = "ch1_%i_vs_ch2_%s" % (ch1, ch2)
	h1 = ROOT.TH1F(h1_name, h1_name, 280, -700., 700.)
	g = ROOT.TF1("gaus_%s"%h1_name, "gaus", -700., 700.)
	for entry in np.array(df.DeltaT):
		h1.Fill(entry)

	h1.Fit(g, "Q")
	mu = g.GetParameter(1)
	sigma_red = g.GetParameter(2)/sqrt(2.)

	h2_mu_delta_t.SetBinContent(ch1_index+1, ch2_index+1, mu)
	h2_mu_delta_t.SetBinContent(ch2_index+1, ch1_index+1, -mu)
	
	h2_sigma_delta_t.SetBinContent(ch1_index+1, ch2_index+1, sigma_red)
	h2_sigma_delta_t.SetBinContent(ch2_index+1, ch1_index+1, sigma_red)

	h1fs.append(h1)
	gaus_fits.append(g)
	print mu, sigma_red
	print

#prepare canvas printing
from copy import deepcopy
h2_mu_delta_t_copy = deepcopy(h2_mu_delta_t)
h2_sigma_delta_t_copy = deepcopy(h2_sigma_delta_t)

h2_mu_delta_t.GetZaxis().SetRangeUser(-280., 280.)
h2_sigma_delta_t.GetZaxis().SetRangeUser(35., 90.)

canvas = ROOT.TCanvas("canvas_mu_delta_t", "canvas_mu_delta_t", 1600, 1200)
h2_mu_delta_t.Draw("COLZ")
h2_mu_delta_t_copy.Draw("TEXTSAME")
selecion_text = ROOT.TPaveText(0.15, 0.70, 0.55, 0.90, "NDC")
selecion_text.AddText("%i #leq E [MIP] #leq %i" % (MINHITENERGY, MAXHITENERGY))
selecion_text.AddText("Shown: #mu(T_{1}-T_{2}) [ps]")
selecion_text.Draw()
h2_mu_delta_t.SetTitle("Channel vs. channel")
canvas.Update()
palette = h2_mu_delta_t.GetListOfFunctions().FindObject("palette")
palette.SetY1NDC(1.5)
palette.SetY2NDC(1.6)
palette.SetX1NDC(1.5)
palette.SetX2NDC(1.6)
canvas.Modified()
canvas.Update()
canvas.Print(args.outputFilePath.replace(".root", "_mu.pdf"))

canvas = ROOT.TCanvas("canvas_sigma_delta_t", "canvas_sigma_delta_t", 1600, 1200)
h2_sigma_delta_t.Draw("COLZ")
h2_sigma_delta_t_copy.Draw("TEXTSAME")
selecion_text = ROOT.TPaveText(0.15, 0.70, 0.55, 0.90, "NDC")
selecion_text.AddText("%i #leq E [MIP] #leq %i" % (MINHITENERGY, MAXHITENERGY))
selecion_text.AddText("Shown: #sigma(T_{1}-T_{2})/#sqrt{2} [ps]")
selecion_text.Draw()
h2_sigma_delta_t.SetTitle("Channel vs. channel")
canvas.Update()
palette = h2_sigma_delta_t.GetListOfFunctions().FindObject("palette")
palette.SetY1NDC(1.5)
palette.SetY2NDC(1.6)
palette.SetX1NDC(1.5)
palette.SetX2NDC(1.6)
canvas.Modified()
canvas.Update()
canvas.Print(args.outputFilePath.replace(".root", "_sigma.pdf"))

#write out in ROOT TFile
outfile = ROOT.TFile(args.outputFilePath, "RECREATE")
h2_mu_delta_t.Write()
h2_sigma_delta_t.Write()
for h1 in h1fs:
	h1.Write()
for g in gaus_fits:
	g.Write()
outfile.Close()
