#Created: 21 Feb 2020
# Last modified: 24 November 2021
#author: Thorben Quast, thorben.quast@cern.ch
#Evaluates the timing performance of single channels in terms of accuracy and precision as a function
#of the hit energy. Produces the relevant distributions and graphs as graphic files.
#Applied both for the TOA-rise/fall and both combined.

from config.analysis import CHANNELEVALUATIONPARAMETERS, TOA_LINEARITY_MAX
MINHITENERGY = CHANNELEVALUATIONPARAMETERS["MINHITENERGY"]
MINENTRIESFOREVAL = CHANNELEVALUATIONPARAMETERS["MINENTRIESFOREVAL"]
MINAMPMCP1 = CHANNELEVALUATIONPARAMETERS["MINAMPMCP1"]

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
from scipy.stats import norm
from math import sqrt
import ROOT

from helpers.functions import repeatedGausFit, mcp_resolution

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=False)
parser.add_argument("--channelKey", type=int, help="path to to ntuple to create channel map from", default=32228, required=False)
parser.add_argument("--outputFilePath_Raw", type=str, help="File path for visualisation", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_raw.pdf", required=False)
parser.add_argument("--outputFilePath_2D", type=str, help="File path for visualisation", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_2D.pdf", required=False)
parser.add_argument("--outputFilePath_Graph", type=str, help="File path for visualisation", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_graphs.pdf", required=False)
parser.add_argument("--outputFilePath_ROOT", type=str, help="File path containing fitted resolution curves.", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_fits.root", required=False)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#reading channels in files for calibration
merged_data_calib = pd.read_hdf(args.inputFileEvaluation, key="calibrated", mode="r")

#step 0:
#select good timing data
selected_samples = merged_data_calib[merged_data_calib.channelkey==args.channelKey]
del merged_data_calib
selected_samples = selected_samples[(selected_samples.TCorrected>=0)&(selected_samples.TCorrected<=25.)&(selected_samples.hit_energy>MINHITENERGY)]
selected_samples = selected_samples[selected_samples.ampMCP1>MINAMPMCP1]


#prepare the figures
fig1, axs1 = plt.subplots(6, 5, figsize=(30,30))
fig2, axs2 = plt.subplots(3, 2, figsize=(12,15))
fig3, axs3 = plt.subplots(1, 2, figsize=(18,5))


colors = {
	"calibrated_hit_time_combined": "black",	
	"calibrated_hit_time_rise": "blue",	
	"calibrated_hit_time_fall": "red",	
}

h1_tmp = None
gaus_tmp = None

if len(selected_samples.hit_energy) > MINENTRIESFOREVAL:
	x_energies = []
	y_mus = {}
	y_mus_error = {}
	y_sigmas = {}
	y_sigmas_error = {}
	for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
		y_mus[toa_type] = []
		y_mus_error[toa_type] = []
		y_sigmas[toa_type] = []
		y_sigmas_error[toa_type] = []



	Energy_quantiles = [50.*i for i in range(31)]
	for quantile_index in range(0, len(Energy_quantiles)-1):
		E2_limit_min = Energy_quantiles[quantile_index]
		E2_limit_max = Energy_quantiles[quantile_index+1]
		selected_indexes = selected_samples.hit_energy.between(E2_limit_min, E2_limit_max)
		drawindex = quantile_index

		energy_entries = selected_samples.hit_energy[selected_indexes]
		if len(energy_entries)<100:
			continue
		Emean = np.mean(energy_entries)
		this_ax = axs1[drawindex/5][drawindex%5]
		x_energies.append(Emean)
			
		ampMCP1_array = np.array(selected_samples[selected_indexes].ampMCP1)
		name = "h1_resMCP1"
		h1_resMCP1 = ROOT.TH1F(name, name, 300, 1., 300.)
		for x in ampMCP1_array:
			h1_resMCP1.Fill(mcp_resolution(x))

		for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
			time_difference_ps = 1000.*(selected_samples[selected_indexes][toa_type]-selected_samples[selected_indexes].TCorrected)
			time_difference_ps = time_difference_ps.dropna()

			#gaussian fits
			if h1_tmp != None:
				del h1_tmp
				del gaus_tmp
			h1_tmp = ROOT.TH1F("h1_tmp", "h1_tmp", 50, -250., 250.)
			h1_tmp.FillN(len(time_difference_ps), np.array(time_difference_ps), np.array([1.]*len(time_difference_ps)))
			
			#estimate parameter values
			mu0 = np.median(time_difference_ps)
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma0 = 0.5*(iqr_84-iqr_16)
			
			gaus_tmp = ROOT.TF1("gaus_tmp", "gaus", -250., 250.)
			gaus_tmp.SetParameter(1, mu0)
			gaus_tmp.SetParameter(2, sigma0)
			repeatedGausFit(h1_tmp, gaus_tmp, rangeInSigmaLeft=1.5, rangeInSigmaRight=1.5, range_adjustment=True)
			mu = gaus_tmp.GetParameter(1) 
			mu_err = gaus_tmp.GetParError(1)
			sigma = sqrt(gaus_tmp.GetParameter(2)**2 - h1_resMCP1.GetMean()**2)		#subtract MCP contribution
			sigma_err = gaus_tmp.GetParError(2)
			
			#plotting with matplotlib
			nbins = 60
			frequencies, bins, patches = this_ax.hist(time_difference_ps, bins=nbins, range=[-750., 750.], density=True, color=colors[toa_type])
			y_gaus = norm.pdf( bins, mu, sigma)
			this_ax.plot(bins, y_gaus, colors[toa_type], linewidth=2, label=r"%s: $\mu$=%5.3fps, $\sigma$=%5.3fps" % (toa_type.replace("calibrated_", ""), mu, sigma))

			y_mus[toa_type].append(mu)
			y_mus_error[toa_type].append(mu_err)
			y_sigmas[toa_type].append(sigma)
			y_sigmas_error[toa_type].append(sigma_err)

		this_ax.set_title("E = %.1f - %.1f MIPs" % (E2_limit_min, E2_limit_max), fontsize=10)
		this_ax.legend()
		this_ax.set_xlabel("Hit time - MCP time [ps]")

	fig1.suptitle("Module %i, Chip %i, Channel %i" % (Module, Chip, Channel), fontsize=30)


	#plot the 2D-histograms of TOA-differences
	selected_samples_cat1 = selected_samples[(selected_samples.toa_rise<=TOA_LINEARITY_MAX) & (selected_samples.toa_fall<=TOA_LINEARITY_MAX)]
	selected_samples_cat2_fall = selected_samples[(selected_samples.toa_rise<=TOA_LINEARITY_MAX) & (selected_samples.toa_fall>TOA_LINEARITY_MAX)]
	selected_samples_cat2_rise = selected_samples[(selected_samples.toa_rise>TOA_LINEARITY_MAX) & (selected_samples.toa_fall<=TOA_LINEARITY_MAX)]

	try:
		axs2[0][0].hist2d((selected_samples_cat1.calibrated_hit_time_rise-selected_samples_cat1.calibrated_hit_time_fall)*1000, selected_samples_cat1.calibrated_hit_time_rise, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
		axs2[0][0].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 1)")
		axs2[0][0].set_ylabel("Calibrated toa-rise [ns]")

		axs2[0][1].hist2d((selected_samples_cat1.calibrated_hit_time_rise-selected_samples_cat1.calibrated_hit_time_fall)*1000, selected_samples_cat1.calibrated_hit_time_fall, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
		axs2[0][1].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 1)")
		axs2[0][1].set_ylabel("Calibrated toa-fall [ns]")
	except:
		pass


	axs2[1][0].hist2d((selected_samples_cat2_fall.calibrated_hit_time_rise-selected_samples_cat2_fall.calibrated_hit_time_fall)*1000, selected_samples_cat2_fall.calibrated_hit_time_rise, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
	axs2[1][0].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 2-FALL)")
	axs2[1][0].set_ylabel("Calibrated toa-rise [ns]")

	axs2[1][1].hist2d((selected_samples_cat2_fall.calibrated_hit_time_rise-selected_samples_cat2_fall.calibrated_hit_time_fall)*1000, selected_samples_cat2_fall.calibrated_hit_time_fall, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
	axs2[1][1].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 2-FALL)")
	axs2[1][1].set_ylabel("Calibrated toa-fall [ns]")


	axs2[2][0].hist2d((selected_samples_cat2_rise.calibrated_hit_time_rise-selected_samples_cat2_rise.calibrated_hit_time_fall)*1000, selected_samples_cat2_rise.calibrated_hit_time_rise, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
	axs2[2][0].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 2-RISE)")
	axs2[2][0].set_ylabel("Calibrated toa-rise [ns]")

	axs2[2][1].hist2d((selected_samples_cat2_rise.calibrated_hit_time_rise-selected_samples_cat2_rise.calibrated_hit_time_fall)*1000, selected_samples_cat2_rise.calibrated_hit_time_fall, bins=50, range=[[-400., 400.],[-5., 30.]], label="Data")
	axs2[2][1].set_xlabel("Calibrated toa-rise - toa-fall [ps] (CAT 2-RISE)")
	axs2[2][1].set_ylabel("Calibrated toa-fall [ns]")


	#plot the resolution graphs
	for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
		axs3[0].errorbar(np.array(x_energies), np.array(y_mus[toa_type]), yerr=np.array(y_mus_error[toa_type]), label=toa_type.replace("calibrated_", ""), color= colors[toa_type])
		axs3[1].errorbar(np.array(x_energies), np.array(y_sigmas[toa_type]), yerr=np.array(y_sigmas_error[toa_type]), label=toa_type.replace("calibrated_", ""), color= colors[toa_type])
	axs3[0].axhline(linewidth=1, color='gold')
	axs3[0].set_xlabel("Hit energy [MIPs]")
	axs3[0].set_ylabel("<Time difference> [ps]")
	axs3[0].set_xlim(0., 1500.)
	axs3[0].set_ylim(-150., 150.)
	axs3[0].grid(b=True)
	axs3[0].legend()
	axs3[1].set_xlabel("Hit energy [MIPs]")
	axs3[1].set_ylabel("Width(Time difference) [ps]")
	axs3[1].set_xlim(0., 1500.)
	axs3[1].set_ylim(0., 250.)
	axs3[1].grid(b=True)
	axs3[1].legend()
	fig3.suptitle("Module %i, Chip %i, Channel %i" % (Module, Chip, Channel), fontsize=30)



fig1.savefig(args.outputFilePath_Raw)
fig2.savefig(args.outputFilePath_2D)
fig3.savefig(args.outputFilePath_Graph)


#perform fitting for extraction of the constant terms
gr_errs = {}
fits = {}

for toa_type in colors:
	gr_errs[toa_type] = ROOT.TGraphErrors()
	gr_errs[toa_type].SetName(toa_type)
	fits[toa_type] = ROOT.TF1(toa_type, "([0]^2+[1]^2/x+[2]^2/x^2)^0.5", 100., 1500.)
	fits[toa_type].SetParName(0, "constant")
	fits[toa_type].SetParName(1, "stochastic")
	fits[toa_type].SetParName(2, "noise")
	fits[toa_type].SetParameter(0, 50.)
	fits[toa_type].FixParameter(1, 0.)
	fits[toa_type].SetParameter(2, 00.)
	for n in range(len(x_energies)):
		x = x_energies[n]
		x_err = 0.
		y = y_sigmas[toa_type][n]
		y_err = y_sigmas_error[toa_type][n]
		gr_errs[toa_type].SetPoint(n, x, y)
		gr_errs[toa_type].SetPointError(n, x_err, y_err)
	for i in range(10):
		gr_errs[toa_type].Fit(fits[toa_type], "QN")
	gr_errs[toa_type].Fit(fits[toa_type], "N")
	
outfileROOT = ROOT.TFile(args.outputFilePath_ROOT, "RECREATE")
for toa_type in gr_errs:
	gr_errs[toa_type].Write()
	fits[toa_type].Write()
outfileROOT.Close()