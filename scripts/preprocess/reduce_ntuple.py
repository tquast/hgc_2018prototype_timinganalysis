#Created: 21 Feb 2020
#Last modified: 24 April 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Reads the ntuple content and saves timing information as flat pandas data frames for subsequent analysis independent of ROOT.

from config.analysis import SPEED_OF_LIGHT, NLAYERS_EE, NLAYERS_FH, NTUPLEPROCESSPARAMETERS
from config.tb_ntuples import getPDGIDFromRun, getBeamEnergyFromRun

POS_MCP1 = NTUPLEPROCESSPARAMETERS["POS_MCP1"]
MAXNHITSFH = NTUPLEPROCESSPARAMETERS["MAXNHITSFH"]
MINHITENERGYANALYSIS = NTUPLEPROCESSPARAMETERS["MINHITENERGYANALYSIS"]

from root_pandas import read_root
import numpy as np
import pandas as pd
from tqdm import tqdm
from math import sqrt

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--inputFilePath", type=str, help="path to the input ntuple", default="/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16/ntuple_1005.root", required=True)
parser.add_argument("--outputFilePath", type=str, help="path to the output h5 file", default="/home/tquast/tmp/run_1005.h5", required=True)
parser.add_argument("--alignmentPath", type=str, help="path to the HGC-DWC alignment file", default="", required=True)
args = parser.parse_args()

df_rechits = read_root(args.inputFilePath, "rechitntupler/hits", columns=["event", "run", "rechit_x", "rechit_y", "rechit_z", "rechit_layer", "rechit_module", "rechit_chip", "rechit_channel", "rechit_energy", "rechit_toaRise_norm", "rechit_toaFall_norm", "rechit_toaRise", "rechit_toaFall"])
df_dwc = read_root(args.inputFilePath, "trackimpactntupler/impactPoints", columns=["event", "run", "dwcReferenceType", "b_x", "b_y", "m_x", "m_y"])
df_mcp = read_root(args.inputFilePath, "MCPntupler/MCP", columns=["event", "run", "ampFit_MCP1", "ampFit_MCP2", "TS_toClock_FE_MCP1", "TS_toClock_FE_MCP2"])

NEvents = len(df_rechits)

#read the alignment
translational_alignment_data = np.genfromtxt(args.alignmentPath, skip_header=2, dtype=[("layer", "i4"), ("x", "f8"), ("y", "f8")])

flat_data = []
for nevent in tqdm(range(NEvents), unit="evt"):
	event = df_rechits.event[nevent]
	run = df_rechits.run[nevent]
	pdgID = getPDGIDFromRun(run)
	beamEnergy = getBeamEnergyFromRun(run)
	ampFit_MCP1 = df_mcp.ampFit_MCP1[nevent]
	TS_toClock_FE_MCP1 = df_mcp.TS_toClock_FE_MCP1[nevent]

	dwcReferenceType = df_dwc.dwcReferenceType[nevent]
	if dwcReferenceType!=13:		#One should require good DWC track if we want to assess the TOF from incidence to the cell eventually.
		continue

	dwc_bx = df_dwc.b_x[nevent]
	dwc_by = df_dwc.b_y[nevent]
	dwc_mx = df_dwc.m_x[nevent]
	dwc_my = df_dwc.m_y[nevent]

	NHit = len(df_rechits.rechit_module[nevent])
	rechits_module = df_rechits.rechit_module[nevent]
	rechits_layer = df_rechits.rechit_layer[nevent]
	rechits_chip = df_rechits.rechit_chip[nevent]
	rechits_channel = df_rechits.rechit_channel[nevent]
	rechits_x = df_rechits.rechit_x[nevent]
	rechits_y = df_rechits.rechit_y[nevent]
	rechits_z = df_rechits.rechit_z[nevent]
	rechits_energy = df_rechits.rechit_energy[nevent]
	rechits_toaRise_norm = df_rechits.rechit_toaRise_norm[nevent]
	rechits_toaFall_norm = df_rechits.rechit_toaFall_norm[nevent]
	rechits_toaRise = df_rechits.rechit_toaRise[nevent]
	rechits_toaFall = df_rechits.rechit_toaFall[nevent]

	#event-based analysis
	E_sum_module = [0.]*150			#actually we are interested in the energy deposited in a module
	N_hits_toa_module = [0]*150		
	N_hits_FH = 0
	for nhit in range(NHit):
		_layer = rechits_layer[nhit]
		_module = rechits_module[nhit]
		_energy = rechits_energy[nhit]
		_toa_rise_norm = rechits_toaRise_norm[nhit]
		_toa_fall_norm = rechits_toaFall_norm[nhit]	
		_toa_rise = rechits_toaRise[nhit]
		_toa_fall = rechits_toaFall[nhit]		
		if _energy < MINHITENERGYANALYSIS:
			continue
		E_sum_module[_module] += _energy
		if _toa_rise>4 and _toa_fall>4:
			N_hits_toa_module[_module]+=1
		if _layer>NLAYERS_EE:
			N_hits_FH+=1
	if N_hits_FH>MAXNHITSFH:		#since v1.5: MAXNHITSFH = infinity
		continue		#skip the event
	
	#event is good --> fill into dataframe
	for nhit in range(NHit):
		_toa_rise = rechits_toaRise[nhit]
		_toa_fall = rechits_toaFall[nhit]		
		if _toa_rise==4:
			continue
		if _toa_fall==4:
			continue
		_layer = rechits_layer[nhit]
		if _layer>NLAYERS_EE+NLAYERS_FH:
			continue

		_toa_rise_norm = rechits_toaRise_norm[nhit]
		if _toa_rise_norm < 0 or _toa_rise_norm > 1:
			continue
		_toa_fall_norm = rechits_toaFall_norm[nhit]	
		if _toa_fall_norm < 0 or _toa_fall_norm > 1:
			continue		

		_x = rechits_x[nhit]
		_y = rechits_y[nhit]
		_z = rechits_z[nhit]
		_energy = rechits_energy[nhit]

		_module = rechits_module[nhit]
		_chip = rechits_chip[nhit]
		_channel = rechits_channel[nhit]

		ch_key = 1000*_module+100*_chip+_channel


		#compute the distance of flight from the MCP to the beginning of the EE and from there to the hit
		TOF_distance = sqrt(pow(dwc_mx, 2)+pow(dwc_my, 2)+1.)*abs(POS_MCP1)
		
		impact_X = - dwc_bx		#the minus is convention of the data format
		impact_Y = - dwc_by		#the minus is convention of the data format
		delta_X = _x - impact_X - translational_alignment_data[_layer-1]["x"]
		delta_Y = _y - impact_Y - translational_alignment_data[_layer-1]["y"]
		TOF_distance += sqrt(pow(delta_X,2) + pow(delta_Y,2) + pow(_z,2))
		corrected_time = TS_toClock_FE_MCP1 + TOF_distance/SPEED_OF_LIGHT
		if corrected_time > 25.:
			corrected_time = corrected_time - 25.		#25ns periodicity
		
		flat_data.append((event, run, pdgID, beamEnergy, N_hits_FH, E_sum_module[_module], N_hits_toa_module[_module], ch_key, _x, _y, _z, _energy, _toa_rise_norm, _toa_fall_norm, _toa_rise, _toa_fall, ampFit_MCP1, TS_toClock_FE_MCP1, corrected_time, dwc_bx, dwc_by))


pd_output = pd.DataFrame(flat_data, columns=["event", "run", "pdgID", "beamEnergy", "N_hits_FH", "E_sum_layer", "N_hits_toa_layer", "channelkey", "hit_x", "hit_y", "hit_z", "hit_energy", "toa_rise_norm", "toa_fall_norm", "toa_rise", "toa_fall", "ampMCP1", "TMCP1", "TCorrected", "dwc_bx", "dwc_by"])
#store in external file
store = pd.HDFStore(args.outputFilePath)
store["pd"] = pd_output
store.close()