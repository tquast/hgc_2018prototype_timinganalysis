#Created: 21 Feb 2020
#Last modified: 24 April 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Template to merge files, counts occurence of channels for min/max identification.
#Normalises the TOA and writes out the affected channels into a file.
#Only those channels in the set of runs can be used for subsequent calibration & timing performance assessment.
from config.analysis import MINMAXPARAMETERS
NMINFORANALYSIS = MINMAXPARAMETERS["NMINFORANALYSIS"]
MINAMP_MCP1 = MINMAXPARAMETERS["MINAMP_MCP1"]
IQR = MINMAXPARAMETERS["IQR"]

from math import ceil, floor
import pandas as pd
from tqdm import tqdm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFiles', nargs='+', help='Input files', required=True)
parser.add_argument("--outputFilePath", type=str, help="path to the output h5 file", default="/home/tquast/tmp/merged_runs.h5", required=False)
args = parser.parse_args()


merged_data = None
for infilepath in tqdm(args.inputFiles, unit="input files"):
	store = pd.HDFStore(infilepath)
	if merged_data is None:
		merged_data = store["pd"]
	else:
		merged_data = pd.concat([merged_data, store["pd"]])
	store.close()
	#print "+",len(store["pd"]),"=",len(merged_data)	

#assign a unique event index
merged_data = merged_data.assign(unique_event_index = 1e6*merged_data["run"]+merged_data["event"])
print "Determining unique channels in the merged file"
counts = merged_data.groupby(["channelkey"])["unique_event_index"].nunique()

min_max_array = []


minmax_corrected_data = []
outliers = []
for chkey, count in tqdm(counts.items(), unit="channels"):
	if count < NMINFORANALYSIS:
		continue

	df_channel = merged_data[merged_data.channelkey==chkey].copy()

	TOA_rise_min = int(floor((df_channel.toa_rise.quantile(1-IQR))))
	TOA_rise_max = int(ceil((df_channel.toa_rise.quantile(IQR))))
	TOA_fall_min =int(floor((df_channel.toa_fall.quantile(1-IQR))))
	TOA_fall_max = int(ceil((df_channel.toa_fall.quantile(IQR))))

	min_max_array.append((chkey, TOA_rise_min, TOA_rise_max, TOA_fall_min, TOA_fall_max, count))

	x = df_channel[(df_channel.toa_fall > TOA_fall_max) | (df_channel.toa_rise > TOA_rise_max)]
	if len(x)>0:
		x = x.assign(TOA_fall_diff_to_max=x.toa_fall-TOA_fall_max)
		x = x.assign(TOA_rise_diff_to_max=x.toa_rise-TOA_rise_max)
		outliers.append(x)

	#normalise the TOAs, removed 02 November 2020
	#df_channel.toa_rise = 1.*(df_channel.toa_rise-TOA_rise_min) / (TOA_rise_max-TOA_rise_min)
	#df_channel.toa_fall = 1.*(df_channel.toa_fall-TOA_fall_min) / (TOA_fall_max-TOA_fall_min)

	#define TOAs as normalised TOAs as in the ntuple
	df_channel.toa_rise = df_channel.toa_rise_norm
	df_channel.toa_fall = df_channel.toa_fall_norm

	#Selecting events with minimum MCP 1
	
	df_channel = df_channel[df_channel.ampMCP1>=MINAMP_MCP1]

	minmax_corrected_data.append(df_channel)


print "Concatenating %i dataframes" % len(minmax_corrected_data)
minmax_corrected_data = pd.concat(minmax_corrected_data)


#perform the selection based on the minimum amplitude of MCP 1

print "Saving min-max corrected entries to",args.outputFilePath
outstore = pd.HDFStore(args.outputFilePath)
#write out the selected channels
outstore["minmaxcorrected"] = minmax_corrected_data

print "Saving min-max to file"
pd_min_max = pd.DataFrame(min_max_array, columns=["channelkey", "TOA_rise_min", "TOA_rise_max", "TOA_fall_min", "TOA_fall_max", "Nevents"])
outstore["minmax"] = pd_min_max
if len(outliers)>0:
	outliers = pd.concat(outliers)
	outstore["outliers"] = outliers
outstore.close()