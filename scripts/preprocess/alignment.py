#Created: 07 April 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Estimates the translational misalignment for each layer w.r.t. the dwc
from config.tb_ntuples import getRuns, get_ntuple_path
from config.analysis import NLAYERS_EE, NLAYERS_FH, ALIGNMENTPARAMETERS
MIN_DWC_REF_TYPE = ALIGNMENTPARAMETERS["MIN_DWC_REF_TYPE"]
MAX_DWC_CHI2 = ALIGNMENTPARAMETERS["MAX_DWC_CHI2"]

import ROOT
import root_numpy as rn
import numpy as np
from tqdm import tqdm
from tabulate import tabulate

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--outputFilePath", type=str, help="Path to the alignment file", default="", required=True)
args = parser.parse_args()

recPosData = {}
for layer in range(1, NLAYERS_EE+NLAYERS_FH+1):
	recPosData[layer] = {"x": [], "y": []}

#loop over the ntuples
for run in tqdm(getRuns(), unit="run"):
	tree_rechits = ROOT.TChain('rechitntupler/hits','rechits')
	tree_tracks = ROOT.TChain('trackimpactntupler/impactPoints','tracks')
	print "Adding Run", run
	tree_rechits.Add(get_ntuple_path(run))	
	tree_tracks.Add(get_ntuple_path(run))

	data_rechits = rn.tree2array(tree_rechits)
	data_tracks = rn.tree2array(tree_tracks)

	#loop over the events
	for ev in tqdm(range(len(data_rechits)), unit="ev"):
		#reconstructed positions
		rechits = data_rechits[ev]
		#DWC-based selection
		dwcReferenceType = data_tracks[ev]["dwcReferenceType"]
		if dwcReferenceType<MIN_DWC_REF_TYPE:
			continue
		dwcReferenceChi2X = data_tracks[ev]["trackChi2_X"]
		if dwcReferenceChi2X>MAX_DWC_CHI2:
			continue
		dwcReferenceChi2Y = data_tracks[ev]["trackChi2_Y"]
		if dwcReferenceChi2Y>MAX_DWC_CHI2:
			continue
		
		#compute impact position per layer, also deviation to DWC extrapolation
		for layer in range(1, NLAYERS_EE+NLAYERS_FH+1):
			layer_hits_x = rechits["rechit_x"][np.where(rechits["rechit_layer"]==layer)]
			layer_hits_y = rechits["rechit_y"][np.where(rechits["rechit_layer"]==layer)]
			layer_hits_z = rechits["rechit_z"][np.where(rechits["rechit_layer"]==layer)]
			layer_hits_energy = rechits["rechit_energy"][np.where(rechits["rechit_layer"]==layer)]

			#compute E19 sum
			if len(layer_hits_energy) >= 1:		#at least one hit for the following
				index_max_energy = np.argmax(layer_hits_energy)
				x_max = layer_hits_x[index_max_energy]
				y_max = layer_hits_y[index_max_energy]

				selected_indexes = np.where((layer_hits_x-x_max)**2+(layer_hits_y-y_max)**2<2.6**2)	#2.6 cm = 0.5 + 2 sqrt(3) * 0.65

				layer_hits_x = layer_hits_x[selected_indexes]		
				layer_hits_y = layer_hits_y[selected_indexes]		
				layer_hits_z = layer_hits_z[selected_indexes]		
				layer_hits_energy = layer_hits_energy[selected_indexes]		
			
			layer_energy = np.sum(layer_hits_energy)
			layer_hit_weights = 3.5 + np.log(layer_hits_energy / layer_energy)
			layer_hit_weights[np.where(layer_hit_weights<0)] = 0
			layer_weight_sum = np.sum(layer_hit_weights)

			if layer_weight_sum == 0:
				continue
			else:
				pos_layer_x = np.sum(layer_hits_x*layer_hit_weights)/layer_weight_sum
				pos_layer_y = np.sum(layer_hits_y*layer_hit_weights)/layer_weight_sum
				
				recPosData[layer]["x"].append(pos_layer_x+data_tracks[ev]["impactX_HGCal_layer_%i"%layer])
				recPosData[layer]["y"].append(pos_layer_y+data_tracks[ev]["impactY_HGCal_layer_%i"%layer])

out_table = []	
for layer in range(1, NLAYERS_EE+NLAYERS_FH+1):
	out_table.append((layer, np.median(recPosData[layer]["x"]), np.median(recPosData[layer]["y"])))

with open(args.outputFilePath, "w") as outfile:
	outfile.write(tabulate(out_table, headers=["layer", "delta_X_cm", "delta_Y_cm"]))
