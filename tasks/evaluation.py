import os, luigi

from config.outputs import OutputfileDirectories
from helpers.tasks import WorkflowTask
from helpers.decorators import *
from config.analysis import EVALUATIONPARAMETERS

class EvaluateCombinedTimingPerformance(WorkflowTask):
    task_namespace = 'Timing'
    pdgID = luigi.IntParameter(default=11, description="Particle ID to evaluate", significant=True)

    def requires(self):
        from tasks.reco import ComputeCommonTimestamp
        return {
            "common": ComputeCommonTimestamp(), 
            "2Dclusters": [Evaluate2DLayerClusterTimingPerformance(layer=_l) for _l in EVALUATIONPARAMETERS["MANUALLAYERSELECTION"]], 
            "channelvschannel": ChannelVsChannel(),
            "channelvschanneldifferential": ChannelVsChannelDifferential(),
            "layervslayer": LayerVsLayer(),
            "perchanneltiming": EvaluateTimingForAllChannels()
        } 

    @AddOutputDirectory(OutputfileDirectories["combined_performance"])  
    @DefineLocalTargets   
    def output(self):     
        return {
            "mpl": "3Dcluster_pdgID%i.png" % self.pdgID,
            "root": "3Dcluster_pdgID%i.root" % self.pdgID
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/evaluate_combined_performance.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFile": self.input()["common"].path,
            "outputFilePath": self.output()["mpl"].path, 
            "rootFilePath": self.output()["root"].path, 
            "pdgID": self.pdgID     
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass  


class LayerVsLayer(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        from tasks.reco import ComputeCommonTimestamp
        return ComputeCommonTimestamp()

    @AddOutputDirectory(OutputfileDirectories["layer_performance"])  
    @DefineLocalTargets   
    def output(self):     
        return "layer_vs_layer.root"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/layer_vs_layer_resolution.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFile": self.input().path,
            "outputFilePath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass 


class ChannelVsChannel(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        from tasks.reco import ApplyTimingCalibration
        return ApplyTimingCalibration()

    @AddOutputDirectory(OutputfileDirectories["channel_evaluation"])  
    @DefineLocalTargets   
    def output(self):     
        return "channel_vs_channel.root"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/channel_vs_channel_resolution.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileEvaluation": self.input().path,
            "outputFilePath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass  

class ChannelVsChannelDifferential(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        from tasks.reco import ApplyTimingCalibration
        return ApplyTimingCalibration()

    @AddOutputDirectory(OutputfileDirectories["channel_evaluation"])  
    @DefineLocalTargets   
    def output(self):     
        return "channel_vs_channel_differential/output.root"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/channel_vs_channel_differential.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileEvaluation": self.input().path,
            "outputFilePath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass  

class Evaluate2DLayerClusterTimingPerformance(WorkflowTask):
    task_namespace = 'Timing'
    layer = luigi.IntParameter(default=8, description="Layer to focus on", significant=True)

    def requires(self):
        from tasks.reco import ComputeCommonTimestamp
        return ComputeCommonTimestamp()

    @AddOutputDirectory(OutputfileDirectories["layer_performance"])  
    @DefineLocalTargets   
    def output(self):     
        return "2Dcluster_layer%i.root" % self.layer

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/evaluate_per_layer_performance.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFile": self.input().path,
            "rootFilePath": self.output().path, 
            "layer": self.layer,
            "pdgID": 11
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass  

# make sure to source ROOT manually beforehand
class EvaluateTimingForAllChannels(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        from tasks.calibration import SummariseIndividualCalibration
        return SummariseIndividualCalibration()

    @AddOutputDirectory(OutputfileDirectories["channel_evaluation"])  
    @DefineLocalTargets   
    def output(self):
        return "constant_terms.h5"

    def command(self):
        pass

    def run(self):
        import pandas as pd
        input_file_calibration = pd.HDFStore(self.input()["merged"].path)
        calib_data = input_file_calibration["calib"]
        input_file_calibration.close()        
        dependencies = []
        for ch_key in calib_data.unique_index.unique():
            dependencies.append(EvaluateChannelTimingPerformance(channelKey=int(ch_key)))
        yield dependencies


            # create module-layer mapping
        print "Create module to layer mapping"
        import numpy as np
        layer, module = np.genfromtxt(open(os.path.join(
            os.environ["WORKFLOWDIR"], "config/CondObjects_data_layer_geom_full_October2018_setup2_v1_promptReco.txt"), "r"), usecols=(0, 5), unpack=True)
        module_layer_map = {}
        for i in range(len(layer)):
            module_layer_map[int(module[i])] = int(layer[i])
        
        import ROOT
        tmp_data = []
        for _d in dependencies:
            _ch = _d.channelKey
            _module = int(_ch / 1000)
            _chip = int(_ch % 1000) / 100
            _channel = _ch % 100
            _layer = module_layer_map[_module]
            infile = ROOT.TFile(_d.output()["root"].path, "READ")
            f1 = infile.Get("calibrated_hit_time_combined")
            c_term = f1.GetParameter(0)
            s_term = f1.GetParameter(1)
            n_term = f1.GetParameter(2)
            val_800 = f1.Eval(800)
            tmp_data.append((_layer, _module, _chip, _channel, c_term, s_term, n_term, val_800))

        tmp_data = pd.DataFrame(tmp_data, columns=["layer", "module", "chip", "channel", "constant", "stochastic", "noise", "Val800MIP"])

        store = pd.HDFStore(self.output().path)
        store["c"] = tmp_data
        store.close()


class EvaluateChannelTimingPerformance(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)

    def requires(self):
        from tasks.reco import ApplyTimingCalibration
        return ApplyTimingCalibration(dataset="calibration")

    @AddOutputDirectory(OutputfileDirectories["channel_evaluation"])  
    @DefineLocalTargets    
    def output(self):
        return {
            "raw": "%i_raw.pdf" % self.channelKey,
            "2D": "%i_2D.pdf" % self.channelKey,
            "graph": "%i_graph.pdf" % self.channelKey,
            "root": "%i_fits.root" % self.channelKey
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/evaluation/evaluate_channel_performance.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileEvaluation": self.input().path,
            "channelKey": self.channelKey,
            "outputFilePath_Raw": self.output()["raw"].path,
            "outputFilePath_2D": self.output()["2D"].path,
            "outputFilePath_Graph": self.output()["graph"].path,
            "outputFilePath_ROOT": self.output()["root"].path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass           