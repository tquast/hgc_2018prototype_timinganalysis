import os, luigi

from config.outputs import OutputfileDirectories
from helpers.tasks import WorkflowTask
from config.tb_ntuples import *
from helpers.decorators import *


class TreatMinMax(WorkflowTask):
    task_namespace = 'Timing'
    runNumberMin = luigi.IntParameter(default=1000, description="minimum run number", significant=True)
    runNumberMax = luigi.IntParameter(default=1100, description="maximum run number", significant=True)

    def requires(self):
        inputs = []
        for run in getRuns(configuration=23):     #use only electron runs
            if not hasMCP(run):
                continue
            if not hasDWC(run):
                continue
            if run > self.runNumberMax:
                continue
            if run < self.runNumberMin:
                continue
            inputs.append(FlattenNtuple(runNumber=run))
        return inputs

    @AddOutputDirectory(OutputfileDirectories["minmax_corrected"])  
    @DefineLocalTargets    
    def output(self):
        return "toa_normalised_runs_%i_to_%i.h5" % (self.runNumberMin, self.runNumberMax)

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/preprocess/minmax_treatment.py"))
    @UnpackPythonOptions
    def command(self):
        tbr = {
            "outputFilePath": self.output().path,
            "inputFiles": ""
        }
        for inp in self.input():
            tbr["inputFiles"] += "%s " % inp.path
        return tbr

    @Subprocess(command)
    def run(self):
        pass
        

class FlattenNtuple(WorkflowTask):
    task_namespace = 'Timing'
    runNumber = luigi.IntParameter(default=0, description="run number", significant=True)

    def requires(self):
        return DetermineAlignmentCorrections()

    @AddOutputDirectory(OutputfileDirectories["flat_ntuples"])  
    @DefineLocalTargets
    def output(self):
        return "reduced_ntuple_run%i.h5" % self.runNumber

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/preprocess/reduce_ntuple.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFilePath": get_ntuple_path(self.runNumber),
            "outputFilePath": self.output().path,
            "alignmentPath": self.input().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass        


class DetermineAlignmentCorrections(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        pass      
    
    @AddOutputDirectory(OutputfileDirectories["alignment"])  
    @DefineLocalTargets
    def output(self):
        return "hgc_dwc_alignment.txt"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/preprocess/alignment.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "outputFilePath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass
