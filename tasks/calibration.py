import os, luigi

from config.outputs import OutputfileDirectories
from helpers.tasks import WorkflowTask
from helpers.decorators import *


class MergeTimingCalibrations(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        return {            
            "calibration_average": CalibrateAverageTimingForAllChannels(),
            "calibration_individual": SummariseIndividualCalibration(), 
            "channel_list": DefineDatasets()
        }
    
    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return "calibration_merged.h5"

    def command(self):
        pass

    def run(self):
        import numpy as np
        import pandas as pd
        store = pd.HDFStore(self.input()["calibration_individual"]["merged"].path)
        calib_individual = store["calib"]
        store.close()
        calib_individual = calib_individual.assign(average_calibration = 0)

        average_calibrations = []
        with open(self.input()["channel_list"]["list_average"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                store = pd.HDFStore(CalibrateTimingForChannel(channelKey=channel[0], individualCalibration=0).output()["calib"].path)
                calib_average_ch = store["calib"]
                store.close()
                calib_average_ch = calib_average_ch.assign(average_calibration = 1)
                average_calibrations.append(calib_average_ch)

        merged_calibration = pd.concat([calib_individual]+average_calibrations)
        merged_calibration = merged_calibration.drop(columns="unique_index")
        merged_calibration = merged_calibration.sort_values(["Module", "Chip", "Channel"], axis=0)
        outstore = pd.HDFStore(self.output().path)
        outstore["calib"] = merged_calibration
        outstore.close()


class CalibrateAverageTimingForAllChannels(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        return DefineDatasets()
    
    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return "calib_average_summary.txt"

    def command(self):
        pass

    def run(self):
        import numpy as np
        dependencies = []
        with open(self.input()["list_average"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                dependencies.append(CalibrateTimingForChannel(channelKey=channel[0], individualCalibration=0))
        yield dependencies
        
        with open(self.output().path, "w") as dummy_file:
            for dep in dependencies:
                dummy_file.write("Created %s \n"%dep.output()["calib"].path)


class SummariseIndividualCalibration(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        return {
            "channel_list": DefineDatasets(),
            "calibration_procedure": CalibrateTimingForAllChannels()
        }

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return {
            "merged": "calib_file.h5",
            "fig": "validation/offsets.png"
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/merge_and_visualise_calib_constants.py"))
    @UnpackPythonOptions
    def command(self):
        tbr = {
            "inputFiles": "",
            "mergedFile": self.output()["merged"].path, 
            "offsetFigure": self.output()["fig"].path
        }
        import numpy as np
        with open(self.input()["channel_list"]["list"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                tbr["inputFiles"] += CalibrateTimingForChannel(channelKey=channel[0], individualCalibration=1).output()["calib"].path+" "
        return tbr

    @Subprocess(command)
    def run(self):
        pass


class CalibrateTimingForAllChannels(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        return DefineDatasets()
    
    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return "calib_summary.txt"

    def command(self):
        pass

    def run(self):
        import numpy as np
        dependencies = []
        with open(self.input()["list"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                dependencies.append(CalibrateTimingForChannel(channelKey=channel[0], individualCalibration=1))
        yield dependencies
        
        with open(self.output().path, "w") as dummy_file:
            for dep in dependencies:
                dummy_file.write("Created %s \n"%dep.output()["calib"].path)


class CalibrateTimingForChannel(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)
    individualCalibration = luigi.IntParameter(default=1, description="Perform individual calibration (1) or average calibration (0)", significant=True)

    def requires(self):
        tbr = {
            "step1": CalibrateTimingForChannel_Step1(channelKey=self.channelKey, tightSelection=1),
            "step2": CalibrateTimingForChannel_Step2(channelKey=self.channelKey),
            "step3": CalibrateTimingForChannel_Step3(channelKey=self.channelKey),
            "step4": CalibrateTimingForChannel_Step4(channelKey=self.channelKey)
        }        
        if self.individualCalibration==0:
            tbr = {
                "step1": CalibrateTimingForChannel_Step1(channelKey=self.channelKey, tightSelection=0),
                "step2": CalibrateTimingForChannel_Step2(channelKey=0),         #channel key = 0 --> average calibration
                "step3": CalibrateTimingForChannel_Step3(channelKey=0),         #channel key = 0 --> average calibration
                "step4": CalibrateTimingForChannel_Step4(channelKey=0)          #channel key = 0 --> average calibration
            }            
        return tbr


    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return {
            "calib": "channel_%i_final.h5" % self.channelKey,
            "validation": "channel_%i_final.root" % self.channelKey
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/calibrate_channel_step5.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileCalib": self.input()["step1"].path,
            "step2File": self.input()["step2"]["calib"].path,
            "step3File": self.input()["step3"]["calib"].path,
            "step4File": self.input()["step4"]["calib"].path,
            "channelKey": self.channelKey,
            "outputFilePath": self.output()["validation"].path,
            "outputDataPath": self.output()["calib"].path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class CalibrateTimingForChannel_Step4(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)

    def requires(self):
        return CalibrateTimingForChannel_Step3(channelKey=self.channelKey)

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return {
            "data": "channel_%i_step4.h5" % self.channelKey,
            "calib": "channel_%i_step4.root" % self.channelKey
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/calibrate_channel_step4.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileCalib": self.input()["data"].path,
            "channelKey": self.channelKey,
            "outputFilePath": self.output()["calib"].path,
            "outputDataPath": self.output()["data"].path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class CalibrateTimingForChannel_Step3(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)

    def requires(self):
        return CalibrateTimingForChannel_Step2(channelKey=self.channelKey)

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return {
            "data": "channel_%i_step3.h5" % self.channelKey,
            "calib": "channel_%i_step3.root" % self.channelKey
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/calibrate_channel_step3.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileCalib": self.input()["data"].path,
            "channelKey": self.channelKey,
            "outputFilePath": self.output()["calib"].path,
            "outputDataPath": self.output()["data"].path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class CalibrateTimingForChannel_Step2(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)

    def requires(self):
        if self.channelKey==0:
            return DeriveAverageCalibration_Step1()
        else:
            return CalibrateTimingForChannel_Step1(channelKey=self.channelKey, tightSelection=1)

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return {
            "data": "channel_%i_step2.h5" % self.channelKey,
            "calib": "channel_%i_step2.root" % self.channelKey
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/calibrate_channel_step2.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileCalib": self.input().path,
            "channelKey": self.channelKey,
            "outputFilePath": self.output()["calib"].path,
            "outputDataPath": self.output()["data"].path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class DeriveAverageCalibration_Step1(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        return SummariseIndividualCalibration()

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return "all_average_step1.h5"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/compute_average_calibration_data.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "calibFile": self.input()["merged"].path,
            "outputDataPath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class CalibrateTimingForChannel_Step1(WorkflowTask):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)
    tightSelection = luigi.IntParameter(default=1, description="Run with hit energy preselection", significant=True)

    def requires(self):
        return DefineDatasets()

    @AddOutputDirectory(OutputfileDirectories["calibration"])  
    @DefineLocalTargets  
    def output(self):
        return "channel_%i_step1.h5" % self.channelKey

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/calibrate_channel_step1.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileCalib": self.input()["calibration"].path,
            "channelKey": self.channelKey,
            "tightSelection": self.tightSelection,
            "outputDataPath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        pass


class DefineDatasets(WorkflowTask):
    task_namespace = 'Timing'

    def requires(self):
        from tasks.preprocess import TreatMinMax
        from config.analysis import MINMAXPARAMETERS
        return [TreatMinMax(runNumberMin=r_min, runNumberMax=r_max) for r_min, r_max in MINMAXPARAMETERS["MINMAXCONSTRANGES"]]

    @AddOutputDirectory(OutputfileDirectories["datasets"])  
    @DefineLocalTargets    
    def output(self):   
        return {
            "list": "channel_list_for_individual_calibration.txt",
            "list_average": "channel_list_for_average_calibration.txt",
            "calibration": "samples_for_calibration.h5",
            "evaluation": "samples_for_evaluation.h5"
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/calibration/define_datasets.py"))
    @UnpackPythonOptions
    def command(self):
        tbr = {
            "outputFileCalibration": self.output()["calibration"].path,
            "outputFileEvaluation": self.output()["evaluation"].path,
            "outputFilePathIndividual": self.output()["list"].path,
            "outputFilePathAverage": self.output()["list_average"].path,
            "inputFiles": ""
        }
        for inp in self.input():
            tbr["inputFiles"] += "%s " % inp.path    
        return tbr     

    @Subprocess(command)
    def run(self):
        pass   