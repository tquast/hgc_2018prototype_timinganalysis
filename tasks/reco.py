import os, luigi

from config.outputs import OutputfileDirectories
from helpers.tasks import WorkflowTask
from helpers.decorators import *


class ComputeCommonTimestamp(luigi.Task):
    task_namespace = 'Timing'

    def requires(self):
        #return ApplyTimingCalibration(dataset="evaluation")
        return ApplyTimingCalibration(dataset="calibration")

    @AddOutputDirectory(OutputfileDirectories["combined_performance"])  
    @DefineLocalTargets 
    def output(self):     
        return "energy_weighted_sums.h5"

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/reco/compute_common_ts.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileEvaluation": self.input().path,
            "outputFilePath": self.output().path
        }

    @Subprocess(command)
    def run(self):
        pass


class ApplyTimingCalibration(WorkflowTask):
    task_namespace = 'Timing'
    dataset = luigi.Parameter(default="calibration", description="evaluation or calibration", significant=True)

    def requires(self):
        from tasks.calibration import DefineDatasets, MergeTimingCalibrations
        return {
            "calib": MergeTimingCalibrations(),
            "samples": DefineDatasets()
        }

    @AddOutputDirectory(OutputfileDirectories["calibrated_samples"])  
    @DefineLocalTargets 
    def output(self):     
        return "%s_calibrated.h5" % self.dataset

    @PythonCommand(os.path.join(os.environ["WORKFLOWDIR"], "scripts/reco/apply_calibration.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFileEvaluation": self.input()["samples"][self.dataset].path,
            "calibFile": self.input()["calib"].path,
            "outputFilePath": self.output().path,
            "dataset": self.dataset
        }

    @Subprocess(command)
    def run(self):
        pass
