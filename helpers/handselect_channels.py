# Created: 12 August 2020
# Last modified: 12 August 2020
# author: Thorben Quast, thorben.quast@cern.ch
# A script with which all images from the calibration in the indicated DIRECTORY are opened sequentially
# and user-defined answers for each image are saved in a dictionary.
# If not deleted, all answers are stored into a dedicated .pickle file such that the image browsing can be done over
# multiple sessions.

from PIL import Image
import os
import re
import pickle
import tqdm

DIRECTORY = "/Users/thorbenquast/Desktop/calibFiguresCopy/"
images = os.listdir(DIRECTORY)
tmp_result_file = os.path.join(DIRECTORY, "decisions.pickle")

if os.path.exists(tmp_result_file):
    with open(tmp_result_file, 'rb') as handle:
        decisions = pickle.load(handle)
else:
    decisions = {"channel": [], "answer": []}


def save():
    with open(tmp_result_file, 'wb') as handle:
        pickle.dump(decisions, handle, protocol=pickle.HIGHEST_PROTOCOL)
    accepted_channels = sorted([decisions["channel"][i] for i in range(
        len(decisions["channel"])) if decisions["answer"][i] == 1])
    rejected_channels = sorted([decisions["channel"][i] for i in range(
        len(decisions["channel"])) if decisions["answer"][i] == 0])
    print("Accepted channels:")
    print(accepted_channels)
    print("Rejected channels:")
    print(rejected_channels)


try:
    images_to_inspect = [
        imgpath for imgpath in images if ("_rise.png" in imgpath)]
    for imgpath in tqdm.tqdm(images_to_inspect, unit="images"):
        chkey = int(re.search(r'\d+', imgpath).group())
        if chkey in decisions["channel"]:
            print(chkey, "decision already taken")
            continue
        image = Image.open(os.path.join(DIRECTORY, imgpath))
        image.show()
        answer = int(input("Accept channel %i ? " % chkey))
        image.close()
        decisions["channel"].append(chkey)
        decisions["answer"].append(answer)
    save()

except:
    print("EXCEPTION")
    save()
