#Created: 20 January 2021
#Last modified: 20 January 2021
#author: Thorben Quast, thorben.quast@cern.ch

from config.analysis import TOA_LINEARITY_MAX, TOT_TURNON_MIPS

def boundary(xref):
	def _f(f):
		def __f(*args):
			return f(*args, xref=xref)
		return __f
	return _f



###### 1. TOA non-linearity #####
NPARAMETERS_FTOA = 7

@boundary(TOA_LINEARITY_MAX)
def func_TOA(x, a0, a1, b0, c0, c1, d0, d1, xref):
	lhs_xref = a0 * xref + b0 + c0/(xref - d0)
	rhs_xref = a1 * xref + 0 + c1/(xref - d1)
	b1 = lhs_xref - rhs_xref

	lhs = a0 * x + b0 + c0/(x - d0)
	rhs = a1 * x + b1 + c1/(x - d1)

	return ((x-xref)<0).astype(float) * lhs + ((x-xref)>=0).astype(float) * rhs	

#special fit function for ROOT
@boundary(TOA_LINEARITY_MAX)
def func_TOA_ROOT(x, par, xref):
	a0 = par[0]
	a1 = par[1]
	b0 = par[2]
	c0 = par[3]
	c1 = par[4]
	d0 = par[5]
	d1 = par[6]

	lhs_xref = a0 * xref + b0 + c0/(xref - d0)
	rhs_xref = a1 * xref + 0 + c1/(xref - d1)
	b1 = lhs_xref - rhs_xref

	lhs = a0 * x[0] + b0 + c0/(x[0] - d0)
	rhs = a1 * x[0] + b1 + c1/(x[0] - d1)

	if (x[0]-xref)<0:
		return lhs
	else:
		return rhs


###### 2. Hit energy TW #####
NPARAMETERS_FTW = 7

@boundary(TOT_TURNON_MIPS)
def func_TW(x, a0, a1, b0, c0, c1, d0, d1, xref):
	lhs_xref = a0 * xref + b0 + c0/(xref - d0)
	rhs_xref = a1 * xref + 0 + c1/(xref - d1)
	b1 = lhs_xref - rhs_xref

	lhs = a0 * x + b0 + c0/(x - d0)
	rhs = a1 * x + b1 + c1/(x - d1)

	return ((x-xref)<0).astype(float) * lhs + ((x-xref)>=0).astype(float) * rhs	


#special fit function for ROOT
@boundary(TOT_TURNON_MIPS)
def func_TW_ROOT(x, par, xref):
	a0 = par[0]
	a1 = par[1]
	b0 = par[2]
	c0 = par[3]
	c1 = par[4]
	d0 = par[5]
	d1 = par[6]

	lhs_xref = a0 * xref + b0 + c0/(xref - d0)
	rhs_xref = a1 * xref + 0 + c1/(xref - d1)
	b1 = lhs_xref - rhs_xref

	lhs = a0 * x[0] + b0 + c0/(x[0] - d0)
	rhs = a1 * x[0] + b1 + c1/(x[0] - d1)

	if (x[0]-xref)<0:
		return lhs
	else:
		return rhs

###### 3. Layer energy TW #####
NPARAMETERS_FTWESUM = 5
def func_Esum_TW(x1, x2, a, b, c, d, e):
	xref = TOT_TURNON_MIPS
	
	lhs = a + b*x1 + c*x1**2 + d*x1**3 + e*x1**4
	rhs = 0
	return ((x2-xref)<0).astype(float) * lhs + ((x2-xref)>=0).astype(float) * rhs	


from math import sqrt

#MCP resolution parameterisation
def mcp_resolution(A):
	c = 9.3		#ps
	n = 15200	#ps
	return sqrt(c**2 + n**2/A**2)

# Repeated gaus fit for evaluation
def repeatedGausFit(histogram, gaus, rangeInSigmaLeft=1.5, rangeInSigmaRight=1.5, range_adjustment=True):
	if range_adjustment:
		gaus.SetRange(histogram.GetXaxis().GetXmin(), histogram.GetXaxis().GetXmax())

	if rangeInSigmaRight==None:
		rangeInSigmaRight = rangeInSigmaLeft

	if histogram.GetEntries()==0:
		return
		
	gaus.SetParameter(1, histogram.GetMean())
	gaus.SetParameter(2, histogram.GetStdDev())
	gaus.SetLineColor(histogram.GetLineColor())
	gaus.SetParLimits(1, histogram.GetMean()-rangeInSigmaLeft*histogram.GetStdDev(), histogram.GetMean()+rangeInSigmaRight*histogram.GetStdDev())

	histogram.Fit(gaus, "QRN")
	for f_i in range(9):
		mu_tmp = gaus.GetParameter(1)
		sigma_tmp = gaus.GetParameter(2)
		if range_adjustment:
			gaus.SetRange(mu_tmp-rangeInSigmaLeft*sigma_tmp, mu_tmp+rangeInSigmaRight*sigma_tmp)
		gaus.SetParameter(1, mu_tmp)
		gaus.SetParameter(2, sigma_tmp)
		histogram.Fit(gaus, "QRN")
	histogram.Fit(gaus, "R")
