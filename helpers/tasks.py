import luigi
from abc import abstractmethod


class WorkflowTask(luigi.Task):

    @abstractmethod
    def output(self):
        pass

    @abstractmethod
    def run(self):
        pass        

    @abstractmethod
    def command(self):
        pass                