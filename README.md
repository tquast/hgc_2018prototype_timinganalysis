# HGC 2018 Prototype: Timing Performance Analysis

![](img/event_video.png)

### Content
1. [Introduction](#Introduction)
2. [Experimental infrastructure](#Experimental-infrastructure)
3. [Data reconstruction](#Data-reconstruction)
4. [Scope of this study](#Scope-of-this-study)
5. [Requirements](#Requirements)
6. [Instructions](#Instructions-to-run-the-calibration-and-performance-assessment)
7. [Preliminary results](#Preliminary-results)
8. [Outlook](#Outlook)

## Introduction
The [CMS High Granularity Calorimeter (HGCAL)]([SKIROC2-CMS](https://iopscience.iop.org/article/10.1088/1748-0221/12/02/C02019/meta)) is designed to provide timestamps of deposited energies with O(10ps) resolution.
In 2018, a prototype of this calorimeter was built and tested with high momentum particle beam at the H2 beam line at CERN. 
The used frontend ASIC is the [SKIROC2-CMS](https://iopscience.iop.org/article/10.1088/1748-0221/12/02/C02019/meta) chip which has prototype timing functionality analogous to the ultimate HGCROC design.

The accumulated test beam data provide a proof-of-concept of the envisioned HGCAL's timing capabilities. 
The code in this repository does the following:

* Per-channel determination of TOA-pedestals in approximately 24-hour periods.
* Per-channnel timing calibration.
* Per-channel timing performance evaluation.
* Computation of a combined timestamp for a given particle shower and assessment of its resolution using high-momentum positrons.

*An integral part of this study is the active usage of a reference time obtained from two MCP's in the beam line. 
With it, the reliance on the clock-to-beam spill asynchronicity is less stringent than for the approaches in which the true particle arrival time w.r.t. clock edge is assumed to be uniformly distributed in a 25 ns interval.*

The first two paragraphs of this README describe the underlying experimental infrastructure as well as the reconstruction of the true raw data to ntuples which are considered the input data to this study. Links to further reading are indicated therein.
It follows a more precise definition of the scope of this study and the computing requirements for running this code.
Preliminary results and a mid-term outlook are provided at the end.

## Experimental infrastructure
In 2017/18, more than 90 HGCAL prototype modules were assembled and placed into sampling configurations for tests with particle beam at CERN's H2 beam line: [List of runs in Google spreadsheet](https://docs.google.com/spreadsheets/d/1FlwthMixJHmHVBguYSbw5mIeBo2tF7rFJPTYLTNI9Vs/edit#gid=290023431).

### Construction of HGC prototype modules
The sensitive part of the HGCAL prototype modules are 6'', n-type silicon sensors with an active thickness of 300 or 200 microns. Each silicon sensor consists of 135 individual pads of different geometric shapes and sizes.

![](img/HGC_prototype_modules.png)

A particle going through the (depleted) silicon generates a signal which is readout by four [SKIROC2-CMS](https://iopscience.iop.org/article/10.1088/1748-0221/12/02/C02019/meta) ASICs.
This ASIC comprises prototype timing functionality via its TOA-rise and TOA-fall which correspond to the time-of-arrival of the induced signal within a clock cycle of 25 ns (clock frequency is 40 MHz).

**For more information on the HGCAL prototype assembly**, see [Chapter 4.5 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf).


### Setup at the H2 beam line at CERN in October 2018
The 2018 prototype had two compartments, one being the electromagnetic one (CE-EE or EE) with lead-copper as principal absorbers and the other being the hadronic one (CE-H or FH) with iron-copper as passive material.
While the FH contained the majority of the available modules, namely 9 layers with 7 modules each plus 3 layers with single modules, the EE was made of 28 layers with single modules. 
In good approximation, electromagnetic showers, e.g. due to positrons, were fully contained in the EE-compartment such that energy depositions in the FH typically stem from hadronically induced cascades (or muons which are not relevant here).
Since the average energy density of hadronic cascades in this calorimeter is smaller than for its electromagnetic counterpart, a large statistics dataset of high energy densities- where the TOA measurement is expected to have optimal resolution- in the FH could not be achieved in the time constrained test beam experiment.

*Consequently, the focus of this study is on the electromagnetic compartment, the EE (=CE-E).*

![](img/HGC_setup_H2_October2018.png)

The two compartments were placed in the [H2 beam line](http://sba.web.cern.ch/sba/BeamsAndAreas/h2/H2manual.html) in the CERN SPS North Area. Electrons and charged pions with momenta ranging from 20-300 GeV/c were used as test particles. (**However, this analysis makes use of the electron dataset only.**)
The detector readout was triggered by the coincidence of two scintillators placed closely upstream the calorimeter.
In addition, impact position measurements were performed with delay wire chambers and - more importantly for this study - the particle-induced signal waveforms of two MCPs in front of the EE-compartment were recorded. 
The analysis of the recorded MCP waveforms allows for the inference of a precise time reference of the particle incidence within a clock cycle. The associated time resolutions should amount to a few 10ps.
The picture below shows photographs of the utilised readout hardware, of a delay wire chamber and of the inside of one of the two MCPs.

![](img/Reference_Detectors.png)

**For detailed information**, see [Chapter 6 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf).

## Data reconstruction

The data reconstruction's objective is to convert the raw bitstreams into calorimeter hits and MCP reference timestamps.
A calorimeter hit is defined to be an active calorimeter cell (synonyme: channel) with 3D spatial coordinates, a reconstructed energy (density) and the time-of-arrival of the deposited energy.

The reconstruction and calibration of the hit energy can be found elsewhere, e.g. in [Chapter 7.2 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf) or the [H2 paper](https://arxiv.org/abs/2012.06336).
Here, the reconstruction of the time-related quanitites is summarised briefly.

### TOA reconstruction
The SKIROC2-CMS ASIC provides two time-of-arrival (TOA) information for channels with high-enough signals. The threshold for triggering the TOA is around a few 10MIPs.
While the TOA-rise measures the time w.r.t. to the next rising clock edge, the TOA-fall measures it w.r.t to the next falling clock edge. In this sense, both information are highly correlated, as can be seen below. By design, they only differ by a constant of 12.5 ns.

![](img/TOA_principle.png)

The conversion of the raw TOA, as it is read out, to actual calorimeter hit timestamps covers two aspects:

1. Calibration of the non-linearity of the TOA.
2. Calibration of the timewalk, i.e. the inherent dependence on the amplitude of the induced signal.

The aspects 1. and 2. are illustrated in the figure above (Fig. 7.5).

In addition, one allows also for a third dependence on the overall signal recorded in a module.
This can be motivated by considering systematic pedestal shifts for high energy densitites which would artificially delay the underlying signal. 

3. Calibration of the signal baseline (B) shifts at high energy densities in a module. This can be understood as a module-energy-dependent timewalk.


![](img/TOA_formula1.png)

Formally, the TOA timestamp (T) is a function the digitised TOA ("rise" in the shown parameterisations above), the calibrated hit energy (E) and the total energy reconstructed in a module. 
For the TOA's non-linearity, it is beneficial to normalise the raw TOA to [0, 1] by its minimum and maximum value to correct for potential pedestal variations.
The parameterisations of the non-linearity and of the hit energy timewalk are identical and are kept general. *Note that, the non-linearity model could be simplified further to account for boundary conditions.*

![](img/TOA_formula2.png)

For the timewalk induced by systematic baseline shifts, a fourth degree polynomial is deplayed.

![](img/TOA_formula3.png)

**All parameters that occur in the formulas above are subject to (channel-wise) calibration.**

**For additional information on the time reconstruction**, see [Chapter 7.2.5 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf).


### MCP timestamp reconstruction
The MCP waveforms were digitised and recorded in the beam tests. Furthmore, the clock signal was injected into the digitiser and recorded.
Example waveforms of one readout are shown below. What may also be seen there is that the efficiency to detect those as a function of the impact position inferred from the delay wire chambers reproduces the MCP's acceptance.

![](img/MCP_timing.png)

The distributions of the amplitudes of reference MCP 1 are shown below for positrons passing through it on the left (positron tracking with delay wire chambers) and for those whose DWC-track points elsewhere on the right.
As one can see, **requiring a minimum MCP amplitude of 100 ADC counts** selects a vast majority of signal events while rejecting most events for which the MCP was not traversed by a particle.

![](img/mcp_amplitudes.png)


By convention, the ultimate MCP timestamp is defined to be always with respect to the last falling edge of the clock. This introduces a fixed offset w.r.t. the TOA's timescale.
The MCP's time is thus confined to the interval of [0, 25ns].

![](img/MCP_formula.png)

**For more information on the MCP time reference reconstruction**, see [Chapter 7.5 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf).

## Scope of this study

The different timing information relevant to this study are sketched below.

![](img/time_measurement.png)

Provided that the calorimeter signal in a given channel is caused by the same particle that traverses the MCP, the reconstructed TOA timestamp (T) can be written as a sum of the MCP timestamp plus the "time-of-flight" (TOF) and the phase difference of the clock ("clock-phase-difference", CPD) injected into the digitiser and into the ROC.

![](img/TOA_MCP_relation1.png)
(The postfixes "-rise" and "-fall" are not explicitly noted down.)

__Role of the TOF__:
This TOF is mostly due to the longitudinal evolution of the shower (essentially the speed of light) and thus it depends on the cell's depth inside the calorimeter.
Distances transverse to the beam axis are considered on an event-by-event basis, but are negligible with respect to the depth.

__Role of the CPD__:
It is assumed that the CPD is a channel-dependent but fixed constant, or its variation is at least negligible. 


Then, the MCP timestamp can be written as a function of the measured hit energy and the normalised TOA (which is scaled to [0, 1]).

![](img/TOA_MCP_relation2.png)

This relationship implies a unqiue conversion of the raw TOA and hit energy to the MCP time which serves as the absolute reference in this system.

### Dataset

- 20-300 GeV positron-induced electromagnetic showers taken with configuration 2 during the October 2018 HGCAL beam test.
- The analysis is focussed on the electromagnetic sections, i.e. layers 1-28. 
- Due to the low occupancy of high energetic hits in the hadronic part, pions and with it the hadronic section are not considered in this study.

---
### Research questions

1. Does the raw TOA in conjunction with the hit energy E exhibit the expected relationship to the reference time as measured by the MCP?
2. What is the timing accuracy and precision for each channel as a function of the hit energy?
3. What is the timing accuracy and precision when the timestamps of multiple hits in an electromagnetic or hadronic shower are combined?

---

## Requirements
In order to run this analysis, please ensure that your computing system meets the following requirements.

### Hardware
* 21GB of free hard disk space for temporary files.
* at very least 8GB RAM (16GB recommended) for loading full datasets
* Access to the input data ntuples ("raw data") on ```/eos```: ```/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v17_patch2s``` 
 
### Software dependencies
* __**Python 2.7**__! 
* **luigi** for workflow management ([readthedocs](https://luigi.readthedocs.io/en/stable/))
* **pandas** for dataframe-based analysis ([installation](https://pandas.pydata.org)) incl. [pytables](https://www.pytables.org)
* **root_pandas** for converting the ROOT-based ntuples into pandas datadframes([github](https://pandas.pydata.org))
* **scipy** for its statistics tools ([website](https://pandas.pydata.org))
* **tensorflow** (v1.12) for unbinned fitting of higher-dimensional models to data ([website](https://www.tensorflow.org/install))
* **matplotlib** for state-of-the-art data visualisation ([website](https://matplotlib.org))
* **seaborn** for statistical data visualisation ([website](https://seaborn.pydata.org))
* **numpy** for interfacing between pandas, matplotlib and scipy when necessary - also has some useful tools ([website](https://numpy.org))
* **tqdm** pretty printing of loop iterations similar to a progress bar ([github](https://github.com/tqdm/tqdm))
* Also refer to ```requirements.txt```for recommended versions of the respective packages.


## Instructions on how to run the calibration and performance assessment
The analysis including timing calibration and performance assessment is implemented modularly in python scripts.
These scripts are steered by a [luigi](https://github.com/spotify/luigi)-based workflow. Each task therein, corresponds to a generic step of the analysis.
This repository defines command line commands to initiate the execution of such tasks.


### Setup
One has to set a few environmental parameters and define the terminal commands that invoke the execution of the analysis steps.
For this purpose, the shell script provided in the main directory needs to be sourced: 

```source setup.sh```

### Luigi-based analysis workflow

Luigi is a data pipelining tool that helps in creating analysis workflows with intermediate files and in resolving dependencies between analysis steps in a natural way.
A nice example including an illustrative example is given in this [blog post](https://intoli.com/blog/luigi-jupyter-notebooks/).
Motivated by [this application](https://www.researchgate.net/publication/317356570_Design_and_Execution_of_make-like_distributed_Analyses_based_on_Spotify's_Pipelining_Package_Luigi), luigi has also been used for the steering of the HGCAL beam test data reconstruction, see [Chapter 7.1 of T.Q.'s doctoral thesis](https://cds.cern.ch/record/2725040/files/CERN-THESIS-2019-367.pdf).

The main idea is to implement the analysis in atomistic and generic steps. The steps themselves may be written in any programming language. Here it is Python.
Every step is then wrapped into a luigi task which can be toggled via the command line.

The figure below illustrates the full luigi-based workflow for this analysis: starting from the data reduction all the way to the combined timing resolution of electromagnetic showers.
![](img/luigi_workflow.png)


### Description of implemented commands
The following commands are defined in ```setup.sh```and evoke a luigi task when executed in the terminal.

* ```DetermineAlignmentCorrections```
**(Data preprocessing)** Estimates the translational misalignment for each layer with respect to the delay wire chambers that are used for tracking the incident particle.

* ```ReduceNtuple <RUN>```
**(Data preprocessing)** Reads the ntuple content and saves timing information as flat pandas data frames for subsequent analysis independent of [ROOT](https://root.cern.ch).

* ```TreatMinMax <RUNMIN> <RUNMAX>```
**(Data preprocessing)** Merges files of runs within the indicated run range (RUNMIN-RUNMAX) and counts the occurence of channels for identification of the minimum and maximum TOA value.
Normalises the TOA and writes out the affected channels into a file.
Only those channels for which the TOA range could be determined reliably can be used for subsequent calibration and timing performance assessment.

* ```DefineDatasets```
**(Dataset definition)** Determines the channels for which the number of entries with energies above a given threshold is sufficient for subsequent time calibration.
Splits the full dataset into a dataset used for calibration and one used for evaluation.
Writes out a list of those channels both as a .h5 file and (optional) as an ntuple for visualisation with [this event display](https://github.com/ThorbenQuast/HGCal_TB_Geant4) (requires [ROOT](https://root.cern.ch/)).

* ```CalibrateTimingForChannel <CHANNELKEY>```
**(Calibration)** Calibrates either the TOA-rise in a four-step procedure given the calibration dataset:

1. Correcting for the period of 25ns in the MCP-time vs. TOA relation.
2. (Pre-) calibration of the timewalk, i.e. dependence on the hit energy (=signal size).
3. (Pre-) calibration of the TOA nonlinearity after correcting the for the timewalk.
4. Unbinned 2D fit of the (TOA, hit energy) to the MCP timestamp.

* ```CalibrateTimingForAllChannels```
**(Calibration)** Wrapper for the ```CalibrateTimingForChannel``` task that executes it for all possible channels and also for the TOA-fall.

* ```ApplyTimingCalibration```
**(Reconstruction)** Applies the derived calibration constants to derive hit timestamps for TOA fall and rise independently.
The TOA fall is corrected by adding 25ns/2 = 12.5ns to match the TOA-rise based timestamp.
The period of the MCP time is similarly corrected.

* ```ComputeCommonTimestamp```
**(Reconstruction)** Computes the common timestamp (or rather the combined deviation) of the calibrated hit timestamps with respect to the MCP time for the evaluation dataset.
This is done for for the TOA-rise/fall and for both combined.

* ```EvaluateChannelTimingPerformance <CHANNELKEY>```
**(Evaluation)** Evaluates the timing performance of single channels in terms of accuracy and precision as a function of the hit energy using the calibration dataset. Produces the relevant distributions and graphs as graphic files.
Applied both for the TOA-rise/fall and both combined.

* ```EvaluateTimingForAllChannels```
**(Evaluation)** Wrapper around the ```EvaluateChannelTimingPerformance```task that executes it for all calibrated channels.

* ```ChannelVsChannel```
**(Evaluation)** Channel vs. channel timestamp comparisons to infer intrinsic per-channel timing resolutions.

* ```ChannelVsChannelDifferential```
**(Evaluation)** Same as ```ChannelVsChannel``` but assessed differentially as a function of the first channel's hit energy.

* ```EvaluateCombinedTimingPerformance```
**(Evaluation)** Evaluation strategy as for the channels but applied to the event timestamps from many channels combined. Invokes all evaluation tasks as dependencies. Applied both for the TOA-rise/fall and both combined. This could be the final figure of this study.


### Configuration files
The definitions of the input test beam ntuples and of the parameters associated to the tasks above are implemented in two configuration files:

* ```config/tb_ntuples.py```: Definition of the run list and location of the test beam ntuples that are used in this study.
* ```config/analysis.py```: Definition and explanation of analysis parameters used at various locations in this workflow.

## Results (status: 23 February 2021)
### MCP-only timing resolution
The reconstructed timestamp of MCP1 is compared to the one of MCP2.
Assuming that both MCP's have the same resolution, the difference divided by sqrt 2 yields an estimate of the MCP-only timing resolution. Hereby, it is important to select events for which the reconstructed waveform amplitude in both MCPs are similar.

![](img/MCP_resolution_formula.png)

__To run this assessment__: 

```python scripts/mcp/mcp_timing_resolution.py```

The MCP-only time resolution converges towards a constant term of less than 10ps at high hit energies, whereas mean deviation between both MCPs (=the accuracy), fluctuates between 270-300 ps, i.e. by roughly 10%.

![](img/mcp_time_resolution.png)

**Note** that MCP2 is used only at this point, i.e. for estimating the timing resolution of MCP1.

### Calibration
Calibration can be performed both for the TOA-rise and the TOA-fall.
One example of corresponding figures for module 32, chip 2, channel 38 is shown below:

**Step 1: Correction of the 25ns periodicity**
![](img/channel_32238_step1.png)

**Step 2: Calibration of the TOA non-linearity**
![](img/channel_32238_step2.png)

**Step 3: Calibration of the hit energy timewalk**
![](img/channel_32238_step3.png)

**Step 4: Calibration of the layer-energy timewalk for hit energies below 250 MIP**
* Possibly caused by common mode noise subtraction.

![](img/channel_32238_step4.png)

**Finally: Residual distribution of calibrated hit timestamps**
* Left: Residual distribution
* 2nd left: Residual distribution vs. normalised TOA
* 2nd right: Residual distribution vs. hit energy
* Right: Residual distribution vs. layer energy

![](img/channel_32238_final.png)

The calibration sequence above is run for more than 130 channels in the electromagnetic section of the October 2018 HGCAL prototype.
From this set of channels, those with failed or bad calibrations are identified first automatically, hereby using the calibration parameters as a reference, and secondly manually.
Ultimately, the calibration is applied and the combined performance is assessed for 134 channels .
The location of these channels (in yellow) is visualised in this graphic:

![](img/channel_location.png)

### Channel-wise performance
To assess the quality of the calibration, the accuracy and precision as a function of the hit energy is evaluated on the same dataset as used for calibration.
Events are selected for which the reconstructed MCP amplitude is at least 500 ADC counts (corresponds to less than ~30ps MCP timing resolution).
For the example channel that was shown before:

![](img/32238_graph.png)

The timing resolution (HGCAL+MCP) for the channels is below 100ps at high energy densities.

* Channel vs. channel resolution is better for channels on the same module. Potentially, less affected by jitter of the clock distribution and/or by the layer- or hit energy timewalk(s).

![](img/channel_vs_channel.png)

**Important comment**: The reported values here serve as rough estimates since the widths are derived from gaussian fits in the full range which are sensitive to outliers.

### Shower timing performance
Events are selected for which the reconstructed MCP amplitude is at least 500 ADC counts (less than ~30ps MCP timing resolution).
The timestamps of hits within an electromagnetic (or hadronic) shower are combined through and energy-weigted average.
In this context, the MCP contribution has been estimated as the average resolution (driven by the MCP amplitude) in the respective dataset and has been subtracted in quadrature.

* **Electrons**

* Integrated time performance over full phase space:

![](img/performance_pdgID11_distributions.png)
![](img/pdgID11_shower_timing_accuracy.png)
![](img/pdgID11_shower_timing_resolution.png)

* Time performance as function of shower properties

![](img/pdgID11_shower_timing_resolutions_vs_esum.png)
![](img/pdgID11_shower_timing_resolutions_vs_nhits.png)
![](img/pdgID11_shower_timing_resolutions_vs_density.png)

* **Charged pions**

* Integrated time performance over full phase space:

![](img/performance_pdgID211_distributions.png)
![](img/pdgID211_shower_timing_accuracy.png)
![](img/pdgID211_shower_timing_resolution.png)

* Time performance as function of shower properties

![](img/pdgID211_shower_timing_resolutions_vs_esum.png)
![](img/pdgID211_shower_timing_resolutions_vs_nhits.png)
![](img/pdgID211_shower_timing_resolutions_vs_density.png)


**Take-away message: The combined (i.e. HGCAL+MCP) resolution amounts to approximately 50ps at high shower energies.**


![](img/performance.png)

## Outlook

### Todos (Winter 2021)
- [x] Improved calibration of the TOA nonlinearity. --> Non-linearity fitted in two ranges
- [x] Improve on the remaining bias with the hit energy. --> Additional residual correction.
- [x] Usage of average vs. median in profile-based calibration. --> Use of averages in ROOT TProfiles.
- [x] Rejection of miscalibrated channels from performance assessment. --> Improved by requiring at least 1000 entries for TOA nonlinearity calibration.
- [x] Shower time resolution and bias as a function of the average hit energy density. --> Done in February 2021.
- [x] (Consistency) Change order of calibration sequence. --> Calibration procedure fully refactored in January 2021.
- [x] (Enhancement) Definition of an alternative shower timestamp. --> Ongoing study by Axel P.
- [x] (Enhancement) Channel-channel timing resolutions. --> Done in February 2021. Confirms results obtained (independently) in my PhD thesis.


### Open questions (Spring 2020)

- [x] **MAJOR:** Investigation of the two populations in the left-hand-side plot in the e+-timing performance figure. --> The two populations are removed when constraining the TOAs to their linear region only. It can be somewhat reproduced when introducing inaccuracies (but not resolutions) that depend on the normalised TOA value.
- [x] **MAJOR:** Cause for inaccuracies of the combined timestamp of around 300ps (absolute value). The per-channel inaccuracies are well below that value. --> This is circumvented by allowing for another TW correction that depends on the energy reconstructed in the respective module.
- [x] **MAJOR:** Channel-channel variations of calibration parameters/curves?
- [ ] minor: Cause for the 10% variation in the mean time difference between MCP1 and MCP2? This deficit is minor because the number of events with high MCP signal amplitudes is steeply falling.

### Possible enhancements (Spring 2020)

- [x] Estimate the TOF using the delay wire chamber tracking. This could potentially minimise the contribution from TOF variations that otherwise would impair the measured timing resolution.
- [x] Assess the impact of restricting the time reconstruction in each event and channel to the TOA which is in its linear range while excluding the other one.
- [x] Use common energy reference in timewalk correction, i.e. common for all channels.
- [x] Calibrate the per-channel TOF as a function of the layer energy.
