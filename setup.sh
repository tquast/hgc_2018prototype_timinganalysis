export WORKFLOWDIR=$PWD
export OUTFILES_DIR=/home/tquast/hgc_prototype2018_timinganalysis_v1.8

export PYTHONPATH=$WORKFLOWDIR:$WORKFLOWDIR/tasks:$PYTHONPATH;
export ROOT_SOURCE="/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.34/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh";

source $ROOT_SOURCE;

EvaluateCombinedTimingPerformance() {
	PDGID=$1;
	luigi --module evaluation Timing.EvaluateCombinedTimingPerformance --workers 1 --pdgID $PDGID;	
}

EvaluateTimingForAllChannels() {
	luigi --module evaluation Timing.EvaluateTimingForAllChannels --workers 1;	
}

ChannelVsChannel() {
	luigi --module evaluation Timing.ChannelVsChannel --workers 1;	
}

ChannelVsChannelDifferential() {
	luigi --module evaluation Timing.ChannelVsChannelDifferential --workers 1;	
}

EvaluateChannelTimingPerformance() {
	CHKEY=$1
	luigi --module evaluation Timing.EvaluateChannelTimingPerformance --workers 1 --channelKey $CHKEY;	
}

ComputeCommonTimestamp() {
	luigi --module reco Timing.ComputeCommonTimestamp --workers 1;	
}

ApplyTimingCalibration() {
	luigi --module reco Timing.ApplyTimingCalibration --workers 1;	
}

MergeTimingCalibrations() {
	luigi --module calibration Timing.MergeTimingCalibrations --workers 1;	
}

SummariseIndividualCalibration() {
	luigi --module calibration Timing.SummariseIndividualCalibration --workers 1;	
}

CalibrateAverageTimingForAllChannels() {
	luigi --module calibration Timing.CalibrateAverageTimingForAllChannels --workers 1;	
}

CalibrateTimingForAllChannels() {
	luigi --module calibration Timing.CalibrateTimingForAllChannels --workers 1;	
}

CalibrateTimingForChannel() {
	CHKEY=$1
	INDIVIDUAL=$2;
	luigi --module calibration Timing.CalibrateTimingForChannel --workers 1 --channelKey $CHKEY --individualCalibration $INDIVIDUAL;	
}

DefineDatasets() {
	luigi --module calibration Timing.DefineDatasets --workers 1;	
}

TreatMinMax() {
	RUN_MIN=$1
	RUN_MAX=$2
	luigi --module preprocess Timing.TreatMinMax --workers 1 --runNumberMin $RUN_MIN --runNumberMax $RUN_MAX;
}

ReduceNtuple() {
	RUN=$1
	luigi --module preprocess Timing.FlattenNtuple --workers 1 --runNumber $RUN;
}

DetermineAlignmentCorrections() {
	luigi --module preprocess Timing.DetermineAlignmentCorrections --workers 1;
}
