import os

#path for the reconstructed/analysed files
BASEPATH = os.environ["OUTFILES_DIR"]
OutputfileDirectories = {
	"alignment": os.path.join(BASEPATH, "alignment"),
	"flat_ntuples": os.path.join(BASEPATH, "flat_ntuples"),
	"minmax_corrected": os.path.join(BASEPATH, "minmax_corrected"),
	"datasets": os.path.join(BASEPATH, "datasets"),
	"calibration": os.path.join(BASEPATH, "calibration"),
	"calibrated_samples": os.path.join(BASEPATH, "calibrated_samples"),
	"combined_performance": os.path.join(BASEPATH, "combined_performance"),
	"layer_performance": os.path.join(BASEPATH, "layer_performance"),
	"channel_evaluation": os.path.join(BASEPATH, "channel_evaluation")
}