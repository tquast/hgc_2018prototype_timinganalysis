# speed of light for TOF computation, potentially useful (tbc)
SPEED_OF_LIGHT = 29.9792458  # cm/ns
# maximum (normalised) TOA value in which the linearity of TOA-time is assumed to hold
TOA_LINEARITY_MAX = 0.65
# hit energy in MIPs at which the TOT tends to be used
TOT_TURNON_MIPS = 250.
# number of layers in the EE to be considered in this analysis
NLAYERS_EE = 28
NLAYERS_FH = 12

NTUPLEPROCESSPARAMETERS = {
    "POS_MCP1": -20,  # position of MCP1 along the z-axis w.r.t. to the start of the HGC-EE prototype, unit: cm
    # maximum number of hits in the FH for an event to be accepted for analysis
    "MAXNHITSFH": 10e9,     #(essentially infinity)
    # minimum hit energy in MIPs for a hit to be considered in the event-based analysis
    "MINHITENERGYANALYSIS": 0.5,
}

# parameters for the layer - DWC alignment corrections
ALIGNMENTPARAMETERS = {
    "MIN_DWC_REF_TYPE": 13,
    "MAX_DWC_CHI2": 10
}

# parameters for the estimation of the MCP-only timing resolution:
MCPSTUDYPARAMETERS = {
    # maximum amplitude difference between MCP 1 and MCP 2 amplitudes for estimating the combined resolution
    "MAXDELTAAMP": 50,
    # maximum amplitude of MCP1 assumed in this study
    "MAXAMPMCP1": 2000,
    # number of bins in the amplitude spectrum in which the combined resolution is to be estimated
    "NBINSMCP": 20
}

# parameters relevant for the determination of the TOA min and max
MINMAXPARAMETERS = {
    "MINAMP_MCP1": 100,  # global event selection: minimum amplitude of MCP1
    # must have at least 10k entries to have less than 1% probability that minimum or maximum value are not hit
    # n.b. p(either min or max not hit) = 2(1-1/x)^N-(1-2/x)^N, x=TOA range, N=number of entries in sample
    "NMINFORANALYSIS": 100,
    # Run ranges in which the TOA pedestals are assumed to be constant <--> in which they are computed separately.
    "MINMAXCONSTRANGES": [(912, 958), (960, 1002), (1003, 1056)],
    #quantile range to use for estimating a more outlier-resistant min/max
    "IQR": 0.9999
}

# parameters relevant for the selection of channels subject for calibration
CHANNELSELECTIONPARAMETERS = {
    # minimum number of entries for channel to be considered for individual calibration
    "NMIN_FOR_CALIB": 30000,
    # minimum energy for precalibration of the TOA, unit: MIPs
    "EMIN_FORCALIBRATION": 250,
    # minimum number of entries with hit energies above EMIN_FORCALIBRATION such that channel is subject for calibration
    # if number of entries is smaller, the particular channel is not calibrated
    "NMIN_ABOVE_EMIN_FOR_CALIB": 1000,
    # list of runs for ultimate performance assessment - not used in the calibration
    "EVALRUNS": [1012, 1007, 960, 971, 985, 998, 978, 967, 992, 1016, 1018, 941, 1051, 1045, 946, 1027, 956, 934]
}

# parameters relevant for the calibration of the TOA to the MCP timestamp
CALIBRATIONPARAMETERS = {
    # minimum hit energy for entries to be considered in the calibration, unit: MIPs
    "MINHITENERGY": 50,
    # number of hit energy bins in the timewalk precalibration
    "NBINS_TW_PRECALIB": 100,
    # number of bins in the layer energy-timewalk precalibration
    "NBINS_TWESUM_PRECALIB": 100,    
    # number of TOA bins in the TOA nonlinearity precalibration
    "NBINS_TOA_PRECALIB": 100,
    # minimum energy for entries to be included in the TOA precalibration, unit: MIPs
    "MINENERGYFORTWCALIB": 50,
    # minimum energy for entries to be included in the TOA precalibration, unit: MIPs
    "MINENERGYFORTOACALIB": CHANNELSELECTIONPARAMETERS["EMIN_FORCALIBRATION"],
}

# parameters relevant for the selection of good calibration results
CALIBRATIONMERGINGPARAMETERS = {
    # assumed hit energy for computation of reference timestamp of a calibrated channel, unit: MIP
    "HITENERGY_FOR_REFERENCE": 600,
    # assumed layer energy for computation of reference timestamp of a calibrated channel, unit: MIP
    "ELAYER_FOR_REFERENCE":	2000,
    # assumed normalised TOA for computation of reference timestamp of a calibrated channel, unit: 1
    "TOANORM_FOR_REFERENCE": 0.5,
    # minimum computed timestamp at reference point for calibration result to be accepted, unit: ns
    "MIN_TIMEREF": 16,
    # maximum computed timestamp at reference point for calibration result to be accepted, unit: ns
    "MAX_TIMEREF": 24,
    # maximum deviation of computed timestamp at reference point w.r.t. to chip-average for calibration result to be accepted, unit: standard deviations
    "MAX_DEVIATION_TIMEREF": 3.0,
    # hand-selected channels for which the calibration does not appear reasonable and which are not subject to the merging in the following
    "BADCHANNELS_HANDSELECTED": []
}

# parameters relevant for the channel-wise evaluation of the timing performance
CHANNELEVALUATIONPARAMETERS = {
    # minimum hit energy for entries to be considered in the evaluation of the channel-wise timing performance
    "MINHITENERGY": 50,
    # minimum MCP 1 amplitude (higher --> less contribution to measured resolution from MCP1)
    "MINAMPMCP1": 500,
    # minimum number of entries for channel to be evaluated
    "MINENTRIESFOREVAL": 200
}

# parameters relevant to the computation of a common timestamp for each event
COMMONTSPARAMETERS = {
    # minimum hit energy for entries to be considered in the computation of an event timestamp, unit: MIPs
    "MINHITENERGY": 50
}

# parameters relevant for the combined evaluation of the timing performance
EVALUATIONPARAMETERS = {
    # minimum MCP 1 amplitude (higher --> less contribution to measured resolution from MCP1)
    "MINAMPMCP1": {11: 500, 211: 100}, 
    # layers to explicitly evaluate
    "MANUALLAYERSELECTION": [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18], 
    # channels to explicitly evaluate
    "MANUALCHANNELSELECTION": [84138, 84240, 32226, 32228, 69138, 69240, 79226, 79228, 76138, 76240, 83226, 83228]
    #"MANUALCHANNELSELECTION": [84136, 84138, 84238, 84240, 32226, 32228, 32336, 69136, 69138, 69238, 69240, 79226, 79228, 79336, 76136, 76138, 76238, 76240, 83226, 83228, 83336]
}
